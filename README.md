## NETIOT UI

UI for the netiot platform.

Built with [Create React App](https://github.com/facebook/create-react-app). 

Uses [react redux](https://www.valentinog.com/blog/react-redux-tutorial-beginners/) and 
[react router](https://reacttraining.com/react-router/).

### Installation
Install [node.js](https://nodejs.org/en/).

Clone project and run `npm install`.

In `.env` change:
 - REACT_APP_DOMAIN
 - REACT_APP_SERVER_BASE_PATH
 - REACT_APP_SERVER_AUTH_BASE_PATH
 - REACT_APP_SERVER_AUTH_USERNAME
 - REACT_APP_SERVER_AUTH_PASSWORD

Start with `npm start`.  

### Structura

To better understand the structure, you can follow [this](http://jasonwatmore.com/post/2017/09/16/react-redux-user-registration-and-login-tutorial-example) tutorial.
(only difference is that we used `utils` instead of `helpers`).

We also have: 
#### /containers
App's pages.

#### /imgs
Images

#### /translations
Localization. Uses [react-intl](https://www.codeandweb.com/babeledit/tutorials/how-to-translate-your-react-app-with-react-intl) si este l10n ready.

Not fully implemented. An example can be seen in `LoginPage.jsx` with `FormattedMessage`.

### Tests
N/A

### CI/CD
Builds the docker image and saves it to the gitlab docker registry.

### Fake backend
Use the `fake-backend.js` file to test new routes that are not implemented on the backend. There are many examples there.

### Notes
Some forms are generated dynamically based on the json received from the backend. This is done using [JSONForms](https://jsonforms.io/).

The applications' interface is loaded dynamically based on the js received from the server (each application microserver
also defines a frontend that is compiled into one js file). This is done in `ApplicationsTableToolbar`, in `handleLaunchApp`.
This uses some tags: `tempApp`, `customApp` and `div-id`. The application configuration is also placed in a tag `appConfig`.
You can understand more about how this works if you checkout the `app-interface` project and look at the test section in
the readme.
 

### Adding a new page example

1. Define new route in `App.js`
2. Add the page's folder in `containers`
3. Modify `HomePage.jsx` and/or one of the HeaderLinks from `components/Header`
4. Use an existing page as a starting point



