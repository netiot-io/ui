import React from "react";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

import infoStyle from "./infoStyle.jsx";

function InfoArea({ ...props }) {
  const { classes, title, description } = props;

  return (
    <div className={classes.infoArea}>
      <div className={classes.descriptionWrapper}>
        <h3 className={classes.infoTitle}>{title}</h3>
        <p className={classes.description}>{description}</p>
      </div>
    </div>
  );
}

InfoArea.defaultProps = {
  iconColor: "gray"
};

InfoArea.propTypes = {
  classes: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  vertical: PropTypes.bool
};

export default withStyles(infoStyle)(InfoArea);
