import React, { Fragment } from 'react';
import {Table, TablePagination , TableCell, TableBody, TableRow, Checkbox, Paper} from '@material-ui/core';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import enhancedTableStyle from './applicationsTableStyles.jsx';
import ApplicationsTableHead from './ApplicationsTableHead';
import ApplicationsTableToolbar from './ApplicationsTableToolbar';
import { connect } from 'react-redux';
import { TableFilter} from "../../Modals/TableFilter";
import { filterSelectors, filterActions } from 'material-ui-filter';
import { withRouter } from 'react-router-dom';
const qs = require('querystringify');

function getSorting(order, orderBy) {
  return order === 'desc'
    ? (a, b) => (b[orderBy] < a[orderBy] ? -1 : 1)
    : (a, b) => (a[orderBy] < b[orderBy] ? -1 : 1);
}

const columnDataEnterprise = [
  { id: 'id', align: 'left', disablePadding: false, label: 'ID' },
  { id: 'name', align: 'left', disablePadding: false, label: 'Name' },
  { id: 'type', align: 'left', disablePadding: false, label: 'Type' },
  { id: 'preview', align: 'left', disablePadding: false, label: 'Preview' },
];

const columnDataPartner = [
  { id: 'id', align: 'left', disablePadding: false, label: 'ID' },
  { id: 'name', align: 'left', disablePadding: false, label: 'Name' },
  { id: 'type', align: 'left', disablePadding: false, label: 'Type' },
  { id: 'organization', align: 'left', disablePadding: false, label: 'Organization' },
  { id: 'preview', align: 'left', disablePadding: false, label: 'Preview' },
];

// ONLY used if we display the filter drawer (see demo https://tarikhuber.github.io/material-ui-filter/)
const filterFields = [
  { name: 'name', label: 'Name' },
  { name: 'type', label: 'Type' },
  { name: 'organization', label: 'Organization' }
];

class ApplicationsTable extends React.Component {
  constructor(props) {
    super(props);
    const {search} = props.location;
    let filterBy = '';
    if (search.length > 0) {
      const query = qs.parse(this.props.location.search);
      filterBy = query.hasOwnProperty('filterBy') ? query.filterBy : '';
    }

    if (filterBy.length > 0) {
      props.setSearch("applications", filterBy);
    }

    this.state = {
      order: 'asc',
      orderBy: 'id',
      selected: [],
      page: 0,
      rowsPerPage: 5,
      searchDefaultValue: filterBy
    };

    this.appRef = React.createRef();
  }

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    this.setState({ order, orderBy });
  };

  handleSelectAllClick = (event, checked) => {
    if (checked) {
      this.setState((prevState, props) => {
        return { selected: props.data.applications.map(n => n.id) };
      });
      return;
    }
    this.setState({ selected: [] });
  };

  handleClick = (event, id) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    this.setState({ selected: newSelected });
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  handleEdit = (application) => {
    this.props.handleEdit(application);
  };

  handleDelete = () => {
    this.props.handleDelete(this.state.selected);
    this.setState({ selected: [] });
  };

  handleCloseToolbar = () => {
    this.setState({ selected: [] });
  };

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  render() {
    const { classes, applications, user, users, filteredApps, abilities } = this.props;
    const { order, orderBy, selected, rowsPerPage, page, searchDefaultValue } = this.state;
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, filteredApps.length - page * rowsPerPage);
    return (
      <Fragment>
        <TableFilter filterName={'applications'} filterFields={filterFields} defaultValue={searchDefaultValue}/>
        <Paper className={classes.root}>
          <ApplicationsTableToolbar applications={applications} selectedApplications={filteredApps.filter(application => selected.includes(application.id))}
                                    handleEdit={this.handleEdit} handleAdd={this.props.handleAdd} handleDelete={this.handleDelete}
                                    handleCloseToolbar={this.handleCloseToolbar} appElement={this.appRef}
          />
          <div className={classes.tableWrapper}>
            <Table className={classes.table} aria-labelledby="tableTitle">
              <ApplicationsTableHead
                numSelected={selected.length}
                order={order}
                orderBy={orderBy}
                onSelectAllClick={this.handleSelectAllClick}
                onRequestSort={this.handleRequestSort}
                rowCount={filteredApps.length}
                columnData={abilities.can('view', 'partner_header') ? columnDataPartner : columnDataEnterprise}
              />
              <TableBody>
                {filteredApps
                  .sort(getSorting(order, orderBy))
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map(application => {
                    const isSelected = this.isSelected(application.id);
                    return (
                      <TableRow
                        hover
                        onClick={event => this.handleClick(event, application.id)}
                        role="checkbox"
                        aria-checked={isSelected}
                        tabIndex={-1}
                        key={application.id}
                        selected={isSelected}
                      >
                        <TableCell padding="checkbox" align="left">
                          <Checkbox checked={isSelected} />
                        </TableCell>
                        <TableCell component="th" scope="row" align="left">
                          {application.id}
                        </TableCell>
                        <TableCell align="left">{application.name}</TableCell>
                        <TableCell align="left">{application.application_type_name}</TableCell>
                        {abilities.can('view', 'partner_header') &&
                        <TableCell align="left">{
                          application.organizations.map((organizationId, index) => {
                            let text = user.organizationId === organizationId ? '' :
                                users.organizations.find(o => o.id === organizationId).name;
                            if (text.length > 0 && application.organizations.length !== index + 1) {
                              text += ', ';
                            }
                            return text;
                        })
                        }</TableCell>
                          }
                        <TableCell align="left"><img src={`data:image/png;base64,${application.application_type_thumbnail}`} style={{height: 100}} alt={application.name}/></TableCell>
                      </TableRow>
                    );
                  })}
                {emptyRows > 0 && (
                  <TableRow style={{ height: 49 * emptyRows }}>
                    <TableCell colSpan={(abilities.can('view', 'partner_header') ? columnDataPartner : columnDataEnterprise).length + 1 } />
                  </TableRow>
                )}
              </TableBody>
            </Table>
          </div>
          <TablePagination
            component="div"
            count={filteredApps.length}
            rowsPerPage={rowsPerPage}
            page={page}
            backIconButtonProps={{
              'aria-label': 'Previous Page',
            }}
            nextIconButtonProps={{
              'aria-label': 'Next Page',
            }}
            onChangePage={this.handleChangePage}
            onChangeRowsPerPage={this.handleChangeRowsPerPage}
          />
          <div id="custom-app" ref={this.appRef} />
        </Paper>
      </Fragment>
    );
  }
}

ApplicationsTable.propTypes = {
  classes: PropTypes.object.isRequired,
  handleEdit: PropTypes.func.isRequired,
  handleDelete: PropTypes.func,
  handleAdd: PropTypes.func
};

function mapStateToProps(state) {
  const { authentication, users, applications, filters } = state;
  const { user, abilities } = authentication;
  const { hasFilters } = filterSelectors.selectFilterProps('applications', filters);
  const apps = applications.applications.map(application => {
    if (abilities.can('view', 'organizations')) {
      const organization = users.organizations.find(organization => application.organizations.includes(organization.id));
      return {
        ...application,
        organizationName: typeof organization !== 'undefined' ? organization.name : ''
      };
    } else {
      return {
        ...application,
        organizationName: user.organizationName
      };
    }
  });
  const filteredApps = filterSelectors.getFilteredList('applications', filters, apps /*, fieldValue => fieldValue.val*/);
  return {
    user,
    users,
    applications,
    hasFilters,
    filteredApps,
    abilities
  };
}

const connectedApplicationsTable = connect(mapStateToProps, { ...filterActions })(withRouter(ApplicationsTable));
const styledApplicationsTable = withStyles(enhancedTableStyle)(connectedApplicationsTable);
export { styledApplicationsTable as ApplicationsTable };