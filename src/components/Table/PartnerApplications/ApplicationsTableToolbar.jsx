/* eslint no-eval: 0 */

import React, {Fragment} from 'react';
import { Toolbar, Typography, IconButton, Tooltip, CircularProgress } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import AddDeviceIcon from '@material-ui/icons/AddBox';
import LaunchIcon from '@material-ui/icons/Launch';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import enhancedTableToolbarStyle from './applicationsTableToolbarStyles.jsx';
import withStyles from '@material-ui/core/styles/withStyles';
import { ApplicationAddDialog } from '../../Modals/ApplicationAddDialog';
import ApplicationDeleteDialog from '../../Modals/ApplicationDeleteDialog';
import {authHeader} from "../../../utils/auth-header";
import {applicationActions} from "../../../actions";
import {connect} from 'react-redux';
import { env } from '../../../utils/config';

class ApplicationsTableToolbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openDelete: false,
      openAdd: false,
      progress: false
    };
  }

  handleOpenDelete = () => {
    this.setState({ openDelete: true });
  };

  handleCloseDelete = () => {
    this.setState({ openDelete: false });
  };

  handleOpenAdd = () => {
    this.setState({ openAdd: true });
  };

  handleCloseAdd = () => {
    this.setState({ openAdd: false });
    this.props.handleCloseToolbar();
  };


  handleLaunchApp = async () => {
    this.setState({progress: true});
    await this.props.dispatch(applicationActions.getApplicationConfig(
      this.props.applications.applicationTypes.find(type => type.id === parseInt(this.props.selectedApplications[0].application_type_id, 10)).application_path,
      this.props.selectedApplications[0].id));
    this.loadApp();
  };

  loadApp() {
      const requestOptions = {
        method: 'GET',
        headers: authHeader()
      };
      fetch(`${env.REACT_APP_SERVER_BASE_PATH}${this.props.selectedApplications[0].application_path}${env.REACT_APP_SERVER_APP_DATA}`, requestOptions)
        .then(response => response.text())
        .then((js) => {
          if (document.getElementById("tempApp") !== null) document.getElementById("tempApp").remove();
          if (document.getElementById("custom-app") !== null) document.getElementById("custom-app").innerHTML = "";
          const script = document.createElement("script");
          script.setAttribute('id', 'tempApp');
          script.setAttribute('type', 'text/javascript');

          const appConfig = {
            appId: this.props.selectedApplications[0].id,
            data: this.props.applications.applicationConfig.data
          };
          script.setAttribute('appConfig', JSON.stringify(appConfig).replace(/"/g, '\''));
          script.setAttribute('div-id', 'custom-app');
          script.innerText = js;

          document.body.appendChild(script);
          (function(){
            const e = eval;
            e(document.getElementById('tempApp').innerHTML);
          })();
          this.setState({progress: false});
          window.scrollTo({ behavior: 'smooth', top: this.props.appElement.current.offsetTop });
        })
        .catch(e => {
          console.log(e);
          this.setState({progress: false});
        });
  }


  render() {
    const { selectedApplications, classes } = this.props;
    const numSelected = selectedApplications.length;
    return (
      <Toolbar
        className={classNames(classes.root, {
          [classes.highlight]: numSelected > 0,
        })}
      >
        <div className={classes.title}>
          {numSelected > 0 ? (
            <Typography color="inherit" variant="subheading">
              {numSelected} selected
            </Typography>
          ) : (
            <Typography variant="title" id="tableTitle">
              Applications
            </Typography>
          )}
        </div>
        <div className={classes.spacer} />
        <div>
          {this.state.progress && <CircularProgress /> }
        </div>
        <div className={classes.actions}>
          {[numSelected === 1 ? (
            <Fragment key='11'>
              <Tooltip title="Edit" key='0'>
                <IconButton aria-label="Edit" onClick={this.handleOpenAdd}>
                  <EditIcon className={classes.editDevice}/>
                </IconButton>
              </Tooltip>
              <Tooltip title="Launch" key='3'>
                <IconButton aria-label="Launch" onClick={this.handleLaunchApp}>
                  <LaunchIcon className={classes.editDevice}/>
                </IconButton>
              </Tooltip>
            </Fragment>
          ) : (null),
            numSelected > 0 ? (
              <Tooltip title="Delete" key='1'>
                <IconButton aria-label="Delete" onClick={this.handleOpenDelete}>
                  <DeleteIcon className={classes.deleteDevice} />
                </IconButton>
              </Tooltip>
            ) : (
              <Tooltip title="Add application" key='2'>
                <IconButton aria-label="Add application" onClick={this.handleOpenAdd}>
                  <AddDeviceIcon className={classes.addDevice} />
                </IconButton>
              </Tooltip>
            )]}
        </div>
        <ApplicationDeleteDialog
          open={this.state.openDelete}
          close={this.handleCloseDelete}
          handleDelete={this.props.handleDelete}
          selectedApplications={selectedApplications}
        />
        <ApplicationAddDialog
          open={this.state.openAdd}
          close={this.handleCloseAdd}
          handleAdd={this.props.handleAdd}
          handleEdit={this.props.handleEdit}
          isEdit={selectedApplications.length === 1}
          selectedApplication={selectedApplications.length === 1 ? selectedApplications[0] : null}
        />
      </Toolbar>
    );
  }
}

ApplicationsTableToolbar.propTypes = {
  classes: PropTypes.object.isRequired,
  applications: PropTypes.object.isRequired,
  selectedApplications: PropTypes.array.isRequired,
  handleEdit: PropTypes.func.isRequired,
  handleAdd: PropTypes.func,
  handleCloseToolbar: PropTypes.func.isRequired,
  appElement: PropTypes.object.isRequired
};

const connectedApplicationsTableToolbar = connect()(ApplicationsTableToolbar);
export default withStyles(enhancedTableToolbarStyle)(connectedApplicationsTableToolbar);