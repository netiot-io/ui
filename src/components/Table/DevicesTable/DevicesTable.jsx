import React, {Fragment} from 'react';
import {Table, TablePagination , TableCell, TableBody, TableRow, Checkbox, Paper, Tooltip } from '@material-ui/core';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import devicesTableStyle from './devicesTableStyles.jsx';
import DevicesTableHead from './DevicesTableHead';
import { DevicesTableToolbar } from './DevicesTableToolbar';
import { filterSelectors, filterActions } from 'material-ui-filter';
import { connect } from 'react-redux';
import { TableFilter} from "../../Modals/TableFilter";

function getSorting(order, orderBy) {
  return order === 'desc'
    ? (a, b) => (b[orderBy] < a[orderBy] ? -1 : 1)
    : (a, b) => (a[orderBy] < b[orderBy] ? -1 : 1);
}

const columnData = [
  { id: 'id', align: 'left', disablePadding: false, label: 'ID' },
  { id: 'name', align: 'left', disablePadding: false, centered: true, label: 'Name' },
  { id: 'deveui', align: 'left', disablePadding: false, label: 'DEVEUI' },
  { id: 'count', align: 'left', disablePadding: false, label: 'Message count' },
  { id: 'last', align: 'left', disablePadding: false, centered: true, label: 'Last message' },
  { id: 'alerts', align: 'left', disablePadding: false, centered: true, label: 'Alerts' },
  { id: 'description', align: 'left', disablePadding: false, centered: true, label: 'Description' },
];

// ONLY used if we display the filter drawer (see demo https://tarikhuber.github.io/material-ui-filter/)
const filterFields = [
  { name: 'name', label: 'Name' },
  { name: 'description', label: 'Description' },
];

class DevicesTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      order: 'asc',
      orderBy: 'id',
      selected: [],
      page: 0,
      rowsPerPage: 5,
    };
  }

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    this.setState({ order, orderBy });
  };

  handleSelectAllClick = (event, checked) => {
    if (checked) {
      this.setState((prevState, props) => {
        return { selected: props.data.items.map(n => n.id) };
      });
      return;
    }
    this.setState({ selected: [] });
  };

  handleClick = (event, id) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    this.setState({ selected: newSelected });
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  handleDelete = () => {
    this.props.handleDelete(this.state.selected);
    this.setState({ selected: [] });
  };

  handleEdit = (sensor) => {
    this.props.handleEdit(sensor);
  };

  handleCloseToolbar = () => {
    this.setState({ selected: [] });
  };

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  render() {
    const { classes, data, filteredDevices } = this.props;
    const { order, orderBy, selected, rowsPerPage, page } = this.state;
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, filteredDevices.length - page * rowsPerPage);
    return (
      <Fragment>
        <TableFilter filterName={'devices'} filterFields={filterFields}/>
        <Paper className={classes.root}>
          <DevicesTableToolbar selectedDevices={data.items.filter(device => selected.includes(device.id))} devices={data}
                                handleDelete={this.handleDelete} handleEdit={this.handleEdit} handleAdd={this.props.handleAdd}
                                handleCloseToolbar={this.handleCloseToolbar}
          />
          <div className={classes.tableWrapper}>
            <Table className={classes.table} aria-labelledby="tableTitle">
              <DevicesTableHead
                numSelected={selected.length}
                order={order}
                orderBy={orderBy}
                onSelectAllClick={this.handleSelectAllClick}
                onRequestSort={this.handleRequestSort}
                rowCount={data.items.length}
                columnData={columnData}
              />
              <TableBody>
                {filteredDevices
                  .sort(getSorting(order, orderBy))
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map(device => {
                    const isSelected = this.isSelected(device.id);
                    return (
                      <TableRow
                        hover
                        onClick={event => this.handleClick(event, device.id)}
                        role="checkbox"
                        aria-checked={isSelected}
                        tabIndex={-1}
                        key={device.id}
                        selected={isSelected}
                        style={{ backgroundColor: device.activate ? "#FFFFFF" : "#bdbfbc"}}
                      >
                        <TableCell padding="checkbox" align='left'>
                          <Checkbox checked={isSelected} />
                        </TableCell>
                        <TableCell component="th" scope="row" align='left'>
                          {device.id}
                        </TableCell>
                        <TableCell className={classes.centeredCell}>{device.name}</TableCell>
                        <TableCell align='left'>{device.device_parameters['devEui']}</TableCell>
                        <TableCell align='left'>{device.messageCount}</TableCell>
                        <TableCell className={classes.centeredCell}>
                          {typeof device.lastMessage !== 'undefined' &&
                              <Tooltip classes={{
                                tooltip: classes.tooltip,
                              }} title={<React.Fragment>
                                  <div key={device.lastMessage.id}> {`@${new Date(device.lastMessage.timestamp).toLocaleString()}`} </div>
                                  <div key={device.lastMessage.id+1}> {JSON.stringify(JSON.parse(device.lastMessage.values), null, '\t')}</div>
                                  </React.Fragment>}>
                                <div>
                                  {device.lastMessage.values.length > 50 ? `${device.lastMessage.values.substring(0, 50)}...` :
                                    device.lastMessage.values}
                                </div>
                              </Tooltip>
                          }
                        </TableCell>
                        <TableCell className={classes.centeredCell}>
                          {typeof device.alerts !== 'undefined' &&
                          <Tooltip classes={{
                            tooltip: classes.tooltip,
                          }} title={<React.Fragment>{device.alerts.map(alert => { return (<div key={alert.id}> {`${alert.id}: ${alert.code} @ ${new Date(alert.timestamp).toLocaleString()}`} </div>);})}</React.Fragment>}>
                            <div>
                              {device.alerts.length > 0 ? `${device.alerts[0].code} @ ${new Date(device.alerts[0].timestamp).toLocaleString()}` : 'N/A'}
                            </div>
                          </Tooltip>
                          }
                        </TableCell>
                        <TableCell className={classes.centeredCell}>{device.description}</TableCell>
                      </TableRow>
                    );
                  })}
                {emptyRows > 0 && (
                  <TableRow style={{ height: 49 * emptyRows }}>
                    <TableCell colSpan={columnData.length + 1} />
                  </TableRow>
                )}
              </TableBody>
            </Table>
          </div>
          <TablePagination
            component="div"
            count={data.items.length}
            rowsPerPage={rowsPerPage}
            page={page}
            backIconButtonProps={{
              'aria-label': 'Previous Page',
            }}
            nextIconButtonProps={{
              'aria-label': 'Next Page',
            }}
            onChangePage={this.handleChangePage}
            onChangeRowsPerPage={this.handleChangeRowsPerPage}
          />
        </Paper>
      </Fragment>
    );
  }
}

DevicesTable.propTypes = {
  classes: PropTypes.object.isRequired,
  data: PropTypes.object.isRequired,
  handleDelete: PropTypes.func.isRequired,
  handleAdd: PropTypes.func.isRequired,
  handleEdit: PropTypes.func.isRequired
};

function mapStateToProps(state) {
  const { filters, devices } = state;
  const { hasFilters } = filterSelectors.selectFilterProps('devices', filters);
  const filteredDevices = filterSelectors.getFilteredList('devices', filters, devices.items /*, fieldValue => fieldValue.val*/);

  return {
    hasFilters,
    filteredDevices
  }
}

const connectedDevicesTable = connect(mapStateToProps, { ...filterActions })(DevicesTable);
const styledDevicesTable = withStyles(devicesTableStyle)(connectedDevicesTable);
export { styledDevicesTable as DevicesTable };