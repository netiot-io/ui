import React from 'react';
import { Toolbar, Typography, IconButton, Tooltip } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import AddDeviceIcon from '@material-ui/icons/AddBox';
import TestDecoderIcon from '@material-ui/icons/Input';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import devicesTableToolbarStyle from './devicesTableToolbarStyles.jsx';
import withStyles from '@material-ui/core/styles/withStyles';
import DeviceDeleteDialog from '../../Modals/DeviceDeleteDialog';
import { DeviceAddDialog } from '../../Modals/DeviceAddDialog';
import { DeviceTestDialog } from "../../Modals/DeviceTestDialog";
import {connect} from 'react-redux';

class DevicesTableToolbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openDelete: false,
      openAdd: false,
      openTest: false
    };
  }

  handleOpenDelete = () => {
    this.setState({ openDelete: true });
  };

  handleCloseDelete = () => {
    this.setState({ openDelete: false });
  };

  handleOpenAdd = () => {
    this.setState({ openAdd: true });
  };

  handleCloseAdd = () => {
    this.setState({ openAdd: false });
    this.props.handleCloseToolbar();
  };

  handleOpenTest = () => {
    this.setState({ openTest: true });
  };

  handleCloseTest = () => {
    this.setState({ openTest: false });
    this.props.handleCloseToolbar();
  };

  render() {
    const { selectedDevices, devices, classes, abilities } = this.props;
    const numSelected = selectedDevices.length;
    return (
      <Toolbar
        className={classNames(classes.root, {
          [classes.highlight]: numSelected > 0,
        })}
      >
        <div className={classes.title}>
          {numSelected > 0 ? (
            <Typography color="inherit" variant="subheading">
              {numSelected} selected
            </Typography>
          ) : (
            <Typography variant="title" id="tableTitle">
              Devices
            </Typography>
          )}
        </div>
        <div className={classes.spacer} />
        <div className={classes.actions}>
          {[numSelected === 1 ? (
            <Tooltip title="Edit" key='0'>
              <IconButton aria-label="Edit"  onClick={this.handleOpenAdd}>
                <EditIcon className={classes.editDevice}/>
              </IconButton>
            </Tooltip>
          ) : (null),
            numSelected === 1 ? (
              <Tooltip title="Test" key='1'>
                <IconButton aria-label="Test"  onClick={this.handleOpenTest}>
                  <TestDecoderIcon className={classes.editDevice}/>
                </IconButton>
              </Tooltip>
            ) : (null),
            numSelected > 0 && abilities.can('edit', 'devices') ? (
              <Tooltip title="Delete" key='2'>
                <IconButton aria-label="Delete" onClick={this.handleOpenDelete}>
                  <DeleteIcon className={classes.deleteDevice} />
                </IconButton>
              </Tooltip>
            ) : ( abilities.can('edit', 'devices') &&
              <Tooltip title="Add device" key='3'>
                <IconButton aria-label="Add device" onClick={this.handleOpenAdd}>
                  <AddDeviceIcon className={classes.addDevice} />
                </IconButton>
              </Tooltip>
            )]}
        </div>
        <DeviceDeleteDialog
          open={this.state.openDelete}
          close={this.handleCloseDelete}
          handleDelete={this.props.handleDelete}
          selectedDevices={selectedDevices}
          />
        {this.state.openAdd && <DeviceAddDialog
          open={this.state.openAdd}
          close={this.handleCloseAdd}
          handleAdd={this.props.handleAdd}
          handleEdit={this.props.handleEdit}
          isEdit={selectedDevices.length === 1}
          devices={devices}
          selectedDevice={selectedDevices.length === 1 ? selectedDevices[0] : null}
        />}
        <DeviceTestDialog
          open={this.state.openTest}
          close={this.handleCloseTest}
          selectedDevice={selectedDevices.length === 1 ? selectedDevices[0] : null}
        />
      </Toolbar>
    );
  }
}

DevicesTableToolbar.propTypes = {
  classes: PropTypes.object.isRequired,
  selectedDevices: PropTypes.array.isRequired,
  devices: PropTypes.object.isRequired,
  handleDelete: PropTypes.func.isRequired,
  handleAdd: PropTypes.func.isRequired,
  handleEdit: PropTypes.func.isRequired,
  handleCloseToolbar: PropTypes.func.isRequired
};

function mapStateToProps(state) {
  const { authentication} = state;
  const { abilities } = authentication;
  return {
    abilities
  };
}

const connectedDevicesTableToolbar = connect(mapStateToProps)(DevicesTableToolbar);
const styledDevicesTableToolbar = withStyles(devicesTableToolbarStyle)(connectedDevicesTableToolbar);
export { styledDevicesTableToolbar as DevicesTableToolbar };