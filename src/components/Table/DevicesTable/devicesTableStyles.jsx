const devicesTableStyle = {
  root: {
    width: '100%',
    marginTop: 2 * 3,
    marginBottom: 2 * 3,
    fontSize: '16px'
  },
  table: {
    minWidth: 1020,
    fontSize: '16px'
  },
  tableWrapper: {
    overflowX: 'auto',
    overflowY: 'hidden'
  },
  centeredCell: {
    textAlign: 'center',
    borderBottom: '1px solid rgba(224, 224, 224, 1)'
  },
  centeredHead: {
    textAlign: 'center',
    paddingRight: '24px'
  },
  tooltip: {
    maxWidth: 500
  }
};

export default devicesTableStyle;
