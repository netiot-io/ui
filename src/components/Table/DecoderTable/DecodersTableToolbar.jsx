import React from 'react';
import { Toolbar, Typography, IconButton, Tooltip } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import AddDeviceIcon from '@material-ui/icons/AddBox';
import TestDecoderIcon from '@material-ui/icons/Input';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import enhancedTableToolbarStyle from './decodersTableToolbarStyles.jsx';
import withStyles from '@material-ui/core/styles/withStyles';
import DecoderDeleteDialog from '../../Modals/DecoderDeleteDialog';
import { DecoderAddDialog } from '../../Modals/DecoderAddDialog';
import { DecoderTestDialog } from '../../Modals/DecoderTestDialog';

class DecodersTableToolbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openDelete: false,
      openAdd: false,
      openTest: false
    };
  }

  handleOpenDelete = () => {
    this.setState({ openDelete: true });
  };

  handleCloseDelete = () => {
    this.setState({ openDelete: false });
  };

  handleOpenAdd = () => {
    this.setState({ openAdd: true });
  };

  handleCloseAdd = () => {
    this.setState({ openAdd: false });
    this.props.handleCloseToolbar();
  };

  handleOpenTest = () => {
    this.setState({ openTest: true });
  };

  handleCloseTest = () => {
    this.setState({ openTest: false });
    this.props.handleCloseToolbar();
  };

  render() {
    const { selectedDecoders, decoders, classes, type, protocols } = this.props;
    const numSelected = selectedDecoders.length;
    return (
      <Toolbar
        className={classNames(classes.root, {
          [classes.highlight]: numSelected > 0,
        })}
      >
        <div className={classes.title}>
          {numSelected > 0 ? (
            <Typography color="inherit" variant="subheading">
              {numSelected} selected
            </Typography>
          ) : (
            <Typography variant="title" id="tableTitle">
              {type === 'public' ? 'Public decoders' : 'Private decoders'}
            </Typography>
          )}
        </div>
        <div className={classes.spacer} />
        <div className={classes.actions}>
          {[numSelected === 1 && type === 'private' ? (
            <Tooltip title="Edit" key='0'>
              <IconButton aria-label="Edit"  onClick={this.handleOpenAdd}>
                <EditIcon className={classes.editDevice}/>
              </IconButton>
            </Tooltip>
          ) : (null),
            numSelected === 1 ? (
              <Tooltip title="Test" key='1'>
                <IconButton aria-label="Test"  onClick={this.handleOpenTest}>
                  <TestDecoderIcon className={classes.editDevice}/>
                </IconButton>
              </Tooltip>
            ) : (null),
            numSelected > 0 && type === 'private' ? (
              <Tooltip title="Delete" key='2'>
                <IconButton aria-label="Delete" onClick={this.handleOpenDelete}>
                  <DeleteIcon className={classes.deleteDevice} />
                </IconButton>
              </Tooltip>
            ) : type === 'private' && (
              <Tooltip title="Add decoder" key='3'>
                <IconButton aria-label="Add decoder" onClick={this.handleOpenAdd}>
                  <AddDeviceIcon className={classes.addDevice} />
                </IconButton>
              </Tooltip>
            )]}
        </div>
        <DecoderDeleteDialog
          open={this.state.openDelete}
          close={this.handleCloseDelete}
          handleDelete={this.props.handleDelete}
          selectedDecoders={selectedDecoders}
          />
        <DecoderAddDialog
          open={this.state.openAdd}
          close={this.handleCloseAdd}
          handleAdd={this.props.handleAdd}
          handleEdit={this.props.handleEdit}
          isEdit={selectedDecoders.length === 1}
          decoders={decoders}
          protocols={protocols}
          selectedDecoders={selectedDecoders.length === 1 ? selectedDecoders[0] : null}
        />
        <DecoderTestDialog
          open={this.state.openTest}
          close={this.handleCloseTest}
          selectedDecoders={selectedDecoders.length === 1 ? selectedDecoders[0] : null}
        />
      </Toolbar>
    );
  }
}

DecodersTableToolbar.propTypes = {
  classes: PropTypes.object.isRequired,
  selectedDecoders: PropTypes.array.isRequired,
  decoders: PropTypes.array.isRequired,
  protocols: PropTypes.array.isRequired,
  handleDelete: PropTypes.func.isRequired,
  handleAdd: PropTypes.func.isRequired,
  handleEdit: PropTypes.func.isRequired,
  handleCloseToolbar: PropTypes.func.isRequired
};

export default withStyles(enhancedTableToolbarStyle)(DecodersTableToolbar);