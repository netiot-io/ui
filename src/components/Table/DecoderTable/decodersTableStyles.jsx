const enhancedTableStyle = {
  root: {
    width: '100%',
    marginTop: 2 * 3,
    marginBottom: 2 * 3,
    fontSize: '16px'
  },
  table: {
    minWidth: 1020,
    fontSize: '16px'
  },
  tableWrapper: {
    overflowX: 'auto',
    overflowY: 'hidden'
  },
};

export default enhancedTableStyle;
