import React, {Fragment} from 'react';
import {Table, TablePagination , TableCell, TableBody, TableRow, Checkbox, Paper} from '@material-ui/core';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import enhancedTableStyle from './decodersTableStyles.jsx';
import DecodersTableHead from './DecodersTableHead';
import DecodersTableToolbar from './DecodersTableToolbar';
import { filterSelectors, filterActions } from 'material-ui-filter';
import { connect } from 'react-redux';
import { TableFilter} from "../../Modals/TableFilter";

function getSorting(order, orderBy) {
  return order === 'desc'
    ? (a, b) => (b[orderBy] < a[orderBy] ? -1 : 1)
    : (a, b) => (a[orderBy] < b[orderBy] ? -1 : 1);
}

const columnData = [
  { id: 'id', align: "left", disablePadding: false, label: 'ID' },
  { id: 'name', align: "left", disablePadding: false, label: 'Name' },
  { id: 'protocol', align: "left", disablePadding: false, label: 'Protocol' },
  { id: 'description', align: "left", disablePadding: false, label: 'Description' },
];

// ONLY used if we display the filter drawer (see demo https://tarikhuber.github.io/material-ui-filter/)
const filterFields = [
  { name: 'name', label: 'Name' },
  { name: 'description', label: 'Description' },
];

class DecodersTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      order: 'asc',
      orderBy: 'id',
      selected: [],
      page: 0,
      rowsPerPage: 5,
    };
  }

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    this.setState({ order, orderBy });
  };

  handleSelectAllClick = (event, checked) => {
    if (checked) {
      this.setState((prevState, props) => {
        return { selected: props.data.map(n => n.id) };
      });
      return;
    }
    this.setState({ selected: [] });
  };

  handleClick = (event, id) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    this.setState({ selected: newSelected });
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  handleDelete = () => {
    this.props.handleDelete(this.state.selected);
    this.setState({ selected: [] });
  };

  handleEdit = (sensor) => {
    this.props.handleEdit(sensor);
  };

  handleCloseToolbar = () => {
    this.setState({ selected: [] });
  };

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  render() {
    const { classes, data, type, protocols,  filters } = this.props;
    const { order, orderBy, selected, rowsPerPage, page } = this.state;
    const filteredDecoders = filterSelectors.getFilteredList('decoders', filters, data /*, fieldValue => fieldValue.val*/);
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, filteredDecoders.length - page * rowsPerPage);
    return (
      <Fragment>
        <TableFilter filterName={'decoders'} filterFields={filterFields}/>
        <Paper className={classes.root}>
          <DecodersTableToolbar selectedDecoders={filteredDecoders.filter(decoder => selected.includes(decoder.id))} decoders={filteredDecoders}
                                handleDelete={this.handleDelete} handleEdit={this.handleEdit} handleAdd={this.props.handleAdd}
                                handleCloseToolbar={this.handleCloseToolbar} type={type} protocols={protocols}
          />
          <div className={classes.tableWrapper}>
            <Table className={classes.table} aria-labelledby="tableTitle">
              <DecodersTableHead
                numSelected={selected.length}
                order={order}
                orderBy={orderBy}
                onSelectAllClick={this.handleSelectAllClick}
                onRequestSort={this.handleRequestSort}
                rowCount={filteredDecoders.length}
                columnData={columnData}
              />
              <TableBody>
                {filteredDecoders
                  .sort(getSorting(order, orderBy))
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map(currentDecoder => {
                    const isSelected = this.isSelected(currentDecoder.id);
                    return (
                      <TableRow
                        hover
                        onClick={event => this.handleClick(event, currentDecoder.id)}
                        role="checkbox"
                        aria-checked={isSelected}
                        tabIndex={-1}
                        key={currentDecoder.id}
                        selected={isSelected}
                      >
                        <TableCell padding="checkbox" align="left">
                          <Checkbox checked={isSelected} />
                        </TableCell>
                        <TableCell component="th" scope="row" align="left">
                          {currentDecoder.id}
                        </TableCell>
                        <TableCell align="left">{currentDecoder.name}</TableCell>
                        <TableCell align="left">{protocols.find(protocol => protocol.id === currentDecoder.protocol_id).name}</TableCell>
                        <TableCell align="left">{currentDecoder.description}</TableCell>
                      </TableRow>
                    );
                  })}
                {emptyRows > 0 && (
                  <TableRow style={{ height: 49 * emptyRows }}>
                    <TableCell colSpan={columnData.length + 1} />
                  </TableRow>
                )}
              </TableBody>
            </Table>
          </div>
          <TablePagination
            component="div"
            count={filteredDecoders.length}
            rowsPerPage={rowsPerPage}
            page={page}
            backIconButtonProps={{
              'aria-label': 'Previous Page',
            }}
            nextIconButtonProps={{
              'aria-label': 'Next Page',
            }}
            onChangePage={this.handleChangePage}
            onChangeRowsPerPage={this.handleChangeRowsPerPage}
          />
      </Paper>
      </Fragment>
    );
  }
}

DecodersTable.propTypes = {
  classes: PropTypes.object.isRequired,
  data: PropTypes.array.isRequired,
  protocols: PropTypes.array.isRequired,
  handleDelete: PropTypes.func.isRequired,
  handleAdd: PropTypes.func.isRequired,
  handleEdit: PropTypes.func.isRequired
};

function mapStateToProps(state) {
  const { filters } = state;
  const { hasFilters } = filterSelectors.selectFilterProps('decoders', filters);

  return {
    filters,
    hasFilters
  }
}

const connectedDecodersTable = connect(mapStateToProps, { ...filterActions })(DecodersTable);
const styledDecodersTable = withStyles(enhancedTableStyle)(connectedDecodersTable);
export { styledDecodersTable as DecodersTable };