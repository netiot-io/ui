import React from 'react';
import {Table, TablePagination , TableCell, TableBody, TableRow, Checkbox, Paper, Tooltip } from '@material-ui/core';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import enhancedTableStyle from './organizationsTableStyles.jsx';
import OrganizationsTableHead from './OrganizationsTableHead';
import OrganizationsTableToolbar from './OrganizationsTableToolbar';
import { withRouter } from 'react-router-dom';

function getSorting(order, orderBy) {
  return order === 'desc'
    ? (a, b) => (b[orderBy] < a[orderBy] ? -1 : 1)
    : (a, b) => (a[orderBy] < b[orderBy] ? -1 : 1);
}

const columnData = [
  { id: 'id', align: "left", disablePadding: false, label: 'ID' },
  { id: 'name', align: "left", disablePadding: false, label: 'Name' },
  { id: 'phone', align: "left", disablePadding: false, label: 'Phone' },
  { id: 'address', align: "left", disablePadding: false, label: 'Address' },
  { id: 'users', align: "left", disablePadding: false, label: 'Users' },
  { id: 'apps', align: "left", disablePadding: false, label: 'Apps' },
  { id: 'description', align: "left", disablePadding: false, label: 'Description' },
];

class OrganizationsTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      order: 'asc',
      orderBy: 'id',
      selected: [],
      page: 0,
      rowsPerPage: 5,
    };
  }

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    this.setState({ order, orderBy });
  };

  handleSelectAllClick = (event, checked) => {
    if (checked) {
      this.setState((prevState, props) => {
        return { selected: props.data.organizations.map(n => n.id) };
      });
      return;
    }
    this.setState({ selected: [] });
  };

  handleClick = (event, id) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    this.setState({ selected: newSelected });
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  handleDelete = () => {
    this.props.handleDelete(this.state.selected);
    this.setState({ selected: [] });
  };

  handleEdit = (sensor) => {
    this.props.handleEdit(sensor);
  };

  handleCloseToolbar = () => {
    this.setState({ selected: [] });
  };

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  render() {
    const { classes, data, applications } = this.props;
    const { order, orderBy, selected, rowsPerPage, page } = this.state;
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, data.length - page * rowsPerPage);
    const organizationsData = data.organizations;
    return (
      <Paper className={classes.root}>
        <OrganizationsTableToolbar selectedOrganizations={organizationsData.filter(organization => selected.includes(organization.id))} organizations={organizationsData}
                              handleDelete={this.handleDelete} handleEdit={this.handleEdit} handleAdd={this.props.handleAdd}
                              handleCloseToolbar={this.handleCloseToolbar}
        />
        <div className={classes.tableWrapper}>
          <Table className={classes.table} aria-labelledby="tableTitle">
            <OrganizationsTableHead
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={this.handleSelectAllClick}
              onRequestSort={this.handleRequestSort}
              rowCount={organizationsData.length}
              columnData={columnData}
            />
            <TableBody>
              {organizationsData
                .sort(getSorting(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map(organization => {
                  const isSelected = this.isSelected(organization.id);
                  return (
                    <TableRow
                      hover
                      onClick={event => this.handleClick(event, organization.id)}
                      role="checkbox"
                      aria-checked={isSelected}
                      tabIndex={-1}
                      key={organization.id}
                      selected={isSelected}
                    >
                      <TableCell padding="checkbox" align="left">
                        <Checkbox checked={isSelected} />
                      </TableCell>
                      <TableCell component="th" scope="row" align="left">
                        {organization.id}
                      </TableCell>
                      <TableCell align="left">{organization.name}</TableCell>
                      <TableCell align="left">{organization.phone === null ? 'N/A' : organization.phone}</TableCell>
                      <TableCell align="left">{organization.address === null ? 'N/A' : organization.address}</TableCell>
                      <TableCell align="left">{typeof data.items === 'undefined' ? 'N/A' : data.items.filter(user => user.organization_id === organization.id).length}</TableCell>
                      <TableCell align="left" onClick={() => this.props.history.push(`${process.env.REACT_APP_DOMAIN}applications?filterBy=${organization.name}`)}>
                        <Tooltip classes={{
                          tooltip: classes.tooltip,
                        }} title={<React.Fragment>
                          {applications.filter(app => typeof app.organizations.find(organizationId => organizationId === organization.id) !== 'undefined').map(app => (
                            <div key={app.id}>{app.name}</div>
                            ))}
                        </React.Fragment>}>
                          <div>
                            {applications.filter(app => typeof app.organizations.find(organizationId => organizationId === organization.id) !== 'undefined').length}
                          </div>
                        </Tooltip>
                      </TableCell>
                      <TableCell align="left">{organization.description}</TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 49 * emptyRows }}>
                  <TableCell colSpan={columnData.length + 1} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </div>
        <TablePagination
          component="div"
          count={organizationsData.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            'aria-label': 'Previous Page',
          }}
          nextIconButtonProps={{
            'aria-label': 'Next Page',
          }}
          onChangePage={this.handleChangePage}
          onChangeRowsPerPage={this.handleChangeRowsPerPage}
        />
      </Paper>
    );
  }
}

OrganizationsTable.propTypes = {
  classes: PropTypes.object.isRequired,
  data: PropTypes.object.isRequired,
  applications: PropTypes.array.isRequired,
  handleDelete: PropTypes.func.isRequired,
  handleAdd: PropTypes.func.isRequired,
  handleEdit: PropTypes.func.isRequired
};

export default withStyles(enhancedTableStyle)(withRouter(OrganizationsTable));