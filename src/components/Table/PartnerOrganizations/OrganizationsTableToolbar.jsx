import React from 'react';
import { Toolbar, Typography, IconButton, Tooltip } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import AddDeviceIcon from '@material-ui/icons/AddBox';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import enhancedTableToolbarStyle from './organizationsTableToolbarStyles.jsx';
import withStyles from '@material-ui/core/styles/withStyles';
import OrganizationsDeleteDialog from '../../Modals/OrganizationsDeleteDialog';
import { OrganizationsAddDialog } from '../../Modals/OrganizationsAddDialog';

class OrganizationsTableToolbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openDelete: false,
      openAdd: false,
    };
  }

  handleOpenDelete = () => {
    this.setState({ openDelete: true });
  };

  handleCloseDelete = () => {
    this.setState({ openDelete: false });
  };

  handleOpenAdd = () => {
    this.setState({ openAdd: true });
  };

  handleCloseAdd = () => {
    this.setState({ openAdd: false });
    this.props.handleCloseToolbar();
  };

  render() {
    const { selectedOrganizations, organizations, classes } = this.props;
    const numSelected = selectedOrganizations.length;
    return (
      <Toolbar
        className={classNames(classes.root, {
          [classes.highlight]: numSelected > 0,
        })}
      >
        <div className={classes.title}>
          {numSelected > 0 ? (
            <Typography color="inherit" variant="subheading">
              {numSelected} selected
            </Typography>
          ) : (
            <Typography variant="title" id="tableTitle">
              Organizations
            </Typography>
          )}
        </div>
        <div className={classes.spacer} />
        <div className={classes.actions}>
          {[numSelected === 1 ? (
            <Tooltip title="Edit" key='0'>
              <IconButton aria-label="Edit"  onClick={this.handleOpenAdd}>
                <EditIcon className={classes.editDevice}/>
              </IconButton>
            </Tooltip>
          ) : (null),
            numSelected > 0 ? (
              <Tooltip title="Delete" key='1'>
                <IconButton aria-label="Delete" onClick={this.handleOpenDelete}>
                  <DeleteIcon className={classes.deleteDevice} />
                </IconButton>
              </Tooltip>
            ) : (
              <Tooltip title="Add organization" key='2'>
                <IconButton aria-label="Add organization" onClick={this.handleOpenAdd}>
                  <AddDeviceIcon className={classes.addDevice} />
                </IconButton>
              </Tooltip>
            )]}
        </div>
        <OrganizationsDeleteDialog
          open={this.state.openDelete}
          close={this.handleCloseDelete}
          handleDelete={this.props.handleDelete}
          selectedOrganizations={selectedOrganizations}
          />
        <OrganizationsAddDialog
          open={this.state.openAdd}
          close={this.handleCloseAdd}
          handleAdd={this.props.handleAdd}
          handleEdit={this.props.handleEdit}
          isEdit={selectedOrganizations.length === 1}
          organizations={organizations}
          selectedOrganization={selectedOrganizations.length === 1 ? selectedOrganizations[0] : null}
        />
      </Toolbar>
    );
  }
}

OrganizationsTableToolbar.propTypes = {
  classes: PropTypes.object.isRequired,
  selectedOrganizations: PropTypes.array.isRequired,
  organizations: PropTypes.array.isRequired,
  handleDelete: PropTypes.func.isRequired,
  handleAdd: PropTypes.func.isRequired,
  handleEdit: PropTypes.func.isRequired,
  handleCloseToolbar: PropTypes.func.isRequired
};

export default withStyles(enhancedTableToolbarStyle)(OrganizationsTableToolbar);