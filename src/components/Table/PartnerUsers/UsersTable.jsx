import React, { Fragment } from 'react';
import {Table, TablePagination , TableCell, TableBody, TableRow, Checkbox, Paper} from '@material-ui/core';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import enhancedTableStyle from './usersTableStyles.jsx';
import UsersTableHead from './UsersTableHead';
import UsersTableToolbar from './UsersTableToolbar';
import { connect } from 'react-redux';
import { TableFilter} from "../../Modals/TableFilter";
import { filterSelectors, filterActions } from 'material-ui-filter';

function getSorting(order, orderBy) {
  return order === 'desc'
    ? (a, b) => (b[orderBy] < a[orderBy] ? -1 : 1)
    : (a, b) => (a[orderBy] < b[orderBy] ? -1 : 1);
}

const columnData = [
  { id: 'id', align: "left", disablePadding: false, label: 'ID' },
  { id: 'name', align: "left", disablePadding: false, label: 'Name' },
  { id: 'phone', align: "left", disablePadding: false, label: 'Phone' },
  { id: 'email', align: "left", disablePadding: false, label: 'Email' },
  { id: 'address', align: "left", disablePadding: false, label: 'Address' },
  { id: 'role', align: "left", disablePadding: false, label: 'Role' },
  { id: 'organization', align: "left", disablePadding: false, label: 'Organization' },
];

// ONLY used if we display the filter drawer (see demo https://tarikhuber.github.io/material-ui-filter/)
const filterFields = [
  { name: 'name', label: 'Name' }
];

class UsersTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      order: 'asc',
      orderBy: 'id',
      selected: [],
      page: 0,
      rowsPerPage: 5,
    };
  }

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    this.setState({ order, orderBy });
  };

  handleSelectAllClick = (event, checked) => {
    if (checked) {
      this.setState((prevState, props) => {
        return { selected: props.data.items.map(n => n.id) };
      });
      return;
    }
    this.setState({ selected: [] });
  };

  handleClick = (event, id) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    this.setState({ selected: newSelected });
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  handleDelete = () => {
    this.props.handleDelete(this.state.selected);
    this.setState({ selected: [] });
  };

  handleEdit = (sensor) => {
    this.props.handleEdit(sensor);
  };

  handleCloseToolbar = () => {
    this.setState({ selected: [] });
  };

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  render() {
    const { classes, filteredUsers, user, abilities, users } = this.props;
    const { order, orderBy, selected, rowsPerPage, page } = this.state;
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, filteredUsers.length - page * rowsPerPage);
    return (
      <Fragment>
        <TableFilter filterName={'users'} filterFields={filterFields}/>
        <Paper className={classes.root}>
          <UsersTableToolbar selectedUsers={filteredUsers.filter(user => selected.includes(user.id))} users={users}
                                handleDelete={this.handleDelete} handleEdit={this.handleEdit} handleAdd={this.props.handleAdd}
                                handleCloseToolbar={this.handleCloseToolbar}
          />
          <div className={classes.tableWrapper}>
            <Table className={classes.table} aria-labelledby="tableTitle">
              <UsersTableHead
                numSelected={selected.length}
                order={order}
                orderBy={orderBy}
                onSelectAllClick={this.handleSelectAllClick}
                onRequestSort={this.handleRequestSort}
                rowCount={filteredUsers.length}
                columnData={columnData}
              />
              <TableBody>
                {filteredUsers
                  .sort(getSorting(order, orderBy))
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map(currentUser => {
                    const isSelected = this.isSelected(currentUser.id);
                    return (
                      <TableRow
                        hover
                        onClick={event => this.handleClick(event, currentUser.id)}
                        role="checkbox"
                        aria-checked={isSelected}
                        tabIndex={-1}
                        key={currentUser.id}
                        selected={isSelected}
                      >
                        <TableCell padding="checkbox" align="left">
                          <Checkbox checked={isSelected} />
                        </TableCell>
                        <TableCell component="th" scope="row" align="left">
                          {currentUser.id}
                        </TableCell>
                        <TableCell align="left">{currentUser.first_name + ' ' + currentUser.last_name}</TableCell>
                        <TableCell align="left">{currentUser.phone_number}</TableCell>
                        <TableCell align="left">{currentUser.email}</TableCell>
                        <TableCell align="left">{currentUser.address}</TableCell>
                        <TableCell align="left">{currentUser.role.replace('_', ' ').toLowerCase().split(' ').map((s) => s.charAt(0).toUpperCase() + s.substring(1)).join(' ')}</TableCell>
                        { abilities.can('view', 'organizations') ? (
                            typeof users.organizations !== 'undefined' && <TableCell align="left">{users.organizations.find(organization => organization.id === parseInt(currentUser.organization_id, 10)).name}</TableCell>
                          ) : (
                            <TableCell align="left">{user.organizationName}</TableCell>
                          )
                        }
                      </TableRow>
                    );
                  })}
                {emptyRows > 0 && (
                  <TableRow style={{ height: 49 * emptyRows }}>
                    <TableCell colSpan={columnData.length + 1} />
                  </TableRow>
                )}
              </TableBody>
            </Table>
          </div>
          <TablePagination
            component="div"
            count={filteredUsers.length}
            rowsPerPage={rowsPerPage}
            page={page}
            backIconButtonProps={{
              'aria-label': 'Previous Page',
            }}
            nextIconButtonProps={{
              'aria-label': 'Next Page',
            }}
            onChangePage={this.handleChangePage}
            onChangeRowsPerPage={this.handleChangeRowsPerPage}
          />
        </Paper>
      </Fragment>
    );
  }
}

UsersTable.propTypes = {
  classes: PropTypes.object.isRequired,
  handleDelete: PropTypes.func.isRequired,
  handleAdd: PropTypes.func.isRequired,
  handleEdit: PropTypes.func.isRequired
};

function mapStateToProps(state) {
  const { authentication, users, filters } = state;
  const { user, abilities } = authentication;
  const { hasFilters } = filterSelectors.selectFilterProps('users', filters);
  const filteredUsers = filterSelectors.getFilteredList('users', filters, users.items /*, fieldValue => fieldValue.val*/);
  return {
    user,
    users,
    abilities,
    hasFilters,
    filteredUsers
  };
}

const connectedUsersTable = connect(mapStateToProps, { ...filterActions })(UsersTable);
const styledUsersTable = withStyles(enhancedTableStyle)(connectedUsersTable);
export { styledUsersTable as UsersTable };