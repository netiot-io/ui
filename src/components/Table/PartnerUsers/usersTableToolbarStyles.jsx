const enhancedTableToolbarStyle = {
    root: {
      paddingRight: 2,
    },
    highlight:
      {
        color: 'black',
        backgroundColor: '#fcc7c7',
      },
  spacer: {
    flex: '1 1 100%',
  },
  actions: {
    color: '#fcc7c7',
    display: 'flex',
    flexDirection: 'row'
  },
  title: {
    flex: '0 0 auto',
  },
  addDevice: {
    color: 'green',
    fontSize: '2.5rem'
  },
  editDevice: {
      color: '#2aa0da'
  },
  deleteDevice: {
      color: '#f05f50'
  },
  modal: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)'
  },
  paper: {
    position: 'absolute',
    width: '20%',
    backgroundColor: 'white',
    boxShadow: 5
  },
  modalTitle: {
    backgroundColor: '#fcc7c7',
    fontSize: "1.3125rem",
    fontWeight: 500,
    fontFamily: ["Roboto", "Helvetica", "Arial", "sans-serif"],
    lineHeight: "1.16667em",
    color: "#f05f50",
    padding: '10px 8px',
    marginBottom: '10px'
  },
  modalText: {
      padding: '0 8px'
  },
  button: {
    margin: '5px',
  }
};

export default enhancedTableToolbarStyle;
