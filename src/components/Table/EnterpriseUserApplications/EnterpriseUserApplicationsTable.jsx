import React, {Fragment} from 'react';
import {Table, TablePagination , TableCell, TableBody, TableRow, Checkbox, Paper } from '@material-ui/core';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import enhancedTableStyle from './enterpriseUserApplicationsTableStyles.jsx';
import EnterpriseUserApplicationsTableHead from './EnterpriseUserApplicationsTableHead';
import { filterSelectors, filterActions } from 'material-ui-filter';
import { connect } from 'react-redux';
import { TableFilter} from "../../Modals/TableFilter";
import EnterpriseApplicationsTableToolbar from './EnterpriseApplicationsTableToolbar';

function getSorting(order, orderBy) {
  return order === 'desc'
    ? (a, b) => (b[orderBy] < a[orderBy] ? -1 : 1)
    : (a, b) => (a[orderBy] < b[orderBy] ? -1 : 1);
}

const columnData = [
  { id: 'id', align: "left", disablePadding: false, label: 'ID' },
  { id: 'name', align: "left", disablePadding: false, label: 'Name' },
  { id: 'preview', align: "left", disablePadding: false, label: 'Preview' },
  { id: 'description', align: "left", disablePadding: false, label: 'description' },
];

const filterFields = [
  { name: 'name', label: 'Name' },
  { name: 'description', label: 'Description' },
];

class EnterpriseUserApplicationsTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      order: 'asc',
      orderBy: 'id',
      selected: [],
      page: 0,
      rowsPerPage: 5,
    };

    this.appRef = React.createRef();
  }

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    this.setState({ order, orderBy });
  };

  handleClick = (event, id) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    this.setState({ selected: newSelected });
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  render() {
    const { classes, filteredApps, applications } = this.props;
    const { order, orderBy, selected, rowsPerPage, page } = this.state;
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, filteredApps.length - page * rowsPerPage);
    return (
      <Fragment>
        <TableFilter filterName={'enterpriseUserApplications'} filterFields={filterFields}/>
        <Paper className={classes.root}>
          <EnterpriseApplicationsTableToolbar applications={applications} selectedApplications={filteredApps.filter(application => selected.includes(application.id))}
                                    appElement={this.appRef} canEdit={false}
          />
          <div className={classes.tableWrapper}>
            <Table className={classes.table} aria-labelledby="tableTitle">
              <EnterpriseUserApplicationsTableHead
                numSelected={selected.length}
                order={order}
                orderBy={orderBy}
                onSelectAllClick={this.handleSelectAllClick}
                onRequestSort={this.handleRequestSort}
                rowCount={filteredApps.length}
                columnData={columnData}
              />
              <TableBody>
                {filteredApps
                  .sort(getSorting(order, orderBy))
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map(application => {
                    const isSelected = this.isSelected(application.id);
                    return (
                      <TableRow
                        hover
                        onClick={event => this.handleClick(event, application.id)}
                        role="checkbox"
                        aria-checked={isSelected}
                        tabIndex={-1}
                        key={application.id}
                        selected={isSelected}
                      >
                        <TableCell padding="checkbox" align="left">
                          <Checkbox checked={isSelected} />
                        </TableCell>
                        <TableCell component="th" scope="row" align="left">
                          {application.id}
                        </TableCell>
                        <TableCell align="left">{application.name}</TableCell>
                        <TableCell align="left"><img src={`data:image/png;base64,${application.application_type_thumbnail}`} style={{height: 100}} alt={application.name}/></TableCell>
                        <TableCell align="left">{application.description}</TableCell>
                      </TableRow>
                    );
                  })}
                {emptyRows > 0 && (
                  <TableRow style={{ height: 49 * emptyRows }}>
                    <TableCell colSpan={columnData.length + 1} />
                  </TableRow>
                )}
              </TableBody>
            </Table>
          </div>
          <TablePagination
            component="div"
            count={filteredApps.length}
            rowsPerPage={rowsPerPage}
            page={page}
            backIconButtonProps={{
              'aria-label': 'Previous Page',
            }}
            nextIconButtonProps={{
              'aria-label': 'Next Page',
            }}
            onChangePage={this.handleChangePage}
            onChangeRowsPerPage={this.handleChangeRowsPerPage}
          />
          <div id="custom-app" ref={this.appRef} />
        </Paper>
      </Fragment>
    );
  }
}

EnterpriseUserApplicationsTable.propTypes = {
  classes: PropTypes.object.isRequired,
  data: PropTypes.array.isRequired
};

function mapStateToProps(state) {
  const { filters, applications } = state;
  const { hasFilters } = filterSelectors.selectFilterProps('enterpriseUserApplications', filters);
  const filteredApps = filterSelectors.getFilteredList('enterpriseUserApplications', filters, applications.applications /*, fieldValue => fieldValue.val*/);

  return {
    hasFilters,
    filteredApps,
    applications
  }
}

const connectedEnterpriseUserApplicationsTable = connect(mapStateToProps, { ...filterActions })(EnterpriseUserApplicationsTable);
const styledEnterpriseUserApplicationsTable = withStyles(enhancedTableStyle)(connectedEnterpriseUserApplicationsTable);
export { styledEnterpriseUserApplicationsTable as EnterpriseUserApplicationsTable };