/* eslint no-eval: 0 */

import React from 'react';
import { Toolbar, Typography, IconButton, Tooltip, CircularProgress } from '@material-ui/core';
import LaunchIcon from '@material-ui/icons/Launch';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import enhancedEnterpriseTableToolbarStyle from './enterpriseApplicationsTableToolbarStyles.jsx';
import withStyles from '@material-ui/core/styles/withStyles';
import {authHeader} from "../../../utils/auth-header";
import {applicationActions} from "../../../actions";
import {connect} from 'react-redux';
import { env } from '../../../utils/config';

class EnterpriseApplicationsTableToolbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      progress: false
    };
  }

  handleLaunchApp = async () => {
    this.setState({progress: true});
    await this.props.dispatch(applicationActions.getApplicationConfig(
      this.props.selectedApplications[0].application_path,
      this.props.selectedApplications[0].id));
    this.loadApp();
  };

  loadApp() {
    const requestOptions = {
      method: 'GET',
      headers: authHeader()
    };
    fetch(`${env.REACT_APP_SERVER_BASE_PATH}${this.props.selectedApplications[0].application_path}${env.REACT_APP_SERVER_APP_DATA}`, requestOptions)
      .then(response => response.text())
      .then((js) => {
        const script = document.createElement("script");
        script.setAttribute('id', 'tempApp');
        script.setAttribute('type', 'text/javascript');

        const appConfig = {
          appId: this.props.selectedApplications[0].id,
          data: this.props.applications.applicationConfig.data
        };
        script.setAttribute('appConfig', JSON.stringify(appConfig).replace(/"/g, '\''));
        script.setAttribute('div-id', 'custom-app');
        script.innerText = js;

        document.body.appendChild(script);
        (function(){
          const e = eval;
          e(document.getElementById('tempApp').innerHTML);
        })();
        this.setState({progress: false});
        window.scrollTo({ behavior: 'smooth', top: this.props.appElement.current.offsetTop });
      })
      .catch(e => {
        console.log(e);
        this.setState({progress: false});
      });
  }


  render() {
    const { selectedApplications, classes } = this.props;
    const numSelected = selectedApplications.length;
    return (
      <Toolbar
        className={classNames(classes.root, {
          [classes.highlight]: numSelected > 0,
        })}
      >
        <div className={classes.title}>
          {numSelected > 0 ? (
            <Typography color="inherit" variant="subheading">
              {numSelected} selected
            </Typography>
          ) : (
            <Typography variant="title" id="tableTitle">
              Applications
            </Typography>
          )}
        </div>
        <div className={classes.spacer} />
        <div>
          {this.state.progress && <CircularProgress /> }
        </div>
        <div className={classes.actions}>
          {[numSelected === 1 ? (
              <Tooltip title="Launch" key='3'>
                <IconButton aria-label="Launch" onClick={this.handleLaunchApp}>
                  <LaunchIcon className={classes.editDevice}/>
                </IconButton>
              </Tooltip>
          ) : (null)]}
        </div>
      </Toolbar>
    );
  }
}

EnterpriseApplicationsTableToolbar.propTypes = {
  classes: PropTypes.object.isRequired,
  applications: PropTypes.object.isRequired,
  selectedApplications: PropTypes.array.isRequired,
  appElement: PropTypes.object.isRequired
};

const connectedEnterpriseApplicationsTableToolbar = connect()(EnterpriseApplicationsTableToolbar);
export default withStyles(enhancedEnterpriseTableToolbarStyle)(connectedEnterpriseApplicationsTableToolbar);