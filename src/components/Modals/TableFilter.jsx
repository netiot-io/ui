import React from 'react';
import {TextField, AppBar, Toolbar, Icon, Paper} from '@material-ui/core';
import modalStyle from './modalStyles';
import withStyles from '@material-ui/core/styles/withStyles';
import {FilterDrawer, filterActions} from 'material-ui-filter';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

class TableFilter extends React.Component {

  render() {
    const { classes, filterName, filterFields, setSearch, defaultValue } = this.props;
    return (
      <Paper>
        <FilterDrawer
          name={'decoders'}
          fields={filterFields}
        />
        <AppBar position="static" className={classes.appBar}>
          <Toolbar>
            <div style={{ display: 'flex' }}>
              <div style={{ width: 'calc(100% - 84px)' }}>
                <div style={{
                  display: 'inline-block',
                  backgroundColor: 'transparent',
                  borderRadius: 5,
                  width: 600,
                  maxWidth: '100%'
                }}
                >
                  <div style={{
                    display: 'flex',
                    borderRadius: 4,
                    paddingLeft: 10,
                    paddingRight: 10
                  }}
                  >
                    <Icon style={{ marginLeft: 10, marginTop: 12, marginRight: 15 }} >search</Icon>
                    <TextField
                      style={{ width: '100%' }}
                      onChange={(e) => {
                        setSearch(filterName, e.target.value);
                      }}
                      defaultValue={defaultValue}
                    />
                  </div>
                </div>
              </div>

            </div>
          </Toolbar>
        </AppBar>
      </Paper>
    )
  }
}

TableFilter.propTypes = {
  filterName: PropTypes.string.isRequired,
  filterFields: PropTypes.array.isRequired,
  defaultValue: PropTypes.string
};

function mapStateToProps(state) {
  const { filters } = state;

  return {
    filters
  }
}

const connectedTableFilter = connect(mapStateToProps, { ...filterActions })(TableFilter);
const styledTableFilter = withStyles(modalStyle)(connectedTableFilter);
export { styledTableFilter as TableFilter };