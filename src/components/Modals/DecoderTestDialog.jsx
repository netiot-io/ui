/* eslint no-eval: 0 */
import React, {Fragment} from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
} from '@material-ui/core';
import modalStyle from './modalStyles';
import withStyles from '@material-ui/core/styles/withStyles';
import PropTypes from "prop-types";
import {connect} from 'react-redux';

const hexRegex = /^[-+]?[0-9A-Fa-f]+\.?[0-9A-Fa-f]*?$/;

class DecoderTestDialog extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      payload: '',
      code: '',
      decoderResult: '',
      payloadValid: true,
      formErrors: {
        payload: 'The payload is not correct'
      },
      openAlert: false
    };
    this.handleTest = this.handleTest.bind(this);
  }

  handleChange = property => event => {
    let value = event.target.value;
    switch(property) {
      case 'payload':
        value = value.trim();
        if (value.length === 0) {
          this.setState({payloadValid: false});
        } else {
          if (hexRegex.test(value.slice(-1))) {
            value = value.replace(/\s/g, '').replace(/(.{2})/g, "$1 ").trim().toUpperCase();
            this.setState({[property]: value});
            this.setState({payloadValid: true});
          }
        }
        break;
      default:
        console.log('Unknown property edit');
    }
  };

  handleTest = () => {
    const bytesPayload = this.state.payload.replace(/\s/g, '').match(/.{2}/g).map(b => parseInt(b.replace(/^#/, ''), 16));
    // eslint-disable-next-line
    const jsFunc = new Function("return " + this.state.code)();
    this.setState({decoderResult: JSON.stringify(JSON.parse(jsFunc(bytesPayload)), null, '\t')});
  };

  handleClose = () => {
    this.props.close();
    this.clearForm();
  };

  clearForm = () => {
    this.setState({
      payload: '',
      code: '',
      decoderResult: '',
      payloadValid: true
    })
  };

  componentWillReceiveProps = nextProps => {
    const {alerts} = nextProps;
    if (alerts.length > 0 && alerts[alerts.length - 1].type === 'success' && nextProps.open) {
      this.handleClose();
    } else if (nextProps.selectedDecoders && !nextProps.selectedDecoders.hasOwnProperty('updating')) {
      this.setState({
        code: nextProps.selectedDecoders.code
      });
    }
  };

  render() {
    const { open, classes } = this.props;
    return (<Fragment>
        <Dialog
          disableBackdropClick
          disableEscapeKeyDown
          maxWidth="sm"
          aria-labelledby="confirmation-dialog-title"
          open={open}
          onClose={this.handleClose}
          classes={{
            paper: classes.paper,
          }}
        >
          <DialogTitle id="confirmation-dialog-title">{'Test decoder'}</DialogTitle>
          <DialogContent>
            <form id="addDecoderForm" className="addDeviceForm" onSubmit={this.handleSubmit}>
              <div className={classes.container}>
                <TextField
                  autoFocus={true}
                  error={!this.state.payloadValid}
                  id="decoderPayload"
                  label="Payload"
                  className={classes.textField}
                  margin="normal"
                  onChange={this.handleChange('payload')}
                  onKeyPress={this.onKeyPressed}
                  value={this.state.payload}
                  helperText={this.state.payloadValid ? '' : this.state.formErrors.payload}
                  required={true}
                  fullWidth={true}
                />
                <TextField
                  id="decoderResult"
                  label="Decoder result"
                  className={classes.textField}
                  value={this.state.decoderResult}
                  margin="normal"
                  fullWidth={true}
                  multiline
                  disabled
                  rows={4}
                  rowsMax={10}
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </div>
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleTest} disabled={this.state.payload.replace(/\s/g, '').length % 2 !== 0} color="primary">
              Test
            </Button>
          </DialogActions>
        </Dialog>
      </Fragment>
    );
  }
}

DecoderTestDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  close: PropTypes.func.isRequired,
  selectedDecoders: PropTypes.object
};

function mapStateToProps(state) {
  const { alerts } = state;
  return {
    alerts
  };
}

const connectedDecoderTestDialog = connect(mapStateToProps)(DecoderTestDialog);
const styledDecoderTestDialog = withStyles(modalStyle)(connectedDecoderTestDialog);
export { styledDecoderTestDialog as DecoderTestDialog };