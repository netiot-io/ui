import React from 'react';
import {Button, Typography, Dialog, DialogTitle, DialogContent, DialogActions } from '@material-ui/core';
import modalStyle from './modalStyles';
import withStyles from '@material-ui/core/styles/withStyles';
import PropTypes from "prop-types";

class DecoderDeleteDialog extends React.Component {
  handleDelete = () => {
    this.props.handleDelete();
    this.props.close();
  };

  render() {
    const { open, close, classes, selectedDecoders } = this.props;
    return (
      <Dialog
        disableBackdropClick
        disableEscapeKeyDown
        maxWidth="xs"
        aria-labelledby="confirmation-dialog-title"
        open={open}
        onClose={close}
        classes={{
          paper: classes.paper,
        }}
      >
        <DialogTitle id="confirmation-dialog-title">Delete device(s)</DialogTitle>
        <DialogContent>
            <Typography variant="subheading" id="simple-modal-description" className={classes.modalText}>
              Are you sure you want to delete:
              <ul>
                {selectedDecoders.map(decoder => <li key={decoder.id}>{decoder.name}</li>)}
              </ul>
            </Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={close} color="primary">
            Cancel
          </Button>
          <Button onClick={this.handleDelete} color="primary">
            Ok
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

DecoderDeleteDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  close: PropTypes.func.isRequired,
  handleDelete: PropTypes.func.isRequired,
  selectedDecoders: PropTypes.array.isRequired
};

export default withStyles(modalStyle)(DecoderDeleteDialog);