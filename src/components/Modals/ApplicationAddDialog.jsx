import React, {Fragment} from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  InputLabel,
  TextField,
  Select,
  FormHelperText,
  CircularProgress,
  Input,
  Chip,
  MenuItem
} from '@material-ui/core';
import modalStyle from './modalStyles';
import withStyles from '@material-ui/core/styles/withStyles';
import PropTypes from "prop-types";
import {connect} from 'react-redux';
import { Actions } from '@jsonforms/core';
import { JsonForms } from '@jsonforms/react';
import {applicationActions} from "../../actions/application.actions";
import classNames from "classnames";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

class ApplicationAddDialog extends React.Component {
  constructor(props) {
    super(props);
    if (this.props.isEdit) {
      this.loadSelected = false;
    }
    props.dispatch(applicationActions.getApplicationTypes());

    this.state = {
      type: '',
      name: '',
      organizationId: '',
      openAlert: false,
      typeValid: true,
      nameValid: true,
      assignedTo: [],
      formErrors: {
        name: 'Please type a name',
        type: 'Please choose a type',
      },
      displayJsonForm: false,
      loading: false
    };
    this.handleAdd = this.handleAdd.bind(this);
  }

  validateForm = () => {
    let formValid = true;
    if (this.state.type.length > 0) {
      this.setState({typeValid: true});
    } else {
      this.setState({typeValid: false});
      formValid = false;
    }
    if (this.state.name.length > 0) {
      this.setState({nameValid: true});
    } else {
      this.setState({nameValid: false});
      formValid = false;
    }
    this.setState({formValid});
    return formValid;
  };

  handleChange = property => event => {
    this.setState({[property]: event.target.value});
    switch (property) {
      case 'type':
        if (event.target.value.length === 0) {
          this.setState({typeValid: false});
        } else {
          this.setState({typeValid: true});
          this.getAppConfig(this.props.applications.applicationTypes.find(type => type.id === parseInt(event.target.value, 10)).application_path);
        }
        break;
      case 'name':
        if (event.target.value.length === 0) {
          this.setState({nameValid: false});
        } else {
          this.setState({nameValid: true});
        }
        break;
      default:
        console.log('Unknown property edit or no validation required');
    }
  };

  getAppConfig = (appPath, appId = -1) => {
    this.setState({loading: true});
    this.props.dispatch(applicationActions.getApplicationConfig(appPath, appId));
  };

  handleEdit = () => {
    if (this.validateForm()) {
      const userIds = this.state.assignedTo.length > 0 ? this.state.assignedTo.map(user => parseInt(user.substr(0, user.indexOf('%')), 10)) : [this.props.user.userId];
      const organizations = this.getOrganizationsIds(this.props.users.items, userIds);
      this.props.handleEdit({
        id: this.props.selectedApplication.id,
        name: this.state.name,
        application_type_id: this.state.type,
        application_type_name: this.props.selectedApplication.application_type_name,
        application_type_thumbnail: this.props.selectedApplication.application_type_thumbnail,
        application_path: this.props.applications.applicationTypes.find(type => type.id === parseInt(this.state.type, 10)).application_path,
        organizations,
        user_ids: userIds,
        activated: true
      });
    }
  };

  handleAdd = () => {
    if (this.validateForm()) {
      const userIds = this.state.assignedTo.length > 0 ? this.state.assignedTo.map(user => parseInt(user.substr(0, user.indexOf('%')), 10)) : [this.props.user.userId];
      const organizations = this.getOrganizationsIds(this.props.users.items, userIds);
      this.props.handleAdd({
        application_type_id: this.state.type,
        application_type_name: this.props.applications.applicationTypes.find(type => type.id === parseInt(this.state.type, 10)).name,
        application_type_thumbnail: this.props.applications.applicationTypes.find(type => type.id === parseInt(this.state.type, 10)).thumbnail,
        application_path: this.props.applications.applicationTypes.find(type => type.id === parseInt(this.state.type, 10)).application_path,
        name: this.state.name,
        organizations,
        user_ids: userIds,
        activated: true
      });
    }
  };

  getOrganizationsIds = (users, userIds) => {
    const organizations = [];
    for (let user of users) {
      if (userIds.includes(user.id)) {
        organizations.push(user.organization_id);
      }
    }
    return organizations;
  };

  handleClose = () => {
    this.props.close();
    this.clearForm();
    this.props.dispatch(applicationActions.deleteStoreApplicationConfig());
   };

  clearForm = () => {
    this.setState({
      type: '',
      name: '',
      organizationId: '',
      typeValid: true,
      nameValid: true,
      displayJsonForm: false,
      assignedTo: []
    });
    this.loadSelected = false;
  };

  componentWillReceiveProps = nextProps => {
    const {alerts} = nextProps;
    /* When a row is selected the info for the selected item is loaded in the form.
     * If the row is then deselected, the info needs to be cleared from the form if the user tries to
     * add a new item.
     */
    if (this.props.open === false && nextProps.open === true && !nextProps.isEdit) {
      this.clearForm();
    }
    if (alerts.length > 0 && alerts[alerts.length - 1].type === 'success' && nextProps.open) {
      this.handleClose();
    } else if (nextProps.open && !this.loadSelected && nextProps.selectedApplication && !nextProps.selectedApplication.hasOwnProperty('updating')) {
      this.loadSelected = true;
      const assignedTo = [];
      for (let userId of nextProps.selectedApplication.user_ids) {
        const u = nextProps.users.items.find(user => user.id === userId);
        if (typeof u !== 'undefined') {
          assignedTo.push(`${u.id}%${u.first_name} ${u.last_name}`);
        }
      }
      this.setState({
        name: nextProps.selectedApplication.name,
        type: nextProps.selectedApplication.application_type_id.toString(),
        assignedTo
      });
      this.getAppConfig(this.props.applications.applicationTypes.find(type => type.id ===
        parseInt(nextProps.selectedApplication.application_type_id, 10)).application_path, nextProps.selectedApplication.id);
    } else if (nextProps.open && typeof nextProps.applications.applicationConfig !== 'undefined' && !this.state.displayJsonForm) {
      const applicationConfig = nextProps.applications.applicationConfig;
      this.props.dispatch(Actions.init(applicationConfig.data, applicationConfig.schema, applicationConfig.uischema));
      this.setState({
        displayJsonForm: true,
        loading: false
      });
    }
  };

  render() {
    const { open, classes, isEdit, applications, users, abilities, user } = this.props;
    return (open && typeof applications.applicationTypes !== 'undefined' && <Fragment>
        <Dialog
          disableBackdropClick
          disableEscapeKeyDown
          disableEnforceFocus
          maxWidth="md"
          aria-labelledby="confirmation-dialog-title"
          open={open}
          onClose={this.handleClose}
          classes={{
            paper: classes.paper,
          }}
        >
          <DialogTitle id="confirmation-dialog-title">{isEdit ? 'Edit application ' : 'Add application '}{this.state.name}</DialogTitle>
          <DialogContent>
            {this.state.loading && <CircularProgress size={24} className={classes.buttonProgress} />}
            <form id="addApplicationForm" className="addDeviceForm" onSubmit={this.handleSubmit}>
              <fieldset disabled={this.state.loading ? 'disabled' : ''}>
                <div className={classes.container}>
                  <TextField
                    autoFocus={true}
                    error={!this.state.nameValid}
                    id="applicationName"
                    label="Name"
                    defaultValue={this.state.name}
                    className={classes.textField}
                    margin="normal"
                    onChange={this.handleChange('name')}
                    helperText={this.state.nameValid ? '' : this.state.formErrors.name}
                    required={true}
                    fullWidth={true}
                  />
                  {typeof applications.applicationTypes !== 'undefined' &&
                  <FormControl required className={classes.typeSelectOrganizations} error={!this.state.typeValid}>
                    <InputLabel htmlFor="protocol-native-required">Application type</InputLabel>
                    <Select
                      native
                      value={this.state.type}
                      onChange={this.handleChange('type')}
                      name="type"
                      inputProps={{
                        id: 'protocol-native-required',
                      }}
                    >
                      <option value=""/>
                      {applications.applicationTypes.map(type => (
                        <option value={type.id} key={type.id} title={type.description}>{type.name}</option>
                      ))}
                    </Select>
                    <FormHelperText
                      id="weight-helper-text">{this.state.typeValid ? '' : this.state.formErrors.type}</FormHelperText>
                  </FormControl>
                  }
                  {this.state.displayJsonForm && <JsonForms />}
                  {abilities.can('assign', 'applications') &&
                  <Fragment>
                    <FormControl className={classNames(classes.formControl, classes.typeSelectOrganizations)}>
                      <InputLabel htmlFor="select-multiple-chip">Assign to</InputLabel>
                      <Select
                        multiple
                        value={this.state.assignedTo}
                        onChange={this.handleChange('assignedTo')}
                        input={<Input id="select-multiple-chip"/>}
                        renderValue={selected => (
                          <div className={classes.chips}>
                            {selected.map((value) => {
                              return (
                                <Chip key={value.substr(0, value.indexOf('%'))} label={value.substr(value.indexOf('%') + 1)}
                                      className={classes.chip}/>
                              )
                            })}
                          </div>
                        )}
                        MenuProps={MenuProps}
                      >
                        {users.items.filter(u => u.id !== user.userId).map(u =>
                          (
                          <MenuItem
                            key={u.id}
                            value={u.id + '%' + u.first_name + ' ' + u.last_name}
                          >
                            {`${u.first_name} ${u.last_name}`}
                          </MenuItem>
                        ))
                        }
                      </Select>
                    </FormControl>
                  </Fragment>
                  }
                </div>
              </fieldset>
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={isEdit ? this.handleEdit : this.handleAdd} color="primary">
              Ok
            </Button>
          </DialogActions>
        </Dialog>
      </Fragment>
    );
  }
}

ApplicationAddDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  close: PropTypes.func.isRequired,
  selectedApplication: PropTypes.object,
  isEdit: PropTypes.bool.isRequired,
  handleEdit: PropTypes.func,
  handleAdd: PropTypes.func
};

function mapStateToProps(state) {
  const { alerts, authentication, applications, users } = state;
  const { user, abilities } = authentication;
  return {
    alerts,
    user,
    applications,
    users,
    abilities
  };
}

const connectedApplicationAddDialog = connect(mapStateToProps)(ApplicationAddDialog);
const styledApplicationAddDialog = withStyles(modalStyle)(connectedApplicationAddDialog);
export { styledApplicationAddDialog as ApplicationAddDialog };