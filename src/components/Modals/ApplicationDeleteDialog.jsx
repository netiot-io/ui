import React from 'react';
import {Button, Typography, Dialog, DialogTitle, DialogContent, DialogActions } from '@material-ui/core';
import modalStyle from './modalStyles';
import withStyles from '@material-ui/core/styles/withStyles';
import PropTypes from "prop-types";

class ApplicationDeleteDialog extends React.Component {
  handleDelete = () => {
    this.props.handleDelete();
    this.props.close();
  };

  render() {
    const { open, close, classes, selectedApplications } = this.props;
    return (
      <Dialog
        disableBackdropClick
        disableEscapeKeyDown
        maxWidth="xs"
        aria-labelledby="confirmation-dialog-title"
        open={open}
        onClose={close}
        classes={{
          paper: classes.paper,
        }}
      >
        <DialogTitle id="confirmation-dialog-title">Delete application(s)</DialogTitle>
        <DialogContent>
            <Typography variant="subheading" id="simple-modal-description" className={classes.modalText}>
              Are you sure you want to delete:
              <ul>
                {selectedApplications.map(application => <li key={application.id}>{application.name}</li>)}
              </ul>
            </Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={close} color="primary">
            Cancel
          </Button>
          <Button onClick={this.handleDelete} color="primary">
            Ok
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

ApplicationDeleteDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  close: PropTypes.func.isRequired,
  handleDelete: PropTypes.func.isRequired,
  selectedApplications: PropTypes.array.isRequired
};

export default withStyles(modalStyle)(ApplicationDeleteDialog);