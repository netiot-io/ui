import React, {Fragment} from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField
} from '@material-ui/core';
import modalStyle from './modalStyles';
import withStyles from '@material-ui/core/styles/withStyles';
import PropTypes from "prop-types";
import {connect} from 'react-redux';
import { phoneValidator } from "../../utils/regex"


class OrganizationsAddDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      nameValid: true,
      phoneValid: true,
      addressValid: true,
      formValid: false,
      formErrors: {
        name: 'Please fill in the Name',
        address: 'Please write an address',
        phone: 'Please write a valid phone number'
      },
      name: '',
      address: '',
      phone: '',
      description: '',
      openAlert: false
    };
    this.handleAdd = this.handleAdd.bind(this);
  }

  validateForm = () => {
    let formValid = true;
    if (this.state.name.length > 0) {
      this.setState({nameValid: true});
    } else {
      this.setState({nameValid: false});
      formValid = false;
    }

    if (phoneValidator.test(this.state.phone)) {
      this.setState({phoneValid: true});
    } else {
      this.setState({phoneValid: false});
      formValid = false;
    }
    if (this.state.address === null || this.state.address.length === 0) {
      this.setState({addressValid: false});
      formValid = false;
    } else {
      this.setState({addressValid: true});
    }
    this.setState({formValid});
    return formValid;
  };

  handleChange = property => event => {
    this.setState({[property]: event.target.value});
    switch(property) {
      case 'name':
        if (event.target.value.length === 0) {
          this.setState({nameValid: false});
        } else {
          this.setState({nameValid: true});
        }
        break;
      case 'phone':
        if (phoneValidator.test(event.target.value)) {
          this.setState({phoneValid: true});
        } else {
          this.setState({phoneValid: false});
        }
        break;
      case 'address':
        if (event.target.value.length === 0) {
          this.setState({addressValid: false});
        } else {
          this.setState({addressValid: true});
        }
        break;
      default:
        console.log('Unknown property edit');
    }
  };

  handleAdd = () => {
    if (this.validateForm()) {
      this.props.handleAdd({
        name: this.state.name,
        phone: this.state.phone,
        address: this.state.address,
        description: this.state.description
      });
    }
  };

  handleEdit = () => {
    if (this.validateForm()) {
      this.props.handleEdit({
        id: this.props.selectedOrganization.id,
        name: this.state.name,
        phone: this.state.phone,
        address: this.state.address,
        description: this.state.description
      });
    }
  };

  handleClose = () => {
    this.props.close();
    this.clearForm();
   };

  clearForm = () => {
    this.setState({
      nameValid: true,
      phoneValid: true,
      addressValid: true,
      formValid: false,
      name: '',
      phone: '',
      address: '',
      description: ''
    })
  };

  componentWillReceiveProps = nextProps => {
    const {alerts} = nextProps;
    /* When a row is selected the info for the selected item is loaded in the form.
     * If the row is then deselected, the info needs to be cleared from the form if the user tries to
     * add a new item.
     */

    if (this.props.open === false && nextProps.open === true && !nextProps.isEdit) {
      this.clearForm();
    }
    if (alerts.length > 0 && alerts[alerts.length - 1].type === 'success' && nextProps.open) {
      this.handleClose();
    } else if (nextProps.selectedOrganization) {
      this.setState({
        name: nextProps.selectedOrganization.name,
        phone: nextProps.selectedOrganization.phone,
        description: nextProps.selectedOrganization.description,
        address: nextProps.selectedOrganization.address
      });
    }
  };

  render() {
    const { open, classes, isEdit } = this.props;

    return (<Fragment>
        <Dialog
          disableBackdropClick
          disableEscapeKeyDown
          maxWidth="sm"
          aria-labelledby="confirmation-dialog-title"
          open={open}
          onClose={this.handleClose}
          classes={{
            paper: classes.paper,
          }}
        >
          <DialogTitle id="confirmation-dialog-title">{isEdit ? 'Edit organization' : 'Add organization'}</DialogTitle>
          <DialogContent>
            <form id="addDeviceForm" className="addDeviceForm" onSubmit={this.handleSubmit}>
              <div className={classes.container}>
                <TextField
                  autoFocus={true}
                  error={!this.state.nameValid}
                  id="organizationName"
                  label="Name"
                  defaultValue={this.state.name}
                  className={classes.textField}
                  margin="normal"
                  onChange={this.handleChange('name')}
                  helperText={this.state.nameValid ? '' : this.state.formErrors.name}
                  required={true}
                  fullWidth={true}
                />
                <TextField
                  error={!this.state.phoneValid}
                  id="organizationPhone"
                  label="Phone"
                  defaultValue={this.state.phone}
                  className={classes.textField}
                  margin="normal"
                  onChange={this.handleChange('phone')}
                  helperText={this.state.phoneValid ? '' : this.state.formErrors.phone}
                  required={true}
                  fullWidth={true}
                />
                <TextField
                  error={!this.state.addressValid}
                  id="organizationAddress"
                  label="Address"
                  defaultValue={this.state.address}
                  className={classes.textField}
                  margin="normal"
                  onChange={this.handleChange('address')}
                  required={true}
                  fullWidth={true}
                />
                <TextField
                  id="organizationDescription"
                  label="Description"
                  defaultValue={this.state.description}
                  className={classes.textField}
                  margin="normal"
                  onChange={this.handleChange('description')}
                  fullWidth={true}
                  multiline
                  rowsMax={10}
                />

              </div>
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={isEdit ? this.handleEdit : this.handleAdd} color="primary">
              Ok
            </Button>
          </DialogActions>
        </Dialog>
      </Fragment>
    );
  }
}

OrganizationsAddDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  close: PropTypes.func.isRequired,
  handleAdd: PropTypes.func.isRequired,
  organizations: PropTypes.array.isRequired,
  selectedOrganization: PropTypes.object,
  isEdit: PropTypes.bool.isRequired,
  handleEdit: PropTypes.func
};

function mapStateToProps(state) {
  const { alerts } = state;
  return {
    alerts
  };
}

const connectedOrganizationsAddDialog = connect(mapStateToProps)(OrganizationsAddDialog);
const styledOrganizationsAddDialog = withStyles(modalStyle)(connectedOrganizationsAddDialog);
export { styledOrganizationsAddDialog as OrganizationsAddDialog };