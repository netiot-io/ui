import React, {Fragment} from 'react';
import {
  Button,
  Chip,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  FormControlLabel,
  FormHelperText,
  Input,
  InputLabel,
  MenuItem,
  Select,
  Switch,
  TextField
} from '@material-ui/core';
import modalStyle from './modalStyles';
import withStyles from '@material-ui/core/styles/withStyles';
import PropTypes from "prop-types";
import classNames from 'classnames';
import {connect} from 'react-redux';
import {deviceActions, userActions} from "../../actions";
import ParametersFieldGeneration from "./ParametersFieldGeneration";
import {deveuiValidator} from "../../utils/regex";
import uuid from 'uuid';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

class DeviceAddDialog extends React.Component {
  constructor(props) {
    super(props);
    if (this.props.isEdit) {
      this.loadSelected = false;
    }
    if (props.abilities.can('view', 'organizations')) {
      if (typeof props.users.organizations === 'undefined') {
        props.dispatch(userActions.getOrganizations(true));
      }
    }
    if (typeof props.devices.protocols === 'undefined') {
      props.dispatch(deviceActions.getProtocols());
    }

    this.state = {
      nameValid: true,
      deveuiValid: true,
      protocolValid: true,
      deviceTypeValid: true,
      decoderValid: true,
      protocolParamsValid: {},
      deviceTypeNameValid: true,
      typeFormValid: false,
      profileValid: true,
      profileNameValid: true,
      formValid: false,
      type: '',
      index: 1,
      formErrors: {
        name: 'Please fill in the Name',
        deveui: 'Please write a valid DEVEUI (16 hexa chars)',
        protocol: 'Please select a protocol',
        deviceType: 'Please select a device type',
        decoder: 'Please select a decoder',
        protocolParams: {},
        deviceTypeName: 'Please fill in the name',
        profile: 'Please select a profile',
        profileName: 'Please fill in the name',
      },
      name: '',
      deveui: '',
      assignedTo: [],
      description: '',
      protocol: '',
      decoder: '',
      deviceType: '',
      protocolParams: {},
      paramsDef: [],
      deviceTypeName: '',
      deviceTypeDescription: '',
      profile: '',
      profileName: '',
      activate: false,
      openAlert: false,
    };

    this.protocolParamsValidation = [];
    this.handleAdd = this.handleAdd.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.validateForm = this.validateForm.bind(this);
    this.addVisibleProtocolParam = this.addVisibleProtocolParam.bind(this);
    this.removeVisibleProtocolParam = this.removeVisibleProtocolParam.bind(this);
  }

  addVisibleProtocolParam = (paramId) => {
    this.protocolParamsValidation = [...this.protocolParamsValidation, paramId];
  };

  removeVisibleProtocolParam = (paramId) => {
    this.protocolParamsValidation = this.protocolParamsValidation.filter(e => e !== paramId);
  };

  validateForm = () => {
    let formValid = true;
    if (this.state.name.length > 0) {
      this.setState({nameValid: true});
    } else {
      this.setState({nameValid: false});
      formValid = false;
    }
    if (this.state.protocol.length === 0) {
      this.setState({protocolValid: false});
      formValid = false;
    } else {
      this.setState({protocolValid: true});
    }
    if (this.state.deviceType.length === 0) {
      this.setState({deviceTypeValid: false});
      formValid = false;
    } else {
      this.setState({deviceTypeValid: true});
    }

    // TODO: decide if we validate it or not
    // if (deveuiValidator.test(this.state.deveui)) {
    //   this.setState({deveuiValid: true});
    // } else {
    //   this.setState({deveuiValid: false});
    //   formValid = false;
    // }
    if (this.state.deviceType === 'new') {
      if (this.state.deviceTypeName.length === 0) {
        this.setState({deviceTypeNameValid: false});
        formValid = false;
      } else {
        this.setState({deviceTypeNameValid: true});
      }
      if (this.state.decoder.length === 0) {
        this.setState({decoderValid: false});
        formValid = false;
      } else {
        this.setState({decoderValid: true});
      }
    }

    const protocol = this.props.devices.protocols.find(protocol => protocol.id === parseInt(this.state.protocol, 10));
    if (protocol.protocol_parameters !== null && this.state.profile.length === 0) {
      this.setState({profileValid: false});
      formValid = false;
    } else {
      this.setState({profileValid: true});
    }
    const invalidParams = {};
    if (this.state.profile === 'new') {
      if (this.state.profileName.length === 0) {
        this.setState({profileNameValid: false});
        formValid = false;
      } else {
        this.setState({profileNameValid: true});
      }
      // if this is a new profile we validate all params (protocol and device)
      this.protocolParamsValidation.forEach(paramId => {
        // TODO: this checks for all displayed params that they are not empty. Should implement using a validation
        // function that is given in the params JSON
        if (this.state.protocolParams[paramId].length === 0) {
          invalidParams[paramId] = false;
          formValid = false;
        } else {
          invalidParams[paramId] = true;
        }
      });
    } else {
      // if we use an existing profile, we only need to validate the device params
      const protocol = this.props.devices.protocols.find(protocol => protocol.id === parseInt(this.state.protocol, 10));
      const deviceParameters = this.getParamIds(protocol.device_parameters);
      this.protocolParamsValidation.forEach(paramId => {
        // TODO: this checks for all displayed params that they are not empty. Should implement using a validation
        // function that is given in the params JSON
        if (deviceParameters.includes(paramId) &&
          !protocol.device_parameters.find(param => param.id === paramId).disabled &&
          !protocol.device_parameters.find(param => param.id === paramId).generate &&
          this.state.protocolParams[paramId].length === 0) {
          invalidParams[paramId] = false;
          formValid = false;
        } else {
          invalidParams[paramId] = true;
        }
      });
    }

    this.setState({formValid});
    this.setState({protocolParamsValid: {...this.state.protocolParamsValid, ...invalidParams}});
    return formValid;
  };

  handleChange = property => event => {
    if (property in this.state) {
      if (property === 'activate') {
        this.setState({ [property]: event.target.checked });
      } else {
        this.setState({[property]: event.target.value});
      }
    } else { // protocol parameters are all saved in their own object
      this.setState({protocolParams: {...this.state.protocolParams, [property]: event.target.value}});
    }
    switch (property) {
      case 'name':
        if (event.target.value.length === 0) {
          this.setState({nameValid: false});
        } else {
          this.setState({nameValid: true});
        }
        break;
      case 'deveui':
        if (deveuiValidator.test(event.target.value)) {
          this.setState({deveuiValid: true});
        } else {
          this.setState({deveuiValid: false});
        }
        break;
      case 'protocol':
        if (event.target.value.length === 0) {
          this.setState({protocolValid: false});
        } else {
          this.setState({protocolValid: true});
          this.props.dispatch(deviceActions.getDeviceTypes(event.target.value));
          this.props.dispatch(deviceActions.getDecoders(event.target.value));
        }
        break;
      case 'deviceType':
        this.setState({profile: ''});
        if (event.target.value.length === 0) {
          this.setState({deviceTypeValid: false});
        } else {
          this.setState({deviceTypeValid: true});
          if (event.target.value !== 'new') {
            this.props.dispatch(deviceActions.getProfiles(event.target.value));
            this.props.dispatch(deviceActions.getDeviceTypeData(event.target.value));
          }
        }
        break;
      case 'decoder':
        if (event.target.value.length === 0) {
          this.setState({decoderValid: false});
        } else {
          this.setState({decoderValid: true});
        }
        break;
      case 'deviceTypeName':
        if (event.target.value.length === 0) {
          this.setState({deviceTypeNameValid: false});
        } else {
          this.setState({deviceTypeNameValid: true});
        }
        break;
      case 'profile':
        if (event.target.value.length === 0) {
          this.setState({profileValid: false});
        } else {
          this.setState({profileValid: true});

          // get the selected protocol object
          const protocol = JSON.parse(JSON.stringify(this.props.devices.protocols.find(protocol => protocol.id === parseInt(this.state.protocol, 10))));
          let protocolParams = {};
          let protocolParamsValid = {};
          let protocolParamsErrors = {};
          let params = protocol.protocol_parameters.concat(protocol.device_parameters);
          if (event.target.value !== 'new') {
            const profileParams = {
              ...this.props.devices.deviceTypes.find(deviceType => deviceType.id === parseInt(this.state.deviceType, 10))
                .profiles.find(profile => profile.id === parseInt(event.target.value, 10)).protocol_parameters
            };
            params = this.updateParamValues(params, profileParams);
          }
          params.forEach(param => { // for each protocol parameter
            // initialize its state variables
            protocolParams = {...protocolParams, [param.id]: ''};
            protocolParamsValid = {...protocolParamsValid, [param.id]: true};
            protocolParamsErrors = {...protocolParamsErrors, [param.id]: `Invalid value for ${param.name}`};
          });
          this.setState({protocolParams});
          this.setState({paramsDef: params});
          this.setState({protocolParamsValid});
          this.setState({formErrors: {...this.state.formErrors, protocolParams: protocolParamsErrors}});
        }
        break;
      case 'profileName':
        if (event.target.value.length === 0) {
          this.setState({profileNameValid: false});
        } else {
          this.setState({profileNameValid: true});
        }
        break;
      default:
        if (property in this.state.protocolParams) {
          if (event.target.value.length === 0) {
            this.setState({protocolParamsValid: {...this.state.protocolParamsValid, [property]: false}});
          } else {
            this.setState({protocolParamsValid: {...this.state.protocolParamsValid, [property]: true}});
          }
          break;
        } else {
          console.log('Unknown property edit');
        }
    }
  };

  getParamValues = (protocolParams) => {
    let paramValues = {};
    protocolParams.forEach(param => {
      // only save the params that have a value. We consider that any empty parameters means that they are not part
      // of the current configuration (eg: params for ABP vs params for OTAA)
      if(param.id === 'devEui' && param.generate) {
        paramValues = {
          ...paramValues,
          [param.id]: uuid.v1()
        };
      }

      if (typeof this.state.protocolParams[param.id] !== 'undefined' &&
        this.state.protocolParams[param.id].length > 0) {
        paramValues = {
          ...paramValues,
          [param.id]: this.state.protocolParams[param.id]
        };
        if (typeof param.value !== 'undefined') {
          param.value.forEach(value => {
            if (typeof value.extra_fields !== 'undefined') {
              paramValues = {
                ...paramValues,
                ...this.getParamValues(value.extra_fields)
              };
            }
          });
        }
      }
    });
    return paramValues;
  };

  getParamIds = (protocolParams) => {
    let paramValues = [];
    protocolParams.forEach(param => {
      paramValues.push(param.id);
      if (typeof param.value !== 'undefined') {
        param.value.forEach(value => {
          if (typeof value.extra_fields !== 'undefined') {
            paramValues = paramValues.concat(this.getParamIds(value.extra_fields));
          }
        });
      }
    });
    return paramValues;
  };

  getFormDeviceData = (isNew) => {
    let profile = null;
    let deviceType = null;
    let deviceTypeId = -1;
    let profileId = -1;

    const protocol = this.props.devices.protocols.find(protocol => protocol.id === parseInt(this.state.protocol, 10));
    const deviceParameters = this.getParamValues(protocol.device_parameters);

    if (this.state.profile === 'new') {
      profile = {
        name: this.state.profileName,
        protocol_parameters: this.getParamValues(protocol.protocol_parameters),
        device_parameters: deviceParameters
      }
    } else if (isNew && protocol.protocol_parameters === null) {
      profile = {
        name: 'empty',
        protocol_parameters: {},
        device_parameters: {}
      }
    } else {
      profileId = this.state.profile;
    }
    if (this.state.deviceType === 'new') {
      deviceType = {
        name: this.state.deviceTypeName,
        description: this.state.deviceTypeDescription,
        protocol_id: parseInt(this.state.protocol, 10),
        decoder_id: parseInt(this.state.decoder, 10),
        activate: true
      }
    } else {
      deviceTypeId = parseInt(this.state.deviceType, 10);
    }

    const deviceData = {
      name: this.state.name,
      description: this.state.description,
      device_type_id: deviceTypeId, // if -1 we fill this in once we add the device type and get the new id from the server
      profile_id: profileId, // if -1 we fill this in once we add the device type and get the new id from the server
      device_parameters: deviceParameters,
      user_ids: this.state.assignedTo.length > 0 ? this.state.assignedTo.map(user => parseInt(user.substr(0, user.indexOf('%')), 10)) : [this.props.user.userId],
      activate: this.state.activate
    };
    return {
      profile,
      deviceType,
      deviceData
    };
  };

  handleAdd = () => {
    if (this.validateForm()) {
      const device = this.getFormDeviceData(true);
      device.deviceData.activate = this.state.activate;
      this.props.handleAdd(device);
    }
  };

  handleEdit = () => {
    if (this.validateForm()) {
      const device = this.getFormDeviceData(false);
      device.deviceData.id = this.props.selectedDevice.id;
      device.deviceData.activate = this.state.activate;
      this.props.handleEdit(device);
    }
  };

  handleClose = () => {
    this.props.close();
    this.clearForm();
  };

  clearForm = () => {
    this.setState({
      nameValid: true,
      deveuiValid: true,
      protocolValid: true,
      deviceTypeValid: true,
      decoderValid: true,
      protocolParamsValid: [],
      typeFormValid: false,
      formValid: false,
      name: '',
      deveui: '',
      description: '',
      type: '',
      assignedTo: [],
      index: 1,
      deviceType: '',
      protocol: '',
      decoder: '',
      protocolParams: {},
      paramsDef: [],
      deviceTypeName: '',
      deviceTypeDescription: '',
      profile: '',
      profileName: '',
      activate: false,
    })
  };

  updateParamValues = (params, values) => {
    const result = [];
    params.forEach(param => {
      if (values.hasOwnProperty(param.id)) {
        if (!(param.value instanceof Array)) {
          param.value = values[param.id];
          result.push(param)
        } else {
          const options = param.value;
          param.value = values[param.id];
          result.push(param);
          const selectedOption = options.find(op => op.name === values[param.id]);
          if (typeof selectedOption.extra_fields !== 'undefined') {
            result.push(...this.updateParamValues(selectedOption.extra_fields, values));
          }
        }
      }

    });
    return result;
  };

  // noinspection JSUnusedLocalSymbols
  // eslint-disable-next-line
  componentWillMount = () => {
    if (this.props.isEdit) {
      this.props.dispatch(deviceActions.getDeviceTypeData(this.props.selectedDevice.device_type_id, true));
    }
  };

  componentWillReceiveProps = nextProps => {
    const {alerts} = nextProps;
    /* When a row is selected the info for the selected item is loaded in the form.
     * If the row is then deselected, the info needs to be cleared from the form if the user tries to
     * add a new item.
     */

    if (this.props.open === false && nextProps.open === true && !nextProps.isEdit) {
      this.clearForm();
    }
    if (alerts.length > 0 && alerts[alerts.length - 1].type === 'success' && nextProps.open) {
      this.handleClose();
    } else if (nextProps.selectedDevice && !nextProps.selectedDevice.hasOwnProperty('updating')) {

      if (!this.loadSelected && typeof nextProps.devices.deviceTypes !== 'undefined' &&
        typeof nextProps.devices.decoders !== 'undefined' &&
        nextProps.devices.deviceTypes.find(deviceType => deviceType.id === nextProps.selectedDevice.device_type_id) &&
        typeof nextProps.devices.deviceTypes.find(deviceType => deviceType.id === nextProps.selectedDevice.device_type_id).profiles !== 'undefined' &&
        typeof nextProps.users.items !== 'undefined') {

        this.loadSelected = true;
        // get the selected protocol object
        const selectedDeviceTypeData = this.props.devices.deviceTypes.find(deviceType => deviceType.id === nextProps.selectedDevice.device_type_id);

        // get all the params (protocol & device) from the protocol - they have no values
        const protocol = JSON.parse(JSON.stringify(this.props.devices.protocols.find(protocol => protocol.id === selectedDeviceTypeData.protocol_id)));
        let params = protocol.protocol_parameters === null ? protocol.device_parameters : protocol.protocol_parameters.concat(protocol.device_parameters);

        // check the profile protocol_params and update their values
        const profileParams = {...nextProps.devices.deviceTypes.find(deviceType => deviceType.id === parseInt(nextProps.selectedDevice.device_type_id, 10))
            .profiles.find(profile => profile.id === parseInt(nextProps.selectedDevice.profile_id, 10)).protocol_parameters, ...nextProps.selectedDevice.device_parameters};
        params = this.updateParamValues(params, profileParams);

        let protocolParams = {};
        let protocolParamsValid = {};
        let protocolParamsErrors = {};

        params.forEach(param => { // for each protocol parameter
          // initialize its state variables
          protocolParams = {...protocolParams, [param.id]: param.value};
          protocolParamsValid = {...protocolParamsValid, [param.id]: true};
          protocolParamsErrors = {...protocolParamsErrors, [param.id]: `Invalid value for ${param.name}`};
        });
        const assignedTo = [];
        for (let userId of nextProps.selectedDevice.user_ids) {
          const u = nextProps.users.items.find(user => user.id === userId);
          if (typeof u !== 'undefined') {
            assignedTo.push(`${u.id}%${u.first_name} ${u.last_name}`);
          }
        }
        this.setState({
          name: nextProps.selectedDevice.name,
          description: nextProps.selectedDevice.description,
          protocol: String(selectedDeviceTypeData.protocol_id),
          deviceType: String(nextProps.selectedDevice.device_type_id),
          profile: String(nextProps.selectedDevice.profile_id),
          activate: nextProps.selectedDevice.activate,
          protocolParams,
          protocolParamsValid,
          paramsDef: params,
          assignedTo,
          formErrors: {...this.state.formErrors, protocolParams: protocolParamsErrors}
        });
      }
    }
  };

  render() {
    const {open, classes, devices, isEdit, users, abilities} = this.props;
    return (typeof users.items !== 'undefined' && typeof devices.protocols !== 'undefined' && typeof users.organizations !== 'undefined' && <Fragment>
        <Dialog
          disableBackdropClick
          disableEscapeKeyDown
          maxWidth="sm"
          aria-labelledby="confirmation-dialog-title"
          open={open}
          onClose={this.handleClose}
          classes={{
            paper: classes.paper,
          }}
        >
          <DialogTitle id="confirmation-dialog-title">{isEdit ? 'Edit device' : 'Add device'}</DialogTitle>
          <DialogContent>
            <form id="addDeviceForm" className="addDeviceForm" onSubmit={this.handleSubmit}>
              <div className={classes.container}>
                {abilities.can('edit', 'devices') &&
                <Fragment>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={this.state.activate}
                        onChange={this.handleChange('activate')}
                        value="activate"
                        color="primary"
                      />
                    }
                    label="Activate"
                  />
                  <TextField
                    autoFocus={true}
                    error={!this.state.nameValid}
                    id="deviceName"
                    label="Name"
                    value={this.state.name}
                    className={classes.textField}
                    margin="normal"
                    onChange={this.handleChange('name')}
                    helperText={this.state.nameValid ? '' : this.state.formErrors.name}
                    required={true}
                    fullWidth={true}
                  />
                  <TextField
                    id="deviceDescription"
                    label="Description"
                    value={this.state.description}
                    className={classes.textField}
                    margin="normal"
                    onChange={this.handleChange('description')}
                    fullWidth={true}
                    multiline
                    rowsMax={10}
                  />
                  <FormControl required className={classes.typeSelectOrganizations} error={!this.state.protocolValid}>
                    <InputLabel htmlFor="protocol-native-required">Protocol</InputLabel>
                    <Select
                      native
                      value={this.state.protocol}
                      onChange={this.handleChange('protocol')}
                      name="protocol"
                      inputProps={{
                        id: 'protocol-native-required',
                      }}
                    >
                      <option value=""/>
                      {devices.protocols.map(protocol => (
                        <option value={protocol.id} key={protocol.id}>{protocol.name}</option>
                      ))}
                    </Select>
                    <FormHelperText
                      id="weight-helper-text">{this.state.protocolValid ? '' : this.state.formErrors.protocol}</FormHelperText>
                  </FormControl>
                  {this.state.protocol.length > 0 &&
                  <Fragment>
                    {devices.deviceTypes &&
                    <FormControl required className={classes.typeSelectOrganizations}
                                 error={!this.state.deviceTypeValid}>
                      <InputLabel htmlFor="protocol-native-required">Device type</InputLabel>
                      <Select
                        native
                        value={this.state.deviceType}
                        onChange={this.handleChange('deviceType')}
                        name="deviceType"
                        inputProps={{
                          id: 'protocol-native-required',
                        }}
                      >
                        <option value="" key={-1}/>
                        <option value="new" key={0}>New</option>
                        {devices.deviceTypes.map(deviceType => (
                          <option value={deviceType.id} key={deviceType.id}
                                  title={deviceType.description}>{deviceType.name}</option>
                        ))}
                      </Select>
                      <FormHelperText
                        id="weight-helper-text">{this.state.deviceTypeValid ? '' : this.state.formErrors.deviceType}</FormHelperText>
                    </FormControl>
                    }

                    {this.state.deviceType.length > 0 &&
                    <Fragment>
                      {this.state.deviceType === 'new' ?
                        <Fragment>
                          <TextField
                            error={!this.state.deviceTypeNameValid}
                            id="deviceTypeName"
                            label="Device type name"
                            defaultValue={this.state.deviceTypeName}
                            className={classes.textField}
                            margin="normal"
                            onChange={this.handleChange('deviceTypeName')}
                            fullWidth={true}
                            helperText={this.state.deviceTypeNameValid ? '' : this.state.formErrors.deviceTypeName}
                            required={true}
                            rowsMax={10}
                          />
                          <TextField
                            id="deviceTypeDescription"
                            label="Device type description"
                            defaultValue={this.state.deviceTypeDescription}
                            className={classes.textField}
                            margin="normal"
                            onChange={this.handleChange('deviceTypeDescription')}
                            fullWidth={true}
                            multiline
                            rowsMax={10}
                          />
                          {devices.decoders &&
                          <FormControl required className={classes.typeSelectOrganizations}
                                       error={!this.state.decoderValid}>
                            <InputLabel htmlFor="protocol-native-required">Decoder</InputLabel>
                            <Select
                              native
                              value={this.state.decoder}
                              onChange={this.handleChange('decoder')}
                              name="decoder"
                              inputProps={{
                                id: 'protocol-native-required',
                              }}
                            >
                              <option value="" key={-1}/>
                              {devices.decoders.map(decoder => (
                                <option value={decoder.id} key={decoder.id}>{decoder.name}</option>
                              ))}
                            </Select>
                            <FormHelperText
                              id="weight-helper-text">{this.state.decoderValid ? '' : this.state.formErrors.decoder}</FormHelperText>
                          </FormControl>
                          }
                        </Fragment>
                        : <Fragment>
                          {devices.deviceTypes.find(deviceType => deviceType.id === parseInt(this.state.deviceType, 10)).decoder_id &&
                          devices.deviceTypes.filter(deviceType => deviceType.id === parseInt(this.state.deviceType, 10)).map(deviceType =>
                            <TextField
                              id={String(deviceType.decoder_id)}
                              key={String(deviceType.decoder_id)}
                              label={'Decoder'}
                              value={devices.decoders.filter(decoder => decoder.id === deviceType.decoder_id).map(decoder => decoder.name)}
                              className={classes.textField}
                              margin="normal"
                              disabled
                              fullWidth={true}
                            />
                          )}
                        </Fragment>
                      }

                      { devices.protocols.find(protocol => protocol.id ===
                        parseInt(this.state.protocol, 10)).protocol_parameters !== null &&
                      <Fragment>
                        <FormControl required className={classes.typeSelectOrganizations}
                                     error={!this.state.profileValid}>
                          <InputLabel htmlFor="protocol-native-required">Profile</InputLabel>
                          <Select
                            native
                            value={this.state.profile}
                            onChange={this.handleChange('profile')}
                            name="profile"
                            inputProps={{
                              id: 'protocol-native-required',
                            }}
                          >
                            <option value="" key={-1}/>
                            <option value="new" key={0}>New</option>
                            {devices.deviceTypes.find(deviceType => deviceType.id === parseInt(this.state.deviceType, 10))
                            && devices.deviceTypes.find(deviceType => deviceType.id === parseInt(this.state.deviceType, 10)).profiles
                            && devices.deviceTypes.find(deviceType => deviceType.id === parseInt(this.state.deviceType, 10)).profiles.map(profile => (
                              <option value={profile.id} key={profile.id}>{profile.name}</option>
                            ))}
                          </Select>
                          <FormHelperText
                            id="weight-helper-text">{this.state.profileValid ? '' : this.state.formErrors.profile}</FormHelperText>
                        </FormControl>

                        {this.state.profile.length > 0 &&
                        <Fragment>
                          {this.state.profile === 'new' ? (
                            <Fragment>
                              <TextField
                                error={!this.state.profileNameValid}
                                id="profileName"
                                label="Profile name"
                                defaultValue={this.state.profileName}
                                className={classes.textField}
                                margin="normal"
                                onChange={this.handleChange('profileName')}
                                fullWidth={true}
                                helperText={this.state.profileNameValid ? '' : this.state.formErrors.profileName}
                                required={true}
                                rowsMax={10}
                              />
                              <Fragment>
                                {devices.protocols.find(protocol => protocol.id ===
                                  parseInt(this.state.protocol, 10)).protocol_parameters.map(param =>
                                  <ParametersFieldGeneration
                                    key={param.id}
                                    paramFieldData={param}
                                    error={this.state.protocolParamsValid}
                                    defaultValue={this.state.protocolParams}
                                    helperText={this.state.formErrors.protocolParams}
                                    handleChange={this.handleChange}
                                    addVisibleProtocolParam={this.addVisibleProtocolParam}
                                    removeVisibleProtocolParam={this.removeVisibleProtocolParam}
                                  />
                                )}
                              </Fragment>
                            </Fragment>
                          ) : (this.state.paramsDef.map(param => <ParametersFieldGeneration
                              key={param.id}
                              paramFieldData={param}
                              error={this.state.protocolParamsValid}
                              defaultValue={this.state.protocolParams}
                              helperText={this.state.formErrors.protocolParams}
                              handleChange={this.handleChange}
                              addVisibleProtocolParam={this.addVisibleProtocolParam}
                              removeVisibleProtocolParam={this.removeVisibleProtocolParam}
                            />)
                          )}
                        </Fragment>
                        }
                      </Fragment>
                      }
                      {devices.protocols.find(protocol => protocol.id ===
                        parseInt(this.state.protocol, 10)).device_parameters !== null &&
                      <Fragment>
                        {devices.protocols.find(protocol => protocol.id ===
                          parseInt(this.state.protocol, 10)).device_parameters.map(param =>
                          <ParametersFieldGeneration
                            key={param.id}
                            paramFieldData={param}
                            error={this.state.protocolParamsValid}
                            defaultValue={this.state.protocolParams}
                            helperText={this.state.formErrors.protocolParams}
                            handleChange={this.handleChange}
                            addVisibleProtocolParam={this.addVisibleProtocolParam}
                            removeVisibleProtocolParam={this.removeVisibleProtocolParam}
                          />
                        )}
                      </Fragment>
                      }
                    </Fragment>
                    }
                  </Fragment>
                  }
                </Fragment>
                }
                {abilities.can('assign', 'devices') &&
                <Fragment>
                  <FormControl className={classNames(classes.formControl, classes.typeSelectOrganizations)}>
                    <InputLabel htmlFor="select-multiple-chip">Assign to</InputLabel>
                    <Select
                      multiple
                      value={this.state.assignedTo}
                      onChange={this.handleChange('assignedTo')}
                      input={<Input id="select-multiple-chip"/>}
                      renderValue={selected => (
                        <div className={classes.chips}>
                          {selected.map((value) => {
                            return (
                              <Chip key={value.substr(0, value.indexOf('%'))} label={value.substr(value.indexOf('%') + 1)}
                                    className={classes.chip}/>
                            )
                          })}
                        </div>
                      )}
                      MenuProps={MenuProps}
                    >
                      {users.items.map(user => (
                        <MenuItem
                          key={user.id}
                          value={user.id + '%' + user.first_name + ' ' + user.last_name}
                        >
                          {`${user.first_name} ${user.last_name}`}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </Fragment>
                }
              </div>
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={isEdit ? this.handleEdit : this.handleAdd} color="primary">
              Ok
            </Button>
          </DialogActions>
        </Dialog>
      </Fragment>
    );
  }
}

DeviceAddDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  close: PropTypes.func.isRequired,
  handleAdd: PropTypes.func.isRequired,
  devices: PropTypes.object.isRequired,
  selectedDevice: PropTypes.object,
  isEdit: PropTypes.bool.isRequired,
  handleEdit: PropTypes.func
};

function mapStateToProps(state) {
  const {alerts, users, authentication} = state;
  const {user, abilities} = authentication;
  return {
    alerts,
    users,
    user,
    abilities
  };
}

const connectedDeviceAddDialog = connect(mapStateToProps)(DeviceAddDialog);
const styledDeviceAddDialog = withStyles(modalStyle)(connectedDeviceAddDialog);
export {styledDeviceAddDialog as DeviceAddDialog};