import React, {Fragment} from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  InputLabel,
  Select,
  TextField,
  FormHelperText
} from '@material-ui/core';
import modalStyle from './modalStyles';
import withStyles from '@material-ui/core/styles/withStyles';
import PropTypes from "prop-types";
import {connect} from 'react-redux';
import { phoneValidator, emailValidator} from "../../utils/regex";

class UsersAddDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstNameValid: true,
      lastNameValid: true,
      phoneValid: true,
      emailValid: true,
      addressValid: true,
      organizationValid: true,
      roleValid: true,
      formValid: false,
      formErrors: {
        firstName: 'Please fill in the First Name',
        lastName: 'Please fill in the Last Name',
        address: 'Please write an address',
        phone: 'Please write a valid phone number',
        email: 'Please write a valid email',
        organization: 'Please select an organization',
        role: 'Please select a role'
      },
      firstName: '',
      lastName: '',
      address: '',
      phone: '',
      email: '',
      organization: props.abilities.can('view', 'organizations') ? '' : props.user.organizationId,
      role: props.abilities.can('view', 'organizations') ? '' : props.user.role.replace('ADMIN', 'USER'),
      openAlert: false
    };
    this.handleAdd = this.handleAdd.bind(this);
  }

  validateForm = () => {
    let formValid = true;
    if (this.state.firstName.length > 0) {
      this.setState({firstNameValid: true});
    } else {
      this.setState({firstNameValid: false});
      formValid = false;
    }

    if (this.state.lastName.length > 0) {
      this.setState({lastNameValid: true});
    } else {
      this.setState({lastNameValid: false});
      formValid = false;
    }

    if (phoneValidator.test(this.state.phone)) {
      this.setState({phoneValid: true});
    } else {
      this.setState({phoneValid: false});
      formValid = false;
    }
    if (emailValidator.test(this.state.email)) {
      this.setState({emailValid: true});
    } else {
      this.setState({emailValid: false});
      formValid = false;
    }
    if (this.state.address.length === 0) {
      this.setState({addressValid: false});
      formValid = false;
    } else {
      this.setState({addressValid: true});
    }
    if (this.state.organization.length === 0) {
      this.setState({organizationValid: false});
      formValid = false;
    } else {
      this.setState({organizationValid: true});
    }
    if (this.state.role.length === 0) {
      this.setState({roleValid: false});
      formValid = false;
    } else {
      this.setState({roleValid: true});
    }

    this.setState({formValid});
    return formValid;
  };

  handleChange = property => event => {
    this.setState({[property]: event.target.value});
    switch(property) {
      case 'firstName':
        if (event.target.value.length === 0) {
          this.setState({firstNameValid: false});
        } else {
          this.setState({firstNameValid: true});
        }
        break;
      case 'lastName':
        if (event.target.value.length === 0) {
          this.setState({lastNameValid: false});
        } else {
          this.setState({lastNameValid: true});
        }
        break;
      case 'phone':
        if (phoneValidator.test(event.target.value)) {
          this.setState({phoneValid: true});
        } else {
          this.setState({phoneValid: false});
        }
        break;
      case 'email':
        if (emailValidator.test(event.target.value)) {
          this.setState({emailValid: true});
        } else {
          this.setState({emailValid: false});
        }
        break;
      case 'organization':
        if (event.target.value.length === 0) {
          this.setState({organizationValid: false});
        } else {
          this.setState({organizationValid: true});
        }
        break;
      case 'role':
        if (event.target.value.length === 0) {
          this.setState({roleValid: false});
        } else {
          this.setState({roleValid: true});
        }
        break;
      case 'address':
        if (event.target.value.length === 0) {
          this.setState({addressValid: false});
        } else {
          this.setState({addressValid: true});
        }
        break;
      default:
        console.log('Unknown property edit');
    }
  };

  handleAdd = () => {
    if (this.validateForm()) {
      this.props.handleAdd({
        first_name: this.state.firstName,
        last_name: this.state.lastName,
        phone_number: this.state.phone,
        email: this.state.email,
        address: this.state.address,
        organization_id: this.state.organization,
        role: this.state.role
      });
    }
  };

  handleEdit = () => {
    if (this.validateForm()) {
      this.props.handleEdit({
        id: this.props.selectedUser.id,
        first_name: this.state.firstName,
        last_name: this.state.lastName,
        phone_number: this.state.phone,
        email: this.state.email,
        address: this.state.address,
        organization_id: this.state.organization,
        role: this.state.role
      });
    }
  };

  handleClose = () => {
    this.props.close();
    this.clearForm();
   };

  clearForm = () => {
    this.setState({
      firstNameValid: true,
      lastNameValid: true,
      phoneValid: true,
      emailValid: true,
      addressValid: true,
      organizationValid: true,
      roleValid: true,
      formValid: false,
      firstName: '',
      lastName: '',
      phone: '',
      email: '',
      address: '',
      organization: '',
      role: ''
    })
  };

  componentWillReceiveProps = nextProps => {
    const {alerts} = nextProps;
    /* When a row is selected the info for the selected item is loaded in the form.
     * If the row is then deselected, the info needs to be cleared from the form if the user tries to
     * add a new item.
     */

    if (this.props.open === false && nextProps.open === true && !nextProps.isEdit) {
      this.clearForm();
    }
    if (alerts.length > 0 && alerts[alerts.length - 1].type === 'success' && nextProps.open) {
      this.handleClose();
    } else if (nextProps.selectedUser) {
      this.setState({
        firstName: nextProps.selectedUser.first_name,
        lastName: nextProps.selectedUser.last_name,
        phone: nextProps.selectedUser.phone_number,
        email: nextProps.selectedUser.email,
        organization: nextProps.selectedUser.organization_id,
        role: nextProps.selectedUser.role,
        address: nextProps.selectedUser.address
      });
    }
  };

  render() {
    const { open, classes, isEdit, users, abilities } = this.props;

    return (
      <Fragment>
        <Dialog
          disableBackdropClick
          disableEscapeKeyDown
          maxWidth="sm"
          aria-labelledby="confirmation-dialog-title"
          open={open}
          onClose={this.handleClose}
          classes={{
            paper: classes.paper,
          }}
        >
          <DialogTitle id="confirmation-dialog-title">{isEdit ? 'Edit user' : 'Add user'}</DialogTitle>
          <DialogContent>
            <form id="addDeviceForm" className="addDeviceForm" onSubmit={this.handleSubmit}>
              <div className={classes.container}>
                <TextField
                  autoFocus={true}
                  error={!this.state.firstNameValid}
                  id="userFirstName"
                  label="First Name"
                  defaultValue={this.state.firstName}
                  className={classes.textField}
                  margin="normal"
                  onChange={this.handleChange('firstName')}
                  helperText={this.state.firstNameValid ? '' : this.state.formErrors.firstName}
                  required={true}
                  fullWidth={true}
                />
                <TextField
                  error={!this.state.lastNameValid}
                  id="userLastName"
                  label="Last Name"
                  defaultValue={this.state.lastName}
                  className={classes.textField}
                  margin="normal"
                  onChange={this.handleChange('lastName')}
                  helperText={this.state.lastNameValid ? '' : this.state.formErrors.lastName}
                  required={true}
                  fullWidth={true}
                />
                <TextField
                  error={!this.state.phoneValid}
                  id="userPhone"
                  label="Phone"
                  defaultValue={this.state.phone}
                  className={classes.textField}
                  margin="normal"
                  onChange={this.handleChange('phone')}
                  helperText={this.state.phoneValid ? '' : this.state.formErrors.phone}
                  required={true}
                  fullWidth={true}
                />
                <TextField
                  error={!this.state.emailValid}
                  id="userEmail"
                  label="Email"
                  defaultValue={this.state.email}
                  className={classes.textField}
                  margin="normal"
                  onChange={this.handleChange('email')}
                  helperText={this.state.emailValid ? '' : this.state.formErrors.email}
                  required={true}
                  fullWidth={true}
                  disabled={isEdit}
                />
                <TextField
                  error={!this.state.addressValid}
                  id="userAddress"
                  label="Address"
                  defaultValue={this.state.address}
                  className={classes.textField}
                  margin="normal"
                  onChange={this.handleChange('address')}
                  required={true}
                  fullWidth={true}
                />
                {abilities.can('view', 'organizations') && typeof users.organizations !== 'undefined' &&
                  <Fragment>
                    <FormControl required className={classes.typeSelectOrganizations}
                                 error={!this.state.organizationValid}>
                      <InputLabel htmlFor="protocol-native-required">Organization</InputLabel>
                      <Select
                        native
                        value={this.state.organization}
                        onChange={this.handleChange('organization')}
                        name="organization"
                        inputProps={{
                          id: 'protocol-native-required',
                        }}
                      >
                        <option value="" key={-1}/>
                        {users.organizations.map(organization => (
                          <option value={organization.id} key={organization.id}>{organization.name}</option>
                        ))}
                      </Select>
                      <FormHelperText
                        id="weight-helper-text">{this.state.organizationValid ? '' : this.state.formErrors.organization}</FormHelperText>
                    </FormControl>

                    <FormControl required className={classes.typeSelectOrganizations}
                                 error={!this.state.roleValid}>
                      <InputLabel htmlFor="protocol-native-required">Role</InputLabel>
                      <Select
                        native
                        value={this.state.role}
                        onChange={this.handleChange('role')}
                        name="role"
                        inputProps={{
                          id: 'protocol-native-required',
                        }}
                      >
                        <option value="" key={-1}/>
                        <option value="ENTERPRISE_USER" key='role1'>User</option>
                        <option value="ENTERPRISE_ADMIN" key='role2'>Admin</option>
                      </Select>
                      <FormHelperText
                        id="weight-helper-text">{this.state.roleValid ? '' : this.state.formErrors.role}</FormHelperText>
                    </FormControl>
                  </Fragment>
                }
              </div>
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={isEdit ? this.handleEdit : this.handleAdd} color="primary">
              Ok
            </Button>
          </DialogActions>
        </Dialog>
      </Fragment>
    );
  }
}

UsersAddDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  close: PropTypes.func.isRequired,
  handleAdd: PropTypes.func.isRequired,
  users: PropTypes.object.isRequired,
  selectedUser: PropTypes.object,
  isEdit: PropTypes.bool.isRequired,
  handleEdit: PropTypes.func
};

function mapStateToProps(state) {
  const { alerts, authentication } = state;
  const { abilities, user } = authentication;
  return {
    alerts,
    abilities,
    user
  };
}

const connectedUsersAddDialog = connect(mapStateToProps)(UsersAddDialog);
const styledUsersAddDialog = withStyles(modalStyle)(connectedUsersAddDialog);
export { styledUsersAddDialog as UsersAddDialog };