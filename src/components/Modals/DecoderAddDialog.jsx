import React, {Fragment} from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
  FormControl,
  InputLabel,
  Select,
  FormHelperText
} from '@material-ui/core';
import modalStyle from './modalStyles';
import withStyles from '@material-ui/core/styles/withStyles';
import PropTypes from "prop-types";
import {connect} from 'react-redux';
import AceEditor from 'react-ace';
// Import a Mode (language)
import 'brace/mode/javascript';

// Import a Theme (okadia, github, xcode etc)
import 'brace/theme/eclipse';

const DEMO_JS_CODE = `
"use strict";
(payload) => {
  // decode a payload/array of bytes and return a json with your data
  
  /*
  * You MUST return an json that respects the formats defined in this document: https://www.google.com
  * The object returned MUST respect this format
  * "timestamp": X, // the timestamp in the received device payload, in milliseconds
  * "data_source": [
  *    {"field_name_1": {value: Y}}, // you can choose any field name and add any extra fields in its object
  *    {"field_name_2:" {value: Z}},
  *    ...
  * ]
  *
  * For extra checks, you can test your code here, too: https://playcode.io/
  * 
  * You MUST define the field name in the 'fields' array. Eg. '['temperature', 'humidity', ...]'.
  * The same FIELD NAMES MUST be used when decoding the bytes.   
  */
  const fields = [];
  let decodedPayload = {};
  //TODO: add your custom code
  return JSON.stringify(decodedPayload);   
};`;

class DecoderAddDialog extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      nameValid: true,
      protocolValid: true,
      descriptionValid: true,
      codeValid: true,
      formValid: false,
      formErrors: {
        name: 'Please fill in the Name',
        protocol: 'Please choose a protocol',
        code: 'Please check your decode function'
      },
      name: '',
      protocol: '',
      code: DEMO_JS_CODE,
      description: '',
      openAlert: false
    };
    this.handleAdd = this.handleAdd.bind(this);
    this.onLoad = this.onLoad.bind(this);
  }

  validateForm = () => {
    let formValid = true;
    if (this.state.name.length > 0) {
      this.setState({nameValid: true});
    } else {
      this.setState({nameValid: false});
      formValid = false;
    }

    if (this.state.protocol.length === 0) {
      this.setState({protocolValid: false});
      formValid = false;
    } else {
      this.setState({protocolValid: true});
    }

    if (this.state.code.length === 0 || this.jsEditor.getSession().getAnnotations().length > 0 ||
      this.state.code.indexOf('(payload) => {') < 0 || this.state.code.indexOf('return') < 0 ||
      this.state.code.lastIndexOf('return') < this.state.code.indexOf('(payload) => {')) {
      this.setState({codeValid: false});
      formValid = false;
    } else {
      this.setState({codeValid: true});
    }
    this.setState({formValid});
    return formValid;
  };

  onLoad = editor => {
    this.jsEditor = editor;
  };


  handleChange = property => event => {
    this.setState({[property]: event.target.value});
    switch(property) {
      case 'name':
        if (event.target.value.length === 0) {
          this.setState({nameValid: false});
        } else {
          this.setState({nameValid: true});
        }
        break;
      case 'protocol':
        if (event.target.value.length === 0) {
          this.setState({protocolValid: false});
        } else {
          this.setState({protocolValid: true});
          const selectedProtocol = this.props.protocols.find(protocol => protocol.id === parseInt(event.target.value, 10));
          if (typeof selectedProtocol !== 'undefined' && selectedProtocol.name.toLowerCase().indexOf("bacnet") >= 0) {
            const devices = JSON.parse(selectedProtocol.metadata).devices;
            let fields = '';
            Object.keys(devices).forEach(function(k) {
              for (let field of devices[k]) {
                fields = fields + `'${field}', `;
              }
            });
            this.setState({
              code: this.state.code.replace("const fields = [];", `const fields = [${fields}];`)
            });
          }
        }
        break;
      case 'code':
        if (event.target.value.length === 0) {
          this.setState({codeValid: false});
        } else {
          console.log('change', event);
          this.setState({codeValid: true});
        }
        break;
      default:
        console.log('Unknown property edit');
    }
  };

  handleAdd = () => {
    if (this.validateForm()) {
      this.props.handleAdd({
        name: this.state.name,
        protocol_id: parseInt(this.state.protocol, 10),
        description: this.state.description,
        code: this.state.code.trim(),
        defined_by_platform: false
      });
    }
  };

  handleEdit = () => {
    if (this.validateForm()) {
      this.props.handleEdit({
        id: this.props.selectedDecoders.id,
        name: this.state.name,
        protocol_id: this.state.protocol,
        description: this.state.description,
        code: this.state.code,
        defined_by_platform: this.props.selectedDecoders.defined_by_platform
      });
    }
  };

  handleClose = () => {
    this.props.close();
    this.clearForm();
  };

  clearForm = () => {
    this.setState({
      nameValid: true,
      protocolValid: true,
      codeValid: true,
      descriptionValid: true,
      formValid: false,
      name: '',
      protocol: '',
      description: '',
      code: DEMO_JS_CODE
    })
  };

  componentWillReceiveProps = nextProps => {
    const {alerts} = nextProps;
    /* When a row is selected the info for the selected item is loaded in the form.
     * If the row is then deselected, the info needs to be cleared from the form if the user tries to
     * add a new item.
     */

    if (this.props.open === false && nextProps.open === true && !nextProps.isEdit) {
      this.clearForm();
    }
    if (alerts.length > 0 && alerts[alerts.length - 1].type === 'success' && nextProps.open) {
      this.handleClose();
    } else if (nextProps.selectedDecoders && !nextProps.selectedDecoders.hasOwnProperty('updating')) {
      this.setState({
        name: nextProps.selectedDecoders.name,
        protocol: nextProps.selectedDecoders.protocol_id,
        description: nextProps.selectedDecoders.description,
        code: nextProps.selectedDecoders.code
      });
    }
  };

  render() {
    const { open, classes, decoders, protocols, isEdit } = this.props;
    return (typeof decoders !== 'undefined' && <Fragment>
        <Dialog
          disableBackdropClick
          disableEscapeKeyDown
          maxWidth="sm"
          aria-labelledby="confirmation-dialog-title"
          open={open}
          onClose={this.handleClose}
          classes={{
            paper: classes.paper,
          }}
        >
          <DialogTitle id="confirmation-dialog-title">{isEdit ? 'Edit decoder' : 'Add decoder'}</DialogTitle>
          <DialogContent>
            <form id="addDecoderForm" className="addDeviceForm" onSubmit={this.handleSubmit}>
              <div className={classes.container}>
                <TextField
                  autoFocus={true}
                  error={!this.state.nameValid}
                  id="decoderName"
                  label="Name"
                  defaultValue={this.state.name}
                  className={classes.textField}
                  margin="normal"
                  onChange={this.handleChange('name')}
                  helperText={this.state.nameValid ? '' : this.state.formErrors.name}
                  required={true}
                  fullWidth={true}
                />
                <Fragment>
                  <FormControl required className={classes.typeSelectOrganizations} error={!this.state.protocolValid}>
                    <InputLabel htmlFor="protocol-native-required">Protocol</InputLabel>
                    <Select
                      native
                      value={this.state.protocol}
                      onChange={this.handleChange('protocol')}
                      name="protocol"
                      inputProps={{
                        id: 'protocol-native-required',
                      }}
                    >
                      <option value=""/>
                      {protocols.map(protocol => (
                        <option value={protocol.id} key={protocol.id}>{protocol.name}</option>
                      ))}
                    </Select>
                    <FormHelperText id="weight-helper-text">{this.state.protocolValid ? '' : this.state.formErrors.protocol}</FormHelperText>
                  </FormControl>
                </Fragment>
                <TextField
                  id="decoderDescription"
                  label="Description"
                  defaultValue={this.state.description}
                  className={classes.descriptionAddDecoder}
                  margin="normal"
                  onChange={this.handleChange('description')}
                  fullWidth={true}
                  multiline
                  rowsMax={10}
                />
                <Fragment>
                  <FormControl required fullWidth={true} margin={'none'} error={!this.state.codeValid}>
                    <InputLabel htmlFor="code"  className={classes.singleLabel}>Decoder code</InputLabel>
                    <FormHelperText id="weight-helper-text">{this.state.codeValid ? '' : this.state.formErrors.code}</FormHelperText>
                    <AceEditor
                      mode="javascript"
                      theme="eclipse"
                      value={this.state.code}
                      onChange={value => {
                        const ev = {};
                        ev.target = {};
                        ev.target.value = value;
                        this.handleChange('code')(ev);
                      }}
                      name="code"
                      width='100%'
                      wrapEnabled={true}
                      onLoad={this.onLoad}
                      editorProps={{
                        $blockScrolling: Infinity
                      }}
                    />
                  </FormControl>
                </Fragment>
              </div>
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={isEdit ? this.handleEdit : this.handleAdd} color="primary">
              Ok
            </Button>
          </DialogActions>
        </Dialog>
      </Fragment>
    );
  }
}

DecoderAddDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  close: PropTypes.func.isRequired,
  handleAdd: PropTypes.func.isRequired,
  decoders: PropTypes.array.isRequired,
  protocols: PropTypes.array.isRequired,
  selectedDecoders: PropTypes.object,
  isEdit: PropTypes.bool.isRequired,
  handleEdit: PropTypes.func
};

function mapStateToProps(state) {
  const { alerts } = state;
  return {
    alerts
  };
}

const connectedDecoderAddDialog = connect(mapStateToProps)(DecoderAddDialog);
const styledDecoderAddDialog = withStyles(modalStyle)(connectedDecoderAddDialog);
export { styledDecoderAddDialog as DecoderAddDialog };