/* eslint no-eval: 0 */
import React, {Fragment} from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
} from '@material-ui/core';
import modalStyle from './modalStyles';
import withStyles from '@material-ui/core/styles/withStyles';
import PropTypes from "prop-types";
import {connect} from 'react-redux';
import {deviceActions} from "../../actions";

const hexRegex = /^[-+]?[0-9A-Fa-f]+\.?[0-9A-Fa-f]*?$/;

class DeviceTestDialog extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      payload: '',
      payloadValid: true,
      formErrors: {
        payload: 'The payload is not correct'
      },
      openAlert: false
    };
    this.handleTest = this.handleTest.bind(this);
  }

  handleChange = property => event => {
    let value = event.target.value;
    switch(property) {
      case 'payload':
        value = value.trim();
        if (value.length === 0) {
          this.setState({payloadValid: false});
        } else {
          if (hexRegex.test(value.slice(-1))) {
            value = value.replace(/\s/g, '').replace(/(.{2})/g, "$1 ").trim().toUpperCase();
            this.setState({[property]: value});
            this.setState({payloadValid: true});
          }
        }
        break;
      default:
        console.log('Unknown property edit');
    }
  };

   handleTest = () => {
     const bytesPayload = this.state.payload.replace(/\s/g, '').match(/.{2}/g).map(b => parseInt(b.replace(/^#/, ''), 16));
     this.props.dispatch(deviceActions.sendPayload(this.props.selectedDevice, bytesPayload));
     this.handleClose();
  };

  handleClose = () => {
    this.props.close();
    this.clearForm();
  };

  clearForm = () => {
    this.setState({
      payload: '',
      payloadValid: true
    })
  };

  componentWillReceiveProps = nextProps => {
    const {alerts} = nextProps;
    if (alerts.length > 0 && alerts[alerts.length - 1].type === 'success' && nextProps.open) {
      this.handleClose();
    }
  };

  render() {
    const { open, classes } = this.props;
    return (<Fragment>
        <Dialog
          disableBackdropClick
          disableEscapeKeyDown
          maxWidth="sm"
          aria-labelledby="confirmation-dialog-title"
          open={open}
          onClose={this.handleClose}
          classes={{
            paper: classes.paper,
          }}
        >
          <DialogTitle id="confirmation-dialog-title">{'Test device'}</DialogTitle>
          <DialogContent>
            <form id="testDeviceForm" className="addDeviceForm" onSubmit={this.handleSubmit}>
              <div className={classes.container}>
                <TextField
                  autoFocus={true}
                  error={!this.state.payloadValid}
                  id="devicePayload"
                  label="Payload"
                  className={classes.textField}
                  margin="normal"
                  onChange={this.handleChange('payload')}
                  onKeyPress={this.onKeyPressed}
                  value={this.state.payload}
                  helperText={this.state.payloadValid ? '' : this.state.formErrors.payload}
                  required={true}
                  fullWidth={true}
                />
              </div>
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleTest} disabled={this.state.payload.replace(/\s/g, '').length % 2 !== 0} color="primary">
              Test
            </Button>
          </DialogActions>
        </Dialog>
      </Fragment>
    );
  }
}

DeviceTestDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  close: PropTypes.func.isRequired,
  selectedDevice: PropTypes.object
};

function mapStateToProps(state) {
  const { alerts } = state;
  return {
    alerts
  };
}

const connectedDeviceTestDialog = connect(mapStateToProps)(DeviceTestDialog);
const styledDeviceTestDialog = withStyles(modalStyle)(connectedDeviceTestDialog);
export { styledDeviceTestDialog as DeviceTestDialog };