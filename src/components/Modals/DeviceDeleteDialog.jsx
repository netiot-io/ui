import React from 'react';
import {Button, Typography, Dialog, DialogTitle, DialogContent, DialogActions } from '@material-ui/core';
import modalStyle from './modalStyles';
import withStyles from '@material-ui/core/styles/withStyles';
import PropTypes from "prop-types";

class DeviceDeleteDialog extends React.Component {
  handleDelete = () => {
    this.props.handleDelete();
    this.props.close();
  };

  render() {
    const { open, close, classes, selectedDevices } = this.props;
    return (
      <Dialog
        disableBackdropClick
        disableEscapeKeyDown
        maxWidth="xs"
        aria-labelledby="confirmation-dialog-title"
        open={open}
        onClose={close}
        classes={{
          paper: classes.paper,
        }}
      >
        <DialogTitle id="confirmation-dialog-title">Delete device(s)</DialogTitle>
        <DialogContent>
            <Typography variant="subheading" id="simple-modal-description" className={classes.modalText}>
              Are you sure you want to delete:
              <ul>
                {selectedDevices.map(device => <li key={device.id}>{device.name}</li>)}
              </ul>
            </Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={close} color="primary">
            Cancel
          </Button>
          <Button onClick={this.handleDelete} color="primary">
            Ok
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

DeviceDeleteDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  close: PropTypes.func.isRequired,
  handleDelete: PropTypes.func.isRequired,
  selectedDevices: PropTypes.array.isRequired
};

export default withStyles(modalStyle)(DeviceDeleteDialog);