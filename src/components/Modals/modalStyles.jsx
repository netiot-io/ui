const modalStyle = {
  modal: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)'
  },
  paper: {
    width: '80%',
  },
  modalTitle: {
    backgroundColor: '#fcc7c7',
    fontSize: "1.3125rem",
    fontWeight: 500,
    fontFamily: ["Roboto", "Helvetica", "Arial", "sans-serif"],
    lineHeight: "1.16667em",
    color: "#f05f50",
    padding: '10px 8px',
    marginBottom: '10px'
  },
  modalText: {
    padding: '0 8px'
  },
  button: {
    margin: '5px',
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: 2,
  },
  editType: {
    color: '#2aa0da',
    fontSize: '2rem',
  },
  typesGroup: {
    display: 'flex',
    flexWrap: 'wrap',
    width: '100%',
    alignItems: 'center'
  },
  typesColumn: {
    width: '90%'
  },
  addTypeColumn: {
    width: '10%',
  },
  addTypeGroup: {
    display: 'flex',
    flexWrap: 'wrap',
    width: '100%',
    alignItems: 'baseline',
    justifyContent: 'space-evenly',
    marginTop: 10
  },
  typeSelect: {
    flex: 2,
    margin: '0 3%'
  },
  typeSelectOrganizations: {
    width: '100%',
    marginTop: '16px',
    marginBottom: '8px',
  },
  singleLabel: {
    width: '100%',
    marginTop: '16px',
    marginBottom: '20px',
    position: 'relative'
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  address: {
    flex: 2,
    margin: '0 3%'
  },
  typeButton: {
    flex: 1,
    margin: '0 3%'
  },
  chip: {
    margin: 5,
  },
  legend: {
    marginTop: 32,
    color: 'rgba(0, 0, 0, 0.54)',
    fontSize: '1rem',
    fontWeight: 'bold',
    lineHeight: 1,
    borderBottom: '0px solid rgba(0, 0, 0, 0.42)'
  },
  error: {
    fontWeight: 'normal'
  },
  descriptionAddDecoder: {
    marginTop: '-5px'
  },
  buttonProgress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  }
};

export default modalStyle;
