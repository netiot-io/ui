import React, {Fragment} from 'react';
import {TextField, FormControl, InputLabel, Select, FormHelperText} from '@material-ui/core';
import modalStyle from './modalStyles';
import withStyles from '@material-ui/core/styles/withStyles';
import PropTypes from "prop-types";

/**
 * Component use to generate the protocol parameters dynamically, based on the give protocol parameters object
 */
class ParametersFieldGeneration extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      displayExtraFields: false, // tells if the parameter component should display its extra fields/parameters
      extraFields: <Fragment /> // components for the extra fields/parameters
    };
    this.handleChange = this.handleChange.bind(this);
  }

  // noinspection JSUnusedGlobalSymbols
  componentDidMount = () => {
    this.props.addVisibleProtocolParam(this.props.paramFieldData.id);
  };

  // noinspection JSUnusedGlobalSymbols
  componentWillUnmount = () => {
    this.props.removeVisibleProtocolParam(this.props.paramFieldData.id);
  };

  handleChange = property => event => {
    let displayExtraFields = false;
    let extraFields = [];
    // if the parameters has values, meaning options for a drop down / select
    if (typeof this.props.paramFieldData.value !== 'undefined' ) {
      // get the option selected by the user
      const selectedValue = this.props.paramFieldData.value.find(param => param.name === event.target.value);
      if (typeof selectedValue.extra_fields !== 'undefined') { // if this options generates extra fields / parameters
        displayExtraFields = true; // display them
        // for each extra field / parameter generate a dynamic component based on its properties
        selectedValue.extra_fields.forEach(extraField => {
          extraFields.push(extraField);
        });
      }
    }
    this.setState({displayExtraFields, extraFields});
    this.props.handleChange(property)(event);
  };

  render() {
    const { paramFieldData, error, defaultValue, helperText, classes } = this.props;
    if (typeof paramFieldData.value === 'undefined' || paramFieldData.value === null) {// text field
      return (
        <TextField
          error={!paramFieldData.disabled && paramFieldData.id in error && !error[paramFieldData.id]}
          id={paramFieldData.id}
          label={paramFieldData.name}
          defaultValue={defaultValue[paramFieldData.id]}
          className={classes.textField}
          margin="normal"
          onChange={this.handleChange(paramFieldData.id)}
          helperText={paramFieldData.id in error && !error[paramFieldData.id] ?
            helperText[paramFieldData.id] :
            paramFieldData.generate ? "If left empty, a value will be generated for you" : ""
            }
          required={paramFieldData.required === true}
          fullWidth={true}
          disabled={paramFieldData.disabled}
        />
      )
    } else if (!(paramFieldData.value instanceof Array)){
      return (
        <TextField
          id={paramFieldData.id}
          label={paramFieldData.name}
          defaultValue={paramFieldData.value}
          className={classes.textField}
          margin="normal"
          required={paramFieldData.required === true}
          disabled
          fullWidth={true}
        />
      )
    } else {
      return (
        <Fragment>
          <FormControl required={paramFieldData.required === true} className={classes.typeSelectOrganizations} error={!error[paramFieldData.id]}>
            <InputLabel htmlFor="protocol-native-required">{paramFieldData.name}</InputLabel>
            <Select
              native
              value={defaultValue[paramFieldData.id]}
              onChange={this.handleChange(paramFieldData.id)}
              name={paramFieldData.id}
              inputProps={{
                id: 'protocol-native-required',
              }}
            >
              <option value=""/>
              {paramFieldData.value.map((optionValue, index) => (
                <option value={optionValue.name} key={index}>{optionValue.name}</option>
              ))}
            </Select>
            <FormHelperText id="weight-helper-text">{error[paramFieldData.id] ? '' : helperText[paramFieldData.id]}</FormHelperText>
          </FormControl>

          {this.state.displayExtraFields && <div>
            { this.state.extraFields.map(extraField => {
              return (<ParametersFieldGeneration
                key={extraField.id}
                paramFieldData={extraField}
                error={error}
                defaultValue={defaultValue}
                helperText={helperText}
                handleChange={this.props.handleChange}
                classes={classes}
                addVisibleProtocolParam={this.props.addVisibleProtocolParam}
                removeVisibleProtocolParam={this.props.removeVisibleProtocolParam}
              />);
            }) }
          </div> }
        </Fragment>
      )
    }
  }
}

ParametersFieldGeneration.propTypes = {
  paramFieldData: PropTypes.object.isRequired,
  error: PropTypes.object.isRequired,
  defaultValue: PropTypes.object.isRequired,
  helperText: PropTypes.object.isRequired,
  handleChange: PropTypes.func.isRequired
};

export default withStyles(modalStyle)(ParametersFieldGeneration);