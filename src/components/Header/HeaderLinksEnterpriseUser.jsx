/*eslint-disable*/
import React from "react";
// react components for routing our app without refresh
import { Link } from "react-router-dom";
import { env } from '../../utils/config';
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";

// @material-ui/icons
import { Apps, Settings } from "@material-ui/icons";

// core components
import CustomDropdown from "../CustomDropdown/CustomDropdown.jsx";
import Button from "../CustomButtons/Button.jsx";

import headerLinksStyle from "./headerLinksStyle.jsx";

function HeaderLinksEnterpriseAdmin({ ...props }) {
  const { classes } = props;
  return (
    <List className={classes.list}>
      <ListItem className={classes.listItem}>
        <Button
          component={Link} to={`${env.REACT_APP_DOMAIN}applications`}
          color="transparent"
          className={classes.navLink}
        >
          <Apps className={classes.icons} /> Apps
        </Button>
      </ListItem>
      <ListItem className={classes.listItem}>
        <CustomDropdown
          noLiPadding
          hoverColor='info'
          buttonText=""
          buttonProps={{
            className: classes.navLink,
            color: "transparent"
          }}
          buttonIcon={Settings}
          dropdownList={[
            <Link to={`${env.REACT_APP_DOMAIN}account`} className={classes.dropdownLink}>Account</Link>,
            <Link to={`${env.REACT_APP_DOMAIN}login`} className={classes.dropdownLink}>Logout</Link>
          ]}
        />
      </ListItem>
    </List>
  );
}

export default withStyles(headerLinksStyle)(HeaderLinksEnterpriseAdmin);
