import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { env } from '../utils/config';

export const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => (
    localStorage.getItem('user')
      ? <Component {...props} />
      : <Redirect to={{ pathname: `${env.REACT_APP_DOMAIN}login`, state: { from: props.location } }} />
  )} />
);