import {deviceConstants} from '../constants';
import {deviceService} from '../services';
import {alertActions} from "./index";

export const deviceActions = {
  getDecoders,
  getProtocols,
  addDecoder,
  updateDecoder,
  deleteDecoder,
  add,
  getAll,
  update,
  delete: _delete,
  getLast10Alerts,
  getMessageCount,
  getLastMessage,
  getDeviceTypes,
  getProfiles,
  getDeviceTypeData,
  sendPayload
};

function getDecoders(protocolId = '') {
  return dispatch => {
    dispatch(request());
    deviceService.getDecoders(protocolId)
      .then(
        response => dispatch(success(response.decoders)),
        error => dispatch(failure(error.toString()))
      );
  };

  function request() { return { type: deviceConstants.GET_DECODERS_REQUEST } }
  function success(decoders) { return { type: deviceConstants.GET_DECODERS_SUCCESS, decoders } }
  function failure(error) { return { type: deviceConstants.GET_DECODERS_FAILURE, error } }
}

function getProtocols() {
  return dispatch => {
    dispatch(request());
    deviceService.getProtocols()
      .then(
        response => dispatch(success(response.protocols)),
        error => dispatch(failure(error.toString()))
      );
  };

  function request() { return { type: deviceConstants.GET_PROTOCOLS_REQUEST } }
  function success(protocols) { return { type: deviceConstants.GET_PROTOCOLS_SUCCESS, protocols } }
  function failure(error) { return { type: deviceConstants.GET_PROTOCOLS_FAILURE, error } }
}

function addDecoder(decoder) {
  return dispatch => {
    dispatch(request());
    deviceService.addDecoder(decoder)
      .then(
        (newDecoder) => {
          decoder.id = newDecoder.decoder_id;
          dispatch(success(decoder));
          dispatch(alertActions.success('Decoder added successfully'));
        },
        error => {
          dispatch(failure(error.toString()));
          dispatch(alertActions.error(error.toString()));
        }
      );
  };

  function request() { return { type: deviceConstants.ADD_DECODER_REQUEST } }
  function success(decoder) { return { type: deviceConstants.ADD_DECODER_SUCCESS, decoder } }
  function failure(error) { return { type: deviceConstants.ADD_DECODER_FAILURE, error } }
}

function updateDecoder(decoder) {
  return dispatch => {
    dispatch(request(decoder.id));

    deviceService.updateDecoder(decoder)
      .then(
        () => {
          dispatch(success(decoder));
          dispatch(alertActions.success('Decoder updated successfully'));
        },
        error => {
          dispatch(failure(decoder.id, error.toString()));
          dispatch(alertActions.error(error.toString()));
        }
      );
  };

  function request(id) { return { type: deviceConstants.UPDATE_DECODER_REQUEST, id } }
  function success(decoder) { return { type: deviceConstants.UPDATE_DECODER_SUCCESS, decoder } }
  function failure(id, error) { return { type: deviceConstants.UPDATE_DECODER_FAILURE, id, error } }
}

function deleteDecoder(id) {
  return dispatch => {
    dispatch(request(id));

    deviceService.deleteDecoder(id)
      .then(
        () => {
          dispatch(success(id));
          dispatch(alertActions.success(`Decoder ${id} deleted successfully`));
        },
        error => {
          dispatch(failure(id, error.toString()));
          dispatch(alertActions.error(error.toString(), [id]));
        }
      );
  };

  function request(id) { return { type: deviceConstants.DELETE_DECODER_REQUEST, id } }
  function success(id) { return { type: deviceConstants.DELETE_DECODER_SUCCESS, id } }
  function failure(id, error) { return { type: deviceConstants.DELETE_DECODER_FAILURE, id, error } }
}

// noinspection JSUnusedLocalSymbols
// eslint-disable-next-line
function addProfile(profile, deviceTypeId) {
  return dispatch => {
    dispatch(request());
    deviceService.addProfile(profile, deviceTypeId)
      .then(
        (profileId) => {
          dispatch(success(profileId));
          dispatch(alertActions.success(`Profile ${profile.name} added successfully`));
        },
        error => {
          dispatch(failure(error.toString()));
          dispatch(alertActions.error(error.toString()));
        }
      );
  };

  function request() { return { type: deviceConstants.ADD_PROFILE_REQUEST } }
  function success(profileId) { return { type: deviceConstants.ADD_PROFILE_SUCCESS, profileId } }
  function failure(error) { return { type: deviceConstants.ADD_PROFILE_FAILURE, error } }
}

function add(device) {
  return dispatch => {
    if (device.profile !== null) {
      if (device.deviceType !== null) {
        return deviceService.addDeviceType(device.deviceType)
          .then(
            newDeviceType => {
              dispatch(alertActions.success(`Device type ${device.deviceType.name} added successfully`));

              return deviceService.addProfile(device.profile, newDeviceType.id)
                .then(
                  newProfile => {
                    dispatch(alertActions.success(`Profile ${device.profile.name} added successfully`));
                    device.deviceData.device_type_id = newDeviceType.id;
                    device.deviceData.profile_id = newProfile.id;
                    dispatch(request());
                    return deviceService.add(device.deviceData)
                      .then(
                        (newDevice) => {
                          device.deviceData.id = newDevice.id;
                          dispatch(success(device.deviceData));
                          dispatch(alertActions.success(`Device ${device.deviceData.name} added successfully`));
                        },
                        error => {
                          dispatch(failure(device.deviceData.name, error.toString()));
                          dispatch(alertActions.error(error.toString(), [device.deviceData.name]));
                        }
                      )
                  },
                  error => {
                    dispatch(failure(device.profile.name, error.toString()));
                    dispatch(alertActions.error(error.toString(), [device.profile.name]));
                  }
                );
            },
            error => {
              dispatch(failure(device.deviceType.name, error.toString()));
              dispatch(alertActions.error(error.toString(), [device.deviceType.name]));
            }
          );
      } else {
        return deviceService.addProfile(device.profile, device.deviceData.device_type_id)
          .then(
            newProfile => {
              dispatch(alertActions.success(`Profile ${device.profile.name} added successfully`));
              device.deviceData.profile_id = newProfile.id;
              dispatch(request());
              return deviceService.add(device.deviceData)
                .then(
                  (newDevice) => {
                    device.deviceData.id = newDevice.id;
                    dispatch(success(device.deviceData));
                    dispatch(alertActions.success(`Device ${device.deviceData.name} added successfully`));
                  },
                  error => {
                    dispatch(failure(device.deviceData.name, error.toString()));
                    dispatch(alertActions.error(error.toString(), [device.deviceData.name]));
                  }
                )
            },
            error => {
              dispatch(failure(device.profile.name, error.toString()));
              dispatch(alertActions.error(error.toString(), [device.profile.name]));
            }
          );
      }
    }

    dispatch(request());
    return deviceService.add(device.deviceData)
      .then(
        (newDevice) => {
          device.deviceData.id = newDevice.id;
          dispatch(success(device.deviceData));
          dispatch(alertActions.success(`Device ${device.deviceData.name} added successfully`));
        },
        error => {
          dispatch(failure(device.deviceData.name, error.toString()));
          dispatch(alertActions.error(error.toString(), [device.deviceData.name]));
        }
      );
  };

  function request() { return { type: deviceConstants.ADD_REQUEST } }
  function success(device) { return { type: deviceConstants.ADD_SUCCESS, device } }
  function failure(error) { return { type: deviceConstants.ADD_FAILURE, error } }
}

function getAll() {
  return dispatch => {
    dispatch(request());

    deviceService.getAll()
      .then(
        response => {
          response.devices.forEach(device => {
            dispatch(getMessageCount(device.id));
            dispatch(getLastMessage(device.id));
            dispatch(getLast10Alerts(device.id));
          });
          dispatch(success(response.devices));
        },
        error => dispatch(failure(error.toString()))
      );
  };

  function request() { return { type: deviceConstants.GETALL_REQUEST } }
  function success(devices) { return { type: deviceConstants.GETALL_SUCCESS, devices } }
  function failure(error) { return { type: deviceConstants.GETALL_FAILURE, error } }
}

function getMessageCount(deviceId) {
  return dispatch => {
    dispatch(request());

    deviceService.getMessageCount(deviceId)
      .then(
        messageCount => dispatch(success(messageCount)),
        error => dispatch(failure(error.toString()))
      );
  };

  function request() { return { type: deviceConstants.GET_MESSAGE_COUNT_REQUEST } }
  function success(messageCount) { return { type: deviceConstants.GET_MESSAGE_COUNT_SUCCESS, messageCount, deviceId } }
  function failure(error) { return { type: deviceConstants.GET_MESSAGE_COUNT_FAILURE, error } }
}

function getLastMessage(deviceId) {
  return dispatch => {
    dispatch(request());

    deviceService.getLastMessage(deviceId)
      .then(
        lastMessages => {
          dispatch(success(lastMessages));
        },
        error => dispatch(failure(error.toString()))
      );
  };

  function request() { return { type: deviceConstants.GET_LAST_MESSAGE_REQUEST } }
  function success(lastMessages) { return { type: deviceConstants.GET_LAST_MESSAGE_SUCCESS, lastMessages, deviceId } }
  function failure(error) { return { type: deviceConstants.GET_LAST_MESSAGE_FAILURE, error } }
}

function getLast10Alerts(deviceId) {
  return dispatch => {
    dispatch(request());

    deviceService.getLast10Alerts(deviceId)
      .then(
        last10Alerts => dispatch(success(last10Alerts)),
        error => dispatch(failure(error.toString()))
      );
  };

  function request() { return { type: deviceConstants.GET_LAST_ALERT_REQUEST } }
  function success(last10Alerts) { return { type: deviceConstants.GET_LAST_ALERT_SUCCESS, last10Alerts, deviceId } }
  function failure(error) { return { type: deviceConstants.GET_LAST_ALERT_FAILURE, error } }
}

function getDeviceTypes(protocolId) {
  return dispatch => {
    dispatch(request());
    deviceService.getDeviceTypes(protocolId)
      .then(
        deviceTypes => dispatch(success(deviceTypes.device_types)),
        error => dispatch(failure(error.toString()))
      );
  };

  function request() { return { type: deviceConstants.GET_DEVICE_TYPES_REQUEST } }
  function success(deviceTypes) { return { type: deviceConstants.GET_DEVICE_TYPES_SUCCESS, deviceTypes } }
  function failure(error) { return { type: deviceConstants.GET_DEVICE_TYPES_FAILURE, error } }
}

function getDeviceTypeData(deviceTypeId, deviceSelected = false) {
  return dispatch => {
    dispatch(request());
    deviceService.getDeviceTypeData(deviceTypeId)
      .then(
        deviceTypeData => {
          dispatch(success(deviceTypeData));
          if (deviceSelected) {
            dispatch(getDeviceTypes(deviceTypeData.protocol_id));
            dispatch(getDecoders(deviceTypeData.protocol_id));
            dispatch(getProfiles(deviceTypeId));
          }
        },
        error => dispatch(failure(error.toString()))
      );
  };

  function request() { return { type: deviceConstants.GET_DEVICE_TYPE_DATA_REQUEST } }
  function success(deviceTypeData) { return { type: deviceConstants.GET_DEVICE_TYPE_DATA_SUCCESS, deviceTypeData, deviceTypeId } }
  function failure(error) { return { type: deviceConstants.GET_DEVICE_TYPE_DATA_FAILURE, error } }
}

function getProfiles(deviceTypeId) {
  return dispatch => {
    dispatch(request());
    deviceService.getProfiles(deviceTypeId)
      .then(
        response => dispatch(success(response.profiles)),
        error => dispatch(failure(error.toString()))
      );
  };

  function request() { return { type: deviceConstants.GET_PROFILES_REQUEST } }
  function success(profiles) { return { type: deviceConstants.GET_PROFILES_SUCCESS, profiles, deviceTypeId } }
  function failure(error) { return { type: deviceConstants.GET_PROFILES_FAILURE, error } }
}

function sendPayload(device, payload) {
  return dispatch => {
    dispatch(request());
    deviceService.sendPayload(device.id, payload)
      .then(
        result => {
          dispatch(success());
          dispatch(alertActions.success(`Payload sent successfully for device ${device.name}`));
        },
        error => {
          dispatch(failure(device.name, error.toString()));
          dispatch(alertActions.error(error.toString(), [device.name]));
        }
      );
  };

  function request() { return { type: deviceConstants.SEND_PAYLOAD_REQUEST } }
  function success() { return { type: deviceConstants.SEND_PAYLOAD_SUCCESS } }
  function failure(error) { return { type: deviceConstants.SEND_PAYLOAD_FAILURE, error } }
}

function update(device) {
  return dispatch => {
    if (device.profile !== null) {
      if (device.deviceType !== null) {
        return deviceService.addDeviceType(device.deviceType)
          .then(
            newDeviceType => {
              dispatch(alertActions.success(`Device type ${device.deviceType.name} added successfully`));

              return deviceService.addProfile(device.profile, newDeviceType.id)
                .then(
                  newProfile => {
                    dispatch(alertActions.success(`Profile ${device.profile.name} added successfully`));
                    device.deviceData.device_type_id = newDeviceType.id;
                    device.deviceData.profile_id = newProfile.id;
                    dispatch(request());
                    return deviceService.update(device.deviceData)
                      .then(
                        (newDevice) => {
                          device.deviceData.id = newDevice.id;
                          dispatch(success(device.deviceData));
                          dispatch(alertActions.success(`Device ${device.deviceData.name} updated successfully`));
                        },
                        error => {
                          dispatch(failure(device.deviceData.name, error.toString()));
                          dispatch(alertActions.error(error.toString(), [device.deviceData.name]));
                        }
                      )
                  },
                  error => {
                    dispatch(failure(device.profile.name, error.toString()));
                    dispatch(alertActions.error(error.toString(), [device.profile.name]));
                  }
                );
            },
            error => {
              dispatch(failure(device.deviceType.name, error.toString()));
              dispatch(alertActions.error(error.toString(), [device.deviceType.name]));
            }
          );
      } else {
        return deviceService.addProfile(device.profile, device.deviceData.device_type_id)
          .then(
            newProfile => {
              dispatch(alertActions.success(`Profile ${device.profile.name} added successfully`));
              device.deviceData.profile_id = newProfile.id;
              dispatch(request());
              return deviceService.update(device.deviceData)
                .then(
                  () => {
                    dispatch(success(device.deviceData));
                    dispatch(alertActions.success(`Device ${device.deviceData.name} updated successfully`));
                  },
                  error => {
                    dispatch(failure(device.deviceData.name, error.toString()));
                    dispatch(alertActions.error(error.toString(), [device.deviceData.name]));
                  }
                )
            },
            error => {
              dispatch(failure(device.profile.name, error.toString()));
              dispatch(alertActions.error(error.toString(), [device.profile.name]));
            }
          );
      }
    }

    dispatch(request());
    return deviceService.update(device.deviceData)
      .then(
        () => {
          dispatch(success(device.deviceData));
          dispatch(alertActions.success(`Device ${device.deviceData.name} updated successfully`));
        },
        error => {
          dispatch(failure(device.deviceData.name, error.toString()));
          dispatch(alertActions.error(error.toString(), [device.deviceData.name]));
        }
      );
  };

  function request(id) { return { type: deviceConstants.UPDATE_REQUEST, id } }
  function success(device) { return { type: deviceConstants.UPDATE_SUCCESS, device } }
  function failure(id, error) { return { type: deviceConstants.UPDATE_FAILURE, id, error } }
}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
  return dispatch => {
    dispatch(request(id));

    deviceService.delete(id)
      .then(
        () => {
          dispatch(success(id));
          dispatch(alertActions.success('Device deleted successfully'));
        },
        error => dispatch(failure(id, error.toString()))
      );
  };

  function request(id) { return { type: deviceConstants.DELETE_REQUEST, id } }
  function success(id) { return { type: deviceConstants.DELETE_SUCCESS, id } }
  function failure(id, error) { return { type: deviceConstants.DELETE_FAILURE, id, error } }
}

