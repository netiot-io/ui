import {applicationConstants} from '../constants';
import {applicationService} from '../services';
import { alertActions } from './';

export const applicationActions = {
  addApplication,
  getUserApplications,
  getApplicationTypes,
  getApplicationConfig,
  updateApplication,
  deleteApplication,
  deleteStoreApplicationConfig
};

function addApplication(application) {
  return dispatch => {
    dispatch(request());
    applicationService.addApplication(application)
      .then(
        (newApplication) => {
          application.id = newApplication.id;
          dispatch(alertActions.success('Application added successfully'));
          return applicationService.addApplicationConfig(newApplication.id, application.application_path, application.data)
            .then(
              () => {
                dispatch(success(application));
                dispatch(alertActions.success(`Application config added successfully`));
              },
              error => dispatch(failure(error.toString()))
            );
        },
        error => {
          dispatch(failure(error.toString()));
          dispatch(alertActions.error(error.toString()));
        }
      );
  };

  function request() { return { type: applicationConstants.ADD_APPLICATION_REQUEST } }
  function success(application) { return { type: applicationConstants.ADD_APPLICATION_SUCCESS, application } }
  function failure(error) { return { type: applicationConstants.ADD_APPLICATION_FAILURE, error } }
}

function getUserApplications() {
  return dispatch => {
    dispatch(request());
    applicationService.getUserApplications()
      .then(
        response => dispatch(success(response.application_instances)),
        error => dispatch(failure(error.toString()))
      );
  };

  function request() { return { type: applicationConstants.GET_USER_APPLICATIONS_REQUEST } }
  function success(applications) { return { type: applicationConstants.GET_USER_APPLICATIONS_SUCCESS, applications } }
  function failure(error) { return { type: applicationConstants.GET_USER_APPLICATIONS_FAILURE, error } }
}

function getApplicationTypes() {
  return dispatch => {
    dispatch(request());
    applicationService.getApplicationTypes()
      .then(
        response => dispatch(success(response.application_types)),
        error => dispatch(failure(error.toString()))
      );
  };

  function request() { return { type: applicationConstants.GET_APPLICATION_TYPES_REQUEST } }
  function success(applicationTypes) { return { type: applicationConstants.GET_APPLICATION_TYPES_SUCCESS, applicationTypes } }
  function failure(error) { return { type: applicationConstants.GET_APPLICATION_TYPES_FAILURE, error } }
}

function getApplicationConfig(applicationPath, applicationId = -1) {
  return dispatch => {
    dispatch(request());
    return applicationService.getApplicationConfig(applicationPath, applicationId)
      .then(
        applicationConfigJson => dispatch(success(applicationConfigJson)),
        error => dispatch(failure(error.toString()))
      );
  };

  function request() { return { type: applicationConstants.GET_APPLICATION_CONFIG_REQUEST } }
  function success(applicationConfig) { return { type: applicationConstants.GET_APPLICATION_CONFIG_SUCCESS, applicationConfig } }
  function failure(error) { return { type: applicationConstants.GET_APPLICATION_CONFIG_FAILURE, error } }
}

function updateApplication(application) {
  return dispatch => {
    dispatch(request(application.id));

    applicationService.updateApplication(application)
      .then(
        () => {
          dispatch(alertActions.success('Application updated successfully'));

          return applicationService.addApplicationConfig(application.id, application.application_path, application.data)
            .then(
              () => {
                dispatch(success(application));
                dispatch(alertActions.success(`Application config updated successfully`));
              },
              error => dispatch(failure(error.toString()))
            );
        },
        error => {
          dispatch(failure(error.toString()));
          dispatch(alertActions.error(error.toString()));
        }
      );
  };

  function request(id) { return { type: applicationConstants.UPDATE_APPLICATIONS_REQUEST, id } }
  function success(application) { return { type: applicationConstants.UPDATE_APPLICATIONS_SUCCESS, application } }
  function failure(id, error) { return { type: applicationConstants.UPDATE_APPLICATIONS_FAILURE, id, error } }
}

function deleteApplication(id) {
  return dispatch => {
    dispatch(request(id));

    applicationService.deleteApplication(id)
      .then(
        () => dispatch(success(id)),
        error => dispatch(failure(id, error.toString()))
      );
  };

  function request(id) { return { type: applicationConstants.DELETE_APPLICATION_REQUEST, id } }
  function success(id) { return { type: applicationConstants.DELETE_APPLICATION_SUCCESS, id } }
  function failure(id, error) { return { type: applicationConstants.DELETE_APPLICATION_FAILURE, id, error } }
}

function deleteStoreApplicationConfig() {
  return dispatch => {
    dispatch(success());
  };

  function success() { return { type: applicationConstants.DELETE_STORE_APPLICATION_CONFIG_SUCCESS} }
}