import {userConstants} from '../constants';
import {userService} from '../services';
import { alertActions } from './';
import { history } from '../utils/store';
import { env } from '../utils/config';

export const userActions = {
  login,
  logout,
  register,
  activate,
  getUserInfo,
  getOrganizations,
  getOrganizationUsers,
  addOrganization,
  updateOrganization,
  deleteOrganization,
  getAll,
  add,
  update,
  delete: _delete,
  setPassword,
  getPartnerInfo,
  changePassword,
  resetPassword
};

function login(username, password) {
  return dispatch => {
    dispatch(request({ username }));

    userService.login(username, password)
      .then(
        user => {
          dispatch(success(user));
          dispatch(userActions.getUserInfo());
          history.push(`${env.REACT_APP_DOMAIN}`);
        },
        error => {
          dispatch(failure(error.toString()));
          dispatch(alertActions.error(error.toString()));
        }
      );
  };

  function request(user) { return { type: userConstants.LOGIN_REQUEST, user } }
  function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
  function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
}

function logout() {
  userService.logout();
  return { type: userConstants.LOGOUT };
}

function activate(token, password = null) {
  return dispatch => {
    dispatch(request(token));

    userService.activate(token, password)
      .then(
        () => {
          dispatch(success());
          if (password != null) {
            history.push(`${env.REACT_APP_DOMAIN}login`);
            dispatch(alertActions.success('Password set! You can now login.'));
          } else {
            dispatch(alertActions.success('Account activated! You can now login.'));
          }
        },
        error => {
          dispatch(failure(error.toString()));
          dispatch(alertActions.error(error.toString()));
        }
      );
  };

  function request(token) { return { type: userConstants.ACTIVATION_REQUEST, token } }
  function success() { return { type: userConstants.ACTIVATION_SUCCESS} }
  function failure(error) { return { type: userConstants.ACTIVATION_FAILURE, error } }
}

function register(user) {
  return dispatch => {
    dispatch(request(user));

    userService.register(user)
      .then(
        () => {
          dispatch(success());
          history.push(`${env.REACT_APP_DOMAIN}login`);
          dispatch(alertActions.success(`Registration successful! Please check your email to activate your account.`));
        },
        error => {
          dispatch(failure(error.toString()));
          dispatch(alertActions.error(error.toString()));
        }
      );
  };

  function request(user) { return { type: userConstants.REGISTER_REQUEST, user } }
  function success(user) { return { type: userConstants.REGISTER_SUCCESS, user } }
  function failure(error) { return { type: userConstants.REGISTER_FAILURE, error } }
}

function getUserInfo() {
  return dispatch => {
    dispatch(request());

    userService.getUserInfo()
      .then(
        user => dispatch(success(user)),
        error => {
          dispatch(failure(error.toString()));
          dispatch(alertActions.error(error.toString()));
        }
      );
  };

  function request() { return { type: userConstants.GET_USER_INFO_REQUEST } }
  function success(user) { return { type: userConstants.GET_USER_INFO_SUCCESS, user } }
  function failure(error) { return { type: userConstants.GET_USER_INFO_FAILURE, error } }
}

function getOrganizations(requestUsers = false) {
  return dispatch => {
    dispatch(request());
    userService.getOrganizations()
      .then(
        organizations => {
          if (requestUsers) {
            organizations.forEach(organization => {
              dispatch(getOrganizationUsers(organization.id));
            });
          }
          dispatch(success(organizations));
        },
        error => dispatch(failure(error.toString()))
      );
  };

  function request() { return { type: userConstants.GET_ORGANIZATIONS_REQUEST } }
  function success(organizations) { return { type: userConstants.GET_ORGANIZATIONS_SUCCESS, organizations } }
  function failure(error) { return { type: userConstants.GET_ORGANIZATIONS_FAILURE, error } }
}

function getOrganizationUsers(organizationId) {
  return dispatch => {
    dispatch(request());
    userService.getOrganizationUsers(organizationId)
      .then(
        users => dispatch(success(users)),
        error => dispatch(failure(error.toString()))
      );
  };

  function request() { return { type: userConstants.GET_ORGANIZATION_USERS_REQUEST } }
  function success(users) { return { type: userConstants.GET_ORGANIZATION_USERS_SUCCESS, users, organizationId } }
  function failure(error) { return { type: userConstants.GET_ORGANIZATION_USERS_FAILURE, error } }
}

function addOrganization(organization) {
  return dispatch => {
    dispatch(request());
    userService.addOrganization(organization)
      .then(
        (newOrganization) => {
          organization.id = newOrganization.organization_id;
          dispatch(success(organization));
          dispatch(alertActions.success('Organization added successfully'));
        },
        error => {
          dispatch(failure(error.toString()));
          dispatch(alertActions.error(error.toString()));
        }
      );
  };

  function request() { return { type: userConstants.ADD_ORGANIZATIONS_REQUEST } }
  function success(organization) { return { type: userConstants.ADD_ORGANIZATIONS_SUCCESS, organization } }
  function failure(error) { return { type: userConstants.ADD_ORGANIZATIONS_FAILURE, error } }
}

function updateOrganization(organization) {
  return dispatch => {
    dispatch(request(organization.id));

    userService.updateOrganization(organization)
      .then(
        () => {
          dispatch(success(organization));
          dispatch(alertActions.success('Organization updated successfully'));
        },
        error => {
          dispatch(failure(organization.id, error.toString()));
          dispatch(alertActions.error(error.toString()));
        }
      );
  };

  function request(id) { return { type: userConstants.UPDATE_ORGANIZATIONS_REQUEST, id } }
  function success(organization) { return { type: userConstants.UPDATE_ORGANIZATIONS_SUCCESS, organization } }
  function failure(id, error) { return { type: userConstants.UPDATE_ORGANIZATIONS_FAILURE, id, error } }
}

function deleteOrganization(id) {
  return dispatch => {
    dispatch(request(id));

    userService.deleteOrganization(id)
      .then(
        () => dispatch(success(id)),
        error => dispatch(failure(id, error.toString()))
      );
  };

  function request(id) { return { type: userConstants.DELETE_ORGANIZATIONS_REQUEST, id } }
  function success(id) { return { type: userConstants.DELETE_ORGANIZATIONS_SUCCESS, id } }
  function failure(id, error) { return { type: userConstants.DELETE_ORGANIZATIONS_FAILURE, id, error } }
}

function getAll() {
  return dispatch => {
    dispatch(request());

    userService.getAll()
      .then(
        users => dispatch(success(users)),
        error => dispatch(failure(error.toString()))
      );
  };

  function request() { return { type: userConstants.GETALL_REQUEST } }
  function success(users) { return { type: userConstants.GETALL_SUCCESS, users } }
  function failure(error) { return { type: userConstants.GETALL_FAILURE, error } }
}

function add(user) {
  return dispatch => {
    dispatch(request());
    userService.add(user)
      .then(
        (newUser) => {
          user.id = newUser.user_id;
          dispatch(success(user));
          dispatch(alertActions.success('User added successfully'));
        },
        error => {
          dispatch(failure(error.toString()));
          dispatch(alertActions.error(error.toString()));
        }
      );
  };

  function request() { return { type: userConstants.ADD_REQUEST } }
  function success(user) { return { type: userConstants.ADD_SUCCESS, user } }
  function failure(error) { return { type: userConstants.ADD_FAILURE, error } }
}

function update(user) {
  return dispatch => {
    dispatch(request());
    userService.update(user)
      .then(
        () => {
          dispatch(success(user));
          dispatch(alertActions.success('User updated successfully'));
        },
        error => {
          dispatch(failure(error.toString()));
          dispatch(alertActions.error(error.toString()));
        }
      );
  };

  function request() { return { type: userConstants.UPDATE_REQUEST } }
  function success(user) { return { type: userConstants.UPDATE_SUCCESS, user } }
  function failure(error) { return { type: userConstants.UPDATE_FAILURE, error } }
}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
  return dispatch => {
    dispatch(request(id));

    userService.delete(id)
      .then(
        () => {
          dispatch(success(id));
          dispatch(alertActions.success(`User ${id} deleted successfully`));
        },
        error => {
          dispatch(failure(id, error.toString()));
          dispatch(alertActions.error(error.toString(), [id]));
        }
      );
  };

  function request(id) { return { type: userConstants.DELETE_REQUEST, id } }
  function success(id) { return { type: userConstants.DELETE_SUCCESS, id } }
  function failure(id, error) { return { type: userConstants.DELETE_FAILURE, id, error } }
}

function setPassword(token, password) {
  return dispatch => {
    dispatch(request());
    userService.setPassword(token, password)
      .then(
        () => {
          dispatch(success());
          history.push(`${env.REACT_APP_DOMAIN}login`);
          dispatch(alertActions.success('Password set! You can now login'));
        },
        error => {
          dispatch(failure(error.toString()));
          dispatch(alertActions.error(error.toString()));
        }
      );
  };

  function request() { return { type: userConstants.SET_PASSWORD_REQUEST } }
  function success() { return { type: userConstants.SET_PASSWORD_SUCCESS } }
  function failure(error) { return { type: userConstants.SET_PASSWORD_FAILURE, error } }
}

function getPartnerInfo() {
  return dispatch => {
    dispatch(request());
    userService.getPartnerInfo()
      .then(
        partner => dispatch(success(partner)),
        error => dispatch(failure(error.toString()))
      );
  };

  function request() { return { type: userConstants.GET_PARTNER_INFO_REQUEST } }
  function success(partner) { return { type: userConstants.GET_PARTNER_INFO_SUCCESS, partner } }
  function failure(error) { return { type: userConstants.GET_PARTNER_INFO_FAILURE, error } }
}

function changePassword(passwords) {
  return dispatch => {
    dispatch(request());
    userService.changePassword(passwords)
      .then(
        () => {
          dispatch(success());
          userService.logout();
          history.push(`${env.REACT_APP_DOMAIN}login`);
          dispatch(alertActions.success('Password set! You can now login'));
        },
        error => dispatch(failure(error.toString()))
      );
  };

  function request() { return { type: userConstants.CHANGE_PASSWORD_REQUEST } }
  function success() { return { type: userConstants.CHANGE_PASSWORD_SUCCESS } }
  function failure(error) { return { type: userConstants.CHANGE_PASSWORD_FAILURE, error } }
}

function resetPassword(username) {
  return dispatch => {
    if (username.length > 0) {
      dispatch(request());
      userService.resetPassword(username)
        .then(
          () => {
            dispatch(success());
            dispatch(alertActions.success('Password reset! Please check your email.'));
          },
          error => dispatch(failure(error.toString()))
        );
    } else {
      dispatch(alertActions.error('Please fill in the username.'));
    }
  };

  function request() { return { type: userConstants.RESET_PASSWORD_REQUEST } }
  function success() { return { type: userConstants.RESET_PASSWORD_SUCCESS } }
  function failure(error) { return { type: userConstants.RESET_PASSWORD_FAILURE, error } }
}