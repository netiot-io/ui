import { alertConstants } from '../constants';

export const alertActions = {
  success,
  error,
  clear
};

function success(message, values = []) {
  return { type: alertConstants.SUCCESS, message, values };
}

function error(message, values = []) {
  return { type: alertConstants.ERROR, message, values };
}

function clear() {
  return { type: alertConstants.CLEAR };
}