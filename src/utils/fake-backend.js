// array in local storage for registered users
// let users = JSON.parse(localStorage.getItem('users')) || [];
// let partnerUsers = JSON.parse(localStorage.getItem('partnerUsers')) || [
//   {
//     id: 1,
//     name: 'u1'
//   },
//   {
//     id: 2,
//     name: 'u2'
//   }
// ];
// let organizations = JSON.parse(localStorage.getItem('organizations')) || [
//   {
//     id: 1,
//     name: "org 1",
//     phone: "1234",
//     address: "teas 2",
//     description: "bla bla",
//     users: [
//       {
//         id: 1,
//         name: 'u1'
//       },
//       {
//         id: 2,
//         name: 'u2'
//       }
//     ]
//   }
// ];

let applications = JSON.parse(localStorage.getItem('applications')) || [
  {
    id: 1,
    name: "app 1",
    description: "bla bla",
    thumbnail: "https://embedwistia-a.akamaihd.net/deliveries/bfda99b020f1affbb7b610fdd198a6baee60b888.jpg?image_crop_resized=1280x720",
    users: [
      {
        id: 9,
        name: 'alex Dinu'
      },
      {
        id: 8,
        name: 'ion Dinu'
      }
    ]
  },
  {
    id: 2,
    name: "app 2",
    description: "tre tre 2",
    thumbnail: "https://themrstee.com/wp-content/uploads/2015/01/iMovie.png",
    users: [
      {
        id: 9,
        name: 'alex Dinu'
      },
      {
        id: 8,
        name: 'ion Dinu'
      }
    ]
  }
];

let applicationTypes = JSON.parse(localStorage.getItem('applicationTypes')) || [
  {
    id: 1,
    name: "Basic",
    description: "Basic chart app"
  },
  {
    id: 2,
    name: "Hotel",
    description: "Hotel POC"
  }
];

let applicationConfig = JSON.parse(localStorage.getItem('applicationConfig')) || {
  schema: {
    "type": "object",
      "properties": {
        "co2_limit": {
          "type": "integer",
          "description": "Please enter the CO2 alert limit"
        },
        "sensors": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "device": {
                "type": "string",
                "enum": ["room 1 (23)", "room 2 (24)"]
              },
              "sensor": {
                "type": "string",
                "enum": ["co2"]
              },
              "id": {
                "type": "integer",
                "uniqueItems": true
              }
            },
            "required": [
              "id", "device", "sensor"
            ]
          }
        }
      },
    "required": [
      "co2_limit"
    ]
  },
  uischema: {
    "type": "VerticalLayout",
    "elements": [
      {
        "type": "HorizontalLayout",
        "elements": [
          {
            "type": "Control",
            "label": "CO2 alert limit",
            "scope": "#/properties/co2_limit"
          }
        ]
      },
      {
        "type": "Control",
        "label": "Sensors",
        "scope": "#/properties/sensors",
        "options": {
          "detail": {
            "type": 'HorizontalLayout',
            "elements": [
              {
                "type": "Control",
                "label": "Device 1",
                "scope": "#/properties/device"
              },
              {
                "type": "Control",
                "label": "Sensor 2",
                "scope": "#/properties/sensor"
              },
              {
                "type": "Control",
                "label": "Room nb",
                "scope": "#/properties/id"
              }
            ]
          }
        }
      }
    ]
  },
  data: {
    "co2_limit": 100,
    "sensors": []
  }
};

// let decoders = JSON.parse(localStorage.getItem('decoders')) || [
//   {
//     id: 1,
//     name: "decoder 1",
//     protocol_id: 1,
//     defined_by_platform: false,
//     description: "bla bla 1",
//     code: "function test() {alert(1);}",
//   },
//   {
//     id: 2,
//     name: "decoder 2",
//     protocol_id: 1,
//     defined_by_platform: false,
//     description: "bla bla 2",
//     code: "function test() {alert(2);}",
//   },
// ];
//
// let protocols = JSON.parse(localStorage.getItem('protocols')) || [
//   {
//     id: 1,
//     name: "LORA",
//     description: "bla bla 1",
//     protocol_parameters: [
//       {id: "loraClass", name: "LORA class", required: "true", value: [
//           {name: "A"}, {name: "B"}, {name: "C"}
//         ]
//       },
//       {id: "activationMethod", name: "Activation method", required: "true", value: [
//           {name: "ABP", extraFields: [
//               {id:"devAddr", name: "DevAddr", required: "true"}, {id: "nwksKey", name: "NwkSKey", required: "true"},
//               {id: "appsKey", name: "AppSKey", required: "true"}
//             ]},
//           {name: "OTAA", extraFields: [
//               {id: "appEui", name: "AppEUI", required: "true"}, {id: "appKey", name: "AppKey", required: "true"}
//             ]
//           }
//         ]
//       }
//     ],
//     device_parameters: [
//       {id: "devEui", name: "DevEUI", required: "true"}
//     ]
//   },
//   {
//     id: 2,
//     name: "HTTP",
//     description: "bla bla 2"
//   },
// ];
//
// let deviceTypes = JSON.parse(localStorage.getItem('deviceTypes')) || [
//   {
//     id: 1,
//     name: "DType 1",
//     description: "bla bla 1",
//     protocol_id: "1",
//     protocol_name: "LORA"
//   },
//   {
//     id: 2,
//     name: "DType 2",
//     description: "bla bla 2",
//     protocol_id: "2",
//     protocol_name: "HTTP"
//   },
// ];
//
// let deviceTypesData = JSON.parse(localStorage.getItem('deviceTypesData')) || [
//   {
//     id: 1,
//     name: "DType 1",
//     description: "bla bla 1",
//     protocol_id: "1",
//     decoder: {
//       id: 1,
//       name: 'decoder 1'
//     },
//     sensors_type : [
//       {id: 1, type: 'Temperature'},
//       {id: 2, type: 'Humidity'}
//     ]
//   },
//   {
//     id: 2,
//     name: "DType 2",
//     description: "bla bla 2",
//     protocol_id: "2",
//     decoder: {
//       id: 2,
//       name: 'decoder 2'
//     },
//     sensors_type : [
//       {id: 1, type: 'Temperature'},
//       {id: 3, type: 'Pressure'}
//     ]
//   },
// ];
//
// let profiles = JSON.parse(localStorage.getItem('profiles')) || [
//   {
//     device_type_id: 1,
//     params: {
//       id: 1,
//       name: 'Profile 1',
//       protocol_parameters: [
//         {id: "loraClass", name: "LORA class", value: "A"},
//         {id: "activationMethod", name: "Activation method", value: "ABP"},
//         {id:"devAddr", name: "DevAddr", value: "1"},
//         {id: "nwksKey", name: "NwkSKey", value: "2"},
//         {id: "appsKey", name: "AppSKey", value: "3"}
//       ],
//       device_parameters: {id: "devEui", name: "DevEUI", value: "xyzt"}
//     }
//   },
//   {
//     device_type_id: 1,
//     params: {
//       id: 2,
//       name: 'Profile 2',
//       protocol_parameters: [
//         {id: "loraClass", name: "LORA class", value: "A"},
//         {id: "activationMethod", name: "Activation method", value: "OTAA"},
//         {id: "appEui", name: "AppEUI", value: "xxx"},
//         {id: "appKey", name: "AppKey", required: "yyy"}
//       ],
//       device_parameters: {id: "devEui", name: "DevEUI", value: "xyz"}
//     }
//   },
//   {
//     device_type_id: 2,
//     params: {
//       id: 2,
//       name: 'Profile 2',
//       protocol_parameters: [
//         {id: "loraClass", name: "LORA class", value: "A"},
//         {id: "activationMethod", name: "Activation method", value: "OTAA"},
//         {id: "appEui", name: "AppEUI", value: "xxx"},
//         {id: "appKey", name: "AppKey", required: "yyy"}
//       ],
//       device_parameters: {id: "devEui", name: "DevEUI", value: "xyz"}
//     }
//   }
// ];
//
// let devices = JSON.parse(localStorage.getItem('devices')) || [
//   {
//     id: 1,
//     name: 'device 1',
//     description: 'dummy desc 1',
//     device_type_id: 1,
//     profile_id: 1,
//     device_parameters: [
//       {id: "devEui", name: "DevEUI", value: "xyzt"}
//     ],
//     apps: [
//       {
//         id: 1,
//         name: 'generic chart'
//       },
//       {
//         id: 2,
//         name: 'generic gauge'
//       }
//     ],
//     users: [
//       {
//         id: 9,
//         name: 'alex Dinu'
//       },
//       {
//         id: 8,
//         name: 'ion Dinu'
//       }
//     ],
//     activate: false
//   },
//   {
//     id: 2,
//     name: 'device 2',
//     description: 'dummy desc 2',
//     device_type_id: 2,
//     profile_id: 2,
//     device_parameters: [
//       {id: "devEui", name: "DevEUI", value: "xyz"}
//     ],
//     apps: [
//       {
//         id: 1,
//         name: 'generic chart'
//       },
//       {
//         id: 2,
//         name: 'generic gauge'
//       }
//     ],
//     users: [
//       {
//         id: 9,
//         name: 'alex Dinu'
//       },
//       {
//         id: 8,
//         name: 'ion Dinu'
//       }
//     ],
//     activate: true
//   }
// ];

// localStorage.setItem('sensorsTypes', JSON.stringify([
//   {id: 1, type: 'Temperature'},
//   {id: 2, type: 'Humidity'},
//   {id: 3, type: 'Pressure'}
// ]));
// let sensorsTypes = JSON.parse(localStorage.getItem('sensorsTypes'));

// noinspection JSUnusedGlobalSymbols
export function configureFakeBackend() {
  let realFetch = window.fetch;
  window.fetch = function (url, opts) {
    return new Promise((resolve, reject) => {
      // wrap in timeout to simulate server api call
      setTimeout(() => {

        // // authenticate
        // if (url.endsWith('/users/authenticate') && opts.method === 'POST') {
        //   // get parameters from post request
        //   let params = JSON.parse(opts.body);
        //   // find if any user matches login credentials
        //   let filteredUsers = users.filter(user => {
        //     return user.username === params.username && user.password === params.password;
        //   });
        //
        //   if (filteredUsers.length) {
        //     // if login details are valid return user details and fake jwt token
        //     let user = filteredUsers[0];
        //
        //     if (user.activated) {
        //       let responseJson = {
        //         id: user.id,
        //         username: user.username,
        //         firstName: user.firstname,
        //         lastName: user.lastname,
        //         token: 'fake-jwt-token'
        //       };
        //       resolve({ok: true, json: () => Promise.resolve(responseJson)});
        //     } else {
        //       reject('backend.user.activation.missing');
        //     }
        //   } else {
        //     // else return error
        //     reject('backend.username.password.incorrect');
        //   }
        //
        //   return;
        // }

        // test payload
        if (url.endsWith('payload/') && opts.method === 'POST') {
          // check for fake auth token in header and return users if valid, this security is implemented server side in a real application
          if (opts.headers && opts.headers.Authorization.includes('Bearer')) {
            let payload = JSON.parse(opts.body);
            console.log(`Received payload: ${payload}`);
            resolve({ ok: true, json: () => Promise.resolve()});
          } else {
            // return 401 not authorised if token is null or invalid
            reject('backend.unauthorised');
          }

          return;
        }

        // // get sensor types
        // if (url.endsWith('/sensor-type/') && opts.method === 'GET') {
        //   // check for fake auth token in header and return users if valid, this security is implemented server side in a real application
        //   if (opts.headers && opts.headers.Authorization.includes('Bearer')) {
        //     resolve({ ok: true, json: () => Promise.resolve(sensorsTypes)});
        //   } else {
        //     // return 401 not authorised if token is null or invalid
        //     reject('backend.unauthorised');
        //   }
        //
        //   return;
        // }

        // get all devices
        // if (url.endsWith('/devices/') && opts.method === 'GET') {
        //   // check for fake auth token in header and return users if valid, this security is implemented server side in a real application
        //   if (opts.headers && opts.headers.Authorization.includes('Bearer')) {
        //     resolve({ ok: true, json: () => Promise.resolve(devices)});
        //   } else {
        //     // return 401 not authorised if token is null or invalid
        //     reject('backend.unauthorised');
        //   }
        //
        //   return;
        // }

        // delete devices
        // if (url.match(/\/device\/\d+$/) && opts.method === 'DELETE') {
        //   // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
        //   if (opts.headers && opts.headers.Authorization.includes('Bearer')) {
        //     // find user by id in users array
        //     let deleteDeviceUrlParts = url.split('/');
        //     let id = parseInt(deleteDeviceUrlParts[deleteDeviceUrlParts.length - 1], 10);
        //     for (let i = 0; i < devices.length; i++) {
        //       let device = devices[i];
        //       if (device.id === id) {
        //         // delete device
        //         devices.splice(i, 1);
        //         localStorage.setItem('devices', JSON.stringify(devices));
        //         break;
        //       }
        //     }
        //
        //     // respond 200 OK
        //     resolve({ ok: true, json: () => Promise.resolve({}) });
        //   } else {
        //     // return 401 not authorised if token is null or invalid
        //     reject('backend.unauthorised');
        //   }
        //
        //   return;
        // }

        // // add device
        // if (url.endsWith('/device/') && opts.method === 'POST') {
        //   if (opts.headers && opts.headers.Authorization.includes('Bearer')) {
        //     // get new user object from post body
        //     let newDevice = JSON.parse(opts.body);
        //
        //     // validation
        //     // let duplicateDevice = devices.filter(device => { return device.deveui.toLowerCase() === newDevice.deveui.toLowerCase(); }).length;
        //     // if (duplicateDevice) {
        //     //   reject('DEVEUI already in use');
        //     //   return;
        //     // }
        //
        //     // save new device
        //     newDevice.id = devices.length ? Math.max(...devices.map(device => device.id)) + 1 : 1;
        //     devices.push(newDevice);
        //     localStorage.setItem('devices', JSON.stringify(devices));
        //
        //     // respond 200 OK
        //     resolve({ ok: true, json: () => Promise.resolve({id: newDevice.id}) });
        //   } else {
        //     // return 401 not authorised if token is null or invalid
        //     reject('backend.unauthorised');
        //   }
        //
        //   return;
        // }

        // // update device
        // if (url.match(/\/device\/\d+$/) && opts.method === 'PUT') {
        //   // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
        //   if (opts.headers && opts.headers.Authorization.includes('Bearer')) {
        //     // find user by id in users array
        //     let updateDeviceUrlParts = url.split('/');
        //     let id = parseInt(updateDeviceUrlParts[updateDeviceUrlParts.length - 1], 10);
        //     let newDevice = JSON.parse(opts.body);
        //
        //     // validation
        //     // let duplicateDevice = devices.filter(device => { return device.deveui.toLowerCase() === newDevice.deveui.toLowerCase() && device.id !== id; }).length;
        //     // if (duplicateDevice) {
        //     //   reject('DEVEUI already in use');
        //     //   return;
        //     // }
        //
        //     const newDevices = devices.map(device => {
        //       if(device.id === id)
        //         return newDevice;
        //       return device
        //     });
        //     localStorage.setItem('devices', JSON.stringify(newDevices));
        //
        //     // respond 200 OK
        //     resolve({ ok: true, json: () => Promise.resolve({newDevice}) });
        //   } else {
        //     // return 401 not authorised if token is null or invalid
        //     reject('backend.unauthorised');
        //   }
        //
        //   return;
        // }

        // // get users
        // if (url.endsWith('/users') && opts.method === 'GET') {
        //   // check for fake auth token in header and return users if valid, this security is implemented server side in a real application
        //   if (opts.headers && opts.headers.Authorization === 'Bearer fake-jwt-token') {
        //     resolve({ ok: true, json: () => Promise.resolve(users)});
        //   } else {
        //     // return 401 not authorised if token is null or invalid
        //     reject('backend.unauthorised');
        //   }
        //
        //   return;
        // }

        // // get user by id
        // if (url.match(/\/users\/\d+$/) && opts.method === 'GET') {
        //   // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
        //   if (opts.headers && opts.headers.Authorization === 'Bearer fake-jwt-token') {
        //     // find user by id in users array
        //     let urlParts = url.split('/');
        //     let id = parseInt(urlParts[urlParts.length - 1], 10);
        //     let matchedUsers = users.filter(user => { return user.id === id; });
        //     let user = matchedUsers.length ? matchedUsers[0] : null;
        //
        //     // respond 200 OK with user
        //     resolve({ ok: true, json: () => user});
        //   } else {
        //     // return 401 not authorised if token is null or invalid
        //     reject('backend.unauthorised');
        //   }
        //
        //   return;
        // }
        //
        // // get user organizations
        // if (url.endsWith('/organizations') && opts.method === 'GET') {
        //   // check for fake auth token in header and return users if valid, this security is implemented server side in a real application
        //   if (opts.headers && opts.headers.Authorization === 'Bearer fake-jwt-token') {
        //     resolve({ ok: true, json: () => Promise.resolve(organizations)});
        //   } else {
        //     // return 401 not authorised if token is null or invalid
        //     reject('backend.unauthorised');
        //   }
        //
        //   return;
        // }

        // get user applications
        if (url.endsWith('/applications') && opts.method === 'GET') {
          // check for fake auth token in header and return users if valid, this security is implemented server side in a real application
          if (opts.headers && opts.headers.Authorization.includes('Bearer')) {
            resolve({ ok: true, json: () => Promise.resolve(applications)});
          } else {
            // return 401 not authorised if token is null or invalid
            reject('backend.unauthorised');
          }

          return;
        }

        // get application types
        if (url.endsWith('/applications/types') && opts.method === 'GET') {
          // check for fake auth token in header and return users if valid, this security is implemented server side in a real application
          if (opts.headers && opts.headers.Authorization.includes('Bearer')) {
            resolve({ ok: true, json: () => Promise.resolve(applicationTypes)});
          } else {
            // return 401 not authorised if token is null or invalid
            reject('backend.unauthorised');
          }

          return;
        }

        // get application config
        if (url.endsWith("/config") && opts.method === 'GET') {
          // check for fake auth token in header and return users if valid, this security is implemented server side in a real application
          if (opts.headers && opts.headers.Authorization.includes('Bearer')) {
            resolve({ ok: true, json: () => Promise.resolve(applicationConfig)});
          } else {
            // return 401 not authorised if token is null or invalid
            reject('backend.unauthorised');
          }

          return;
        }

        // update applications
        if (url.match(/\/applications\/\d+$/) && opts.method === 'PUT') {
          // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
          if (opts.headers && opts.headers.Authorization.includes('Bearer')) {
            // find user by id in users array
            let urlParts = url.split('/');
            let id = parseInt(urlParts[urlParts.length - 1], 10);
            let newApplication = JSON.parse(opts.body);

            const newApplications = applications.map(application => {
              if(application.id === id)
                return newApplication;
              return application
            });
            localStorage.setItem('applications', JSON.stringify(newApplications));

            // respond 200 OK
            resolve({ ok: true, json: () => Promise.resolve({newApplication}) });
          } else {
            // return 401 not authorised if token is null or invalid
            reject('backend.unauthorised');
          }

          return;
        }

        // get decoders
        // if (url.indexOf('/decoders/')>0 && opts.method === 'GET') {
        //   // check for fake auth token in header and return users if valid, this security is implemented server side in a real application
        //   if (opts.headers && opts.headers.Authorization.includes('Bearer')) {
        //     const protocolId = url.substring(url.indexOf('protocolId')+11);
        //
        //     // find if any user matches login credentials
        //     let filteredDecoders = decoders;
        //     if (protocolId.length > 0) {
        //       filteredDecoders = decoders.filter(decoder => {
        //         return decoder.protocolId === parseInt(protocolId, 10);
        //       });
        //     }
        //
        //     resolve({ ok: true, json: () => Promise.resolve({decoders: [...filteredDecoders]})});
        //   } else {
        //     // return 401 not authorised if token is null or invalid
        //     reject('backend.unauthorised');
        //   }
        //
        //   return;
        // }

        // get protocols
        // if (url.endsWith('/protocol/') && opts.method === 'GET') {
        //   // check for fake auth token in header and return users if valid, this security is implemented server side in a real application
        //   if (opts.headers && opts.headers.Authorization.includes('Bearer')) {
        //     resolve({ ok: true, json: () => Promise.resolve({protocols})});
        //   } else {
        //     // return 401 not authorised if token is null or invalid
        //     reject('backend.unauthorised');
        //   }
        //
        //   return;
        // }

        // // get message count
        // if (url.endsWith('/count/') && opts.method === 'GET') {
        //   // check for fake auth token in header and return users if valid, this security is implemented server side in a real application
        //   if (opts.headers && opts.headers.Authorization.includes('Bearer')) {
        //     resolve({ ok: true, json: () => Promise.resolve(Math.floor(Math.random() * 100) + 1  )});
        //   } else {
        //     // return 401 not authorised if token is null or invalid
        //     reject('backend.unauthorised');
        //   }
        //
        //   return;
        // }

        // get last message
        // if (url.endsWith('/last-message/') && opts.method === 'GET') {
        //   // check for fake auth token in header and return users if valid, this security is implemented server side in a real application
        //   if (opts.headers && opts.headers.Authorization.includes('Bearer')) {
        //     const message = {
        //       latitude:0,
        //       longitude:0,
        //       timestamp: new Date().getTime(),
        //       devEUI:"DECE1F000BA30400",
        //       devAddr:"89150126",
        //       sensors:[
        //         {
        //           a: 1,
        //           w: {
        //             t: Math.random() * 20 + 1,
        //             tu: "C",
        //             p: Math.random() * 1000 + 1,
        //             pu: "P",
        //             h: Math.random() * 80 + 30
        //           }
        //         }
        //       ]
        //     };
        //     resolve({ ok: true, json: () => Promise.resolve(JSON.stringify(message))});
        //   } else {
        //     // return 401 not authorised if token is null or invalid
        //     reject('backend.unauthorised');
        //   }
        //
        //   return;
        // }

        // get last alert
        // if (url.indexOf('/alert/')>0 && opts.method === 'GET') {
        //   // check for fake auth token in header and return users if valid, this security is implemented server side in a real application
        //   if (opts.headers && opts.headers.Authorization.includes('Bearer')) {
        //     const message = {
        //       totalpages: 1,
        //       totalelements: 1,
        //       size: 1,
        //       numberOfElements : 1,
        //       content: [
        //         {
        //           id: 1,
        //           timestamp: new Date().getTime(),
        //           message: "fireeeee!"
        //         }
        //     ]
        //   };
        //     resolve({ ok: true, json: () => Promise.resolve(JSON.stringify(message))});
        //   } else {
        //     // return 401 not authorised if token is null or invalid
        //     reject('backend.unauthorised');
        //   }
        //
        //   return;
        // }

        // // get device types
        // if (url.indexOf('/device-type/?protocolId=')>0 && opts.method === 'GET') {
        //   // check for fake auth token in header and return users if valid, this security is implemented server side in a real application
        //   if (opts.headers && opts.headers.Authorization.includes('Bearer')) {
        //     resolve({ ok: true, json: () => Promise.resolve(deviceTypes)});
        //   } else {
        //     // return 401 not authorised if token is null or invalid
        //     reject('backend.unauthorised');
        //   }
        //
        //   return;
        // }
        //
        // // get device type data
        // if (url.match(/\/device-type\/\d+$/) && opts.method === 'GET') {
        //   // check for fake auth token in header and return users if valid, this security is implemented server side in a real application
        //   if (opts.headers && opts.headers.Authorization.includes('Bearer')) {
        //     let deviceTypeDataUrlParts = url.split('/');
        //     let deviceTypeId = parseInt(deviceTypeDataUrlParts[deviceTypeDataUrlParts.length - 1], 10);
        //     resolve({ ok: true, json: () => Promise.resolve(deviceTypesData.find(deviceTypeData => deviceTypeData.id === deviceTypeId))});
        //   } else {
        //     // return 401 not authorised if token is null or invalid
        //     reject('backend.unauthorised');
        //   }
        //
        //   return;
        // }
        //
        // // add device type
        // if (url.endsWith('/device-type/') && opts.method === 'POST') {
        //   if (opts.headers && opts.headers.Authorization.includes('Bearer')) {
        //     // get new profile object from post body
        //
        //     let newDeviceType = JSON.parse(opts.body);
        //
        //     // save new device type
        //     newDeviceType.id = Math.max(...deviceTypes.map(deviceType => deviceType.id)) + 1;
        //     deviceTypes.push({
        //       id: newDeviceType.id,
        //       name: newDeviceType.name,
        //       description: newDeviceType.description,
        //       protocol_id: newDeviceType.protocol_id,
        //       protocol_name: protocols.find(protocol => protocol.id === newDeviceType.protocol_id).name
        //     });
        //
        //     deviceTypesData.push({
        //       id: newDeviceType.id,
        //       name: newDeviceType.name,
        //       description: newDeviceType.description,
        //       protocol_id: newDeviceType.protocol_id,
        //       decoder: {
        //         id: newDeviceType.decoder_id,
        //         name: decoders.find(decoder => decoder.id === newDeviceType.decoder_id).name
        //       },
        //       sensors_type: newDeviceType.sensors_type
        //     });
        //
        //     localStorage.setItem('deviceTypes', JSON.stringify(deviceTypes));
        //     localStorage.setItem('deviceTypesData', JSON.stringify(deviceTypesData));
        //
        //     // respond 200 OK
        //     resolve({ ok: true, json: () => Promise.resolve({id: newDeviceType.id}) });
        //   } else {
        //     // return 401 not authorised if token is null or invalid
        //     reject('backend.unauthorised');
        //   }
        //
        //   return;
        // }

        // add decoder
        // if (url.endsWith('/decoders/') > 0 && opts.method === 'POST') {
        //   if (opts.headers && opts.headers.Authorization.includes('Bearer')) {
        //     // get new user object from post body
        //     let newDecoder = JSON.parse(opts.body);
        //
        //     // save new decoder
        //     newDecoder.id = decoders.length ? Math.max(...decoders.map(decoder => decoder.id)) + 1 : 1;
        //     newDecoder.protocolId = protocols.filter(protocol => {
        //       return protocol.name === newDecoder.protocol;
        //     })[0].id;
        //     decoders.push(newDecoder);
        //     localStorage.setItem('decoders', JSON.stringify(decoders));
        //
        //     // respond 200 OK
        //     resolve({ ok: true, json: () => Promise.resolve({decoder_id: newDecoder.id}) });
        //   } else {
        //     // return 401 not authorised if token is null or invalid
        //     reject('backend.unauthorised');
        //   }
        //
        //   return;
        // }

        // update decoder
        // if (url.match(/\/decoders\/\d+$/) && opts.method === 'PUT') {
        //   // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
        //   if (opts.headers && opts.headers.Authorization.includes('Bearer')) {
        //     // find user by id in users array
        //     let urlParts = url.split('/');
        //     let id = parseInt(urlParts[urlParts.length - 1], 10);
        //     let newDecoder = JSON.parse(opts.body);
        //
        //     const newDecoders = decoders.map(decoder => {
        //       if(decoder.id === id)
        //         return newDecoder;
        //       return decoder
        //     });
        //     localStorage.setItem('decoders', JSON.stringify(newDecoders));
        //
        //     // respond 200 OK
        //     resolve({ ok: true, json: () => Promise.resolve({newDecoder}) });
        //   } else {
        //     // return 401 not authorised if token is null or invalid
        //     reject('backend.unauthorised');
        //   }
        //
        //   return;
        // }

        // delete decoder
        // if (url.match(/\/decoders\/\d+$/) && opts.method === 'DELETE') {
        //   // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
        //   if (opts.headers && opts.headers.Authorization.includes('Bearer')) {
        //     // find user by id in users array
        //     let urlParts = url.split('/');
        //     let id = parseInt(urlParts[urlParts.length - 1], 10);
        //     for (let i = 0; i < decoders.length; i++) {
        //       let decoder = decoders[i];
        //       if (decoder.id === id) {
        //         // delete decoder
        //         decoders.splice(i, 1);
        //         localStorage.setItem('decoders', JSON.stringify(decoders));
        //         break;
        //       }
        //     }
        //
        //     // respond 200 OK
        //     resolve({ ok: true, json: () => Promise.resolve({}) });
        //   } else {
        //     // return 401 not authorised if token is null or invalid
        //     reject('backend.unauthorised');
        //   }
        //
        //   return;
        // }

        // // get profiles
        // if (url.endsWith('/profile/') && opts.method === 'GET') {
        //   // check for fake auth token in header and return users if valid, this security is implemented server side in a real application
        //   if (opts.headers && opts.headers.Authorization.includes('Bearer')) {
        //     let profileUrlParts = String(url).split('/');
        //     const deviceTypeId = parseInt(profileUrlParts[profileUrlParts.length - 3], 10);
        //     const filteredProfiles = profiles.filter(profile => profile.device_type_id === deviceTypeId).map(profile => profile.params);
        //     resolve({ ok: true, json: () => Promise.resolve(filteredProfiles)});
        //   } else {
        //     // return 401 not authorised if token is null or invalid
        //     reject('backend.unauthorised');
        //   }
        //
        //   return;
        // }
        //
        // // add profile
        // if (url.endsWith('/profile/') && opts.method === 'POST') {
        //   if (opts.headers && opts.headers.Authorization.includes('Bearer')) {
        //     // get new profile object from post body
        //     let addProfileUrlParts = String(url).split('/');
        //     const addProfileDeviceTypeId = parseInt(addProfileUrlParts[addProfileUrlParts.length - 3], 10);
        //     let newProfile = JSON.parse(opts.body);
        //
        //     // save new profile
        //     newProfile.id = Math.max(...profiles.map(profile => profile.params.id)) + 1;
        //
        //     profiles.push({
        //       device_type_id: addProfileDeviceTypeId,
        //       params: newProfile
        //     });
        //
        //     localStorage.setItem('profiles', JSON.stringify(profiles));
        //
        //     // respond 200 OK
        //     resolve({ ok: true, json: () => Promise.resolve({id: newProfile.id}) });
        //   } else {
        //     // return 401 not authorised if token is null or invalid
        //     reject('backend.unauthorised');
        //   }
        //
        //   return;
        // }

        // // get partner users
        // if (url.endsWith('/me') && opts.method === 'GET') {
        //   // check for fake auth token in header and return users if valid, this security is implemented server side in a real application
        //   if (opts.headers && opts.headers.Authorization === 'Bearer fake-jwt-token') {
        //     resolve({ ok: true, json: () => Promise.resolve(partnerUsers)});
        //   } else {
        //     // return 401 not authorised if token is null or invalid
        //     reject('backend.unauthorised');
        //   }
        //
        //   return;
        // }

        // // register user
        // if (url.endsWith('/users/register') && opts.method === 'POST') {
        //   // get new user object from post body
        //   let newUser = JSON.parse(opts.body);
        //   newUser.username = newUser.email;
        //   delete newUser.email;
        //
        //   // validation
        //   let duplicateUser = users.filter(user => { return user.username === newUser.username; }).length;
        //   if (duplicateUser) {
        //     reject('backend.username.taken:' + newUser.username);
        //     return;
        //   }
        //
        //   // save new user
        //   newUser.id = users.length ? Math.max(...users.map(user => user.id)) + 1 : 1;
        //   newUser.activated = false;
        //   newUser.phone = parseInt(1000 + Math.random() * (10000000 - 1000), 10);
        //   users.push(newUser);
        //   localStorage.setItem('users', JSON.stringify(users));
        //
        //   // respond 200 OK
        //   resolve({ ok: true, json: () => Promise.resolve({id: newUser.id}) });
        //
        //   return;
        // }
        //
        // // activate user
        // if (url.endsWith('/users/activate') && opts.method === 'POST') {
        //   let id = parseInt(JSON.parse(opts.body).id, 10);
        //   // find if any user matches login credentials
        //   let filteredUsers = users.filter(user => {
        //     return user.id === id;
        //   });
        //
        //   if (filteredUsers.length) {
        //     // if login details are valid return user details and fake jwt token
        //     let newUser = filteredUsers[0];
        //     newUser.activated = true;
        //
        //     const newUsers = users.map(user => {
        //       if(user.id === id)
        //         return newUser;
        //       return user
        //     });
        //     localStorage.setItem('users', JSON.stringify(newUsers));
        //
        //     resolve({ ok: true, json: () => Promise.resolve({id: newUser.id}) });
        //   } else {
        //     // else return error
        //     reject('backend.user.activation.failed');
        //   }
        //
        //   return;
        // }
        //
        // // delete user
        // if (url.match(/\/users\/\d+$/) && opts.method === 'DELETE') {
        //   // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
        //   if (opts.headers && opts.headers.Authorization === 'Bearer fake-jwt-token') {
        //     // find user by id in users array
        //     let urlParts = url.split('/');
        //     let id = parseInt(urlParts[urlParts.length - 1], 10);
        //     for (let i = 0; i < users.length; i++) {
        //       let user = users[i];
        //       if (user.id === id) {
        //         // delete user
        //         users.splice(i, 1);
        //         localStorage.setItem('users', JSON.stringify(users));
        //         break;
        //       }
        //     }
        //
        //     // respond 200 OK
        //     resolve({ ok: true, json: () => Promise.resolve({}) });
        //   } else {
        //     // return 401 not authorised if token is null or invalid
        //     reject('backend.unauthorised');
        //   }
        //
        //   return;
        // }

        // pass through any requests not handled above
        realFetch(url, opts).then(response => resolve(response));

      }, 500);
    });
  }
}