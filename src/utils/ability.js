import { AbilityBuilder } from '@casl/ability';

export default (type) =>
  AbilityBuilder.define((can, cannot) => {
    switch(type) {
      case 'ADMIN':
        can('send', 'bacnet');
        can('view', 'user_header');
        break;
      case 'FREE_USER':
        can('view', 'apps');
        can('view', 'devices');
        can('view', 'user_header');
        can('view', 'subscription');
        can('view', 'partner_info');
        break;
      case 'premium_user':
        can('view', 'apps');
        break;
      case 'ENTERPRISE_USER':
        can('view', 'apps');
        can('view', 'partner_info');
        can('view', 'enterprise_user_header');
        break;
      case 'ENTERPRISE_ADMIN':
        cannot('view', 'organizations');
        can('view', 'users');
        can('view', 'devices');
        can('assign', 'devices');
        can('assign', 'applications');
        can('view', 'apps');
        can('view', 'enterprise_admin_header');
        can('view', 'partner_info');
        break;
      case 'PARTNER_USER':
        can('view', 'apps');
        can('view', 'devices');
        can('edit', 'devices');
        can('assign', 'devices');
        can('assign', 'applications');
        can('view', 'organizations');
        can('view', 'users');
        can('view', 'partner_header');
        can('view', 'decoders');
        break;
      default:
        break;
    }
  });
