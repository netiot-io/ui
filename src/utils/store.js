import { createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import thunk from 'redux-thunk';
import createHistory from 'history/createBrowserHistory';
import { createLogger } from 'redux-logger';
import rootReducer from '../reducers/index';
import {addLocaleData } from "react-intl";
import locale_en from 'react-intl/locale-data/en';
import locale_ro from 'react-intl/locale-data/ro';
import messages_ro from "../translations/ro.json";
import messages_en from "../translations/en.json";
import { materialFields, materialRenderers } from '@jsonforms/material-renderers';

// configure localization
addLocaleData([...locale_en, ...locale_ro]); // add supported locales

// link locales to translation files
const messages = {
  'ro': messages_ro,
  'en': messages_en
};

// get browser language
const language = navigator.language.split(/[-_]/)[0];  // language without region code

// create router history
export const history = createHistory();

// add locale config to initial state
const initialState = {
  intl: {
    locale: language,
    messages: messages[language],
  },
  jsonforms: {
    renderers: materialRenderers,
    fields: materialFields,
  }
};
const enhancers = [];

const loggerMiddleware = createLogger();

// add thunk and router as middleware
const middleware = [
  thunk,
  routerMiddleware(history),
  loggerMiddleware
];

if (process.env.NODE_ENV === 'development') {
  const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__;

  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension())
  }
}

const composedEnhancers = compose(
  applyMiddleware(...middleware),
  ...enhancers
);

const store = createStore(
  rootReducer,
  initialState,
  composedEnhancers
);

export default store;