import { authHeader } from '../utils/auth-header';
import { Base64 } from 'js-base64';
import { env } from '../utils/config';

export const userService = {
  login,
  logout,
  register,
  activate,
  getUserInfo,
  getOrganizations,
  getOrganizationUsers,
  addOrganization,
  updateOrganization,
  deleteOrganization,
  getAll,
  getById,
  add,
  update,
  delete: _delete,
  setPassword,
  getPartnerInfo,
  changePassword,
  resetPassword
};

let timerId;

function login(username, password) {
  const requestOptions = {
    method: 'POST',
    headers: {
      'Authorization': 'Basic ' + Base64.encode(env.REACT_APP_SERVER_AUTH_USERNAME + ':' + env.REACT_APP_SERVER_AUTH_PASSWORD)
    }
  };
  return fetch(`${env.REACT_APP_SERVER_AUTH_BASE_PATH}?grant_type=password&password=${password}&username=${username}`, requestOptions)
    .then(handleResponse)
    .then(user => {
      // login successful if there's a jwt token in the response
      if (user.access_token) {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('user', JSON.stringify(user));
        refreshToken(user, requestOptions);
      }

      return user;
    });
}

function refreshToken(user, requestOptions) {
  // set timer to refresh token when it expires
  timerId = setTimeout(() => {
    fetch(`${env.REACT_APP_SERVER_AUTH_BASE_PATH}?grant_type=refresh_token&refresh_token=${user.refresh_token}`, requestOptions)
      .then(handleResponse)
      .then(user => {
        // login successful if there's a jwt token in the response
        let cachedUser = JSON.parse(localStorage.getItem('user'));
        cachedUser.access_token = user.access_token;
        localStorage.setItem('user', JSON.stringify(cachedUser));
        refreshToken(user, requestOptions);
      });
  }, (user.expires_in - 60) * 1000);
}

function logout() {
  // remove user from local storage to log user out
  clearInterval(timerId);
  localStorage.removeItem('user');
}

function getUserInfo() {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  };
  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_USER_BASE_PATH}${env.REACT_APP_SERVER_USER_INFO}`, requestOptions)
    .then(handleResponse)
    .then(user => {
      let cachedUser = JSON.parse(localStorage.getItem('user'));
      cachedUser.firstName = user.first_name;
      cachedUser.lastName = user.last_name;
      cachedUser.email = user.email;
      cachedUser.organizationId = user.organization_id;
      cachedUser.organizationName = user.organization_name;
      cachedUser.organizationPhone = user.organization_phone;
      cachedUser.organizationAddress = user.organization_address;
      localStorage.setItem('user', JSON.stringify(cachedUser));

      return cachedUser;
  });
}


function getOrganizations() {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  };
  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_ORGANIZATION_BASE_PATH}`, requestOptions).then(handleResponse);
}

function getOrganizationUsers(organizationId) {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  };
  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_ORGANIZATION_BASE_PATH}/${organizationId}/users`, requestOptions).then(handleResponse);
}

function addOrganization(organization) {
  const requestOptions = {
    method: 'POST',
    headers: { ...authHeader(), 'Content-Type': 'application/json' },
    body: JSON.stringify(organization)
  };

  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_ORGANIZATION_BASE_PATH}?organizationType=ENTERPRISE`, requestOptions).then(handleResponse);
}

function updateOrganization(organization) {
  const requestOptions = {
    method: 'PUT',
    headers: { ...authHeader(), 'Content-Type': 'application/json' },
    body: JSON.stringify(organization)
  };
  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_ORGANIZATION_BASE_PATH}`, requestOptions).then(handleResponse);
}

// prefixed function name with underscore because delete is a reserved word in javascript
function deleteOrganization(id) {
  const requestOptions = {
    method: 'DELETE',
    headers: authHeader()
  };

  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_ORGANIZATION_BASE_PATH}/${id}`, requestOptions).then(handleResponse);
}

function getAll() {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  };

  return fetch(`${env.REACT_APP_API_URL}/users`, requestOptions).then(handleResponse);
}

function getById(id) {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  };

  return fetch(`${env.REACT_APP_API_URL}/users/${id}`, requestOptions).then(handleResponse);
}

function register(user) {
  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(user)
  };

  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_USER_BASE_PATH}${env.REACT_APP_SERVER_USER_REGISTER}${env.REACT_APP_SERVER_SIMPLE_USER_REGISTER}`, requestOptions).then(handleResponse);
}

function activate(token, password) {
  const pass = password === null ? '' : `?password=${password}`;

  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    }
  };

  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_USER_BASE_PATH}${env.REACT_APP_SERVER_USER_ACTIVATE.replace('{token}', token)}${pass}`, requestOptions).then(handleResponse);
}

function add(user) {
  const requestOptions = {
    method: 'POST',
    headers: { ...authHeader(), 'Content-Type': 'application/json' },
    body: JSON.stringify(user)
  };

  let registerPath = '';
  switch (user.role) {
    case 'ENTERPRISE_USER':
    case 'ENTERPRISE_ADMIN':
      registerPath = `${env.REACT_APP_SERVER_ENTERPRISE_USER_REGISTER}?role_type=${getHackedUserRole(user.role)}&organization_id=${user.organization_id}`;
      break;
    default:
      break;
  }
  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_USER_BASE_PATH}${env.REACT_APP_SERVER_USER_REGISTER}${registerPath}`, requestOptions).then(handleResponse);
}

function update(user) {
  const updateUser = { ...user };
  updateUser.role = getHackedUserRole(user.role);

  const requestParameters = `?organization_id=${updateUser.organization_id}&role=${updateUser.role}`;

  const requestOptions = {
    method: 'PUT',
    headers: { ...authHeader(), 'Content-Type': 'application/json' },
    body: JSON.stringify(updateUser)
  };

  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_USER_BASE_PATH}${requestParameters}`, requestOptions).then(handleResponse);
}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
  const requestOptions = {
    method: 'DELETE',
    headers: authHeader()
  };

  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_USER_BASE_PATH}${id}`, requestOptions).then(handleResponse);
}

function setPassword(token, password) {
  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    }
  };

  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_USER_BASE_PATH}${env.REACT_APP_SERVER_USER_RESET_PASSWORD.replace('{token}', token)}?new-password=${password}`, requestOptions).then(handleResponse);
}

function getPartnerInfo() {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  };
  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_USER_BASE_PATH}${env.REACT_APP_SERVER_USER_PARENT}`, requestOptions).then(handleResponse);
}

function changePassword(passwords) {
  const requestOptions = {
    method: 'POST',
    headers: { ...authHeader(), 'Content-Type': 'application/json' },
    body: JSON.stringify(passwords)
  };

  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_USER_BASE_PATH}${env.REACT_APP_SERVER_USER_CHANGE_PASSWORD}`, requestOptions).then(handleResponse);
}

function resetPassword(username) {
  const requestOptions = {
    method: 'POST',
    headers: { ...authHeader(), 'Content-Type': 'application/json' }
  };

  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_USER_BASE_PATH}${env.REACT_APP_SERVER_USER_REQUEST_RESET_PASSWORD}?email=${username}`, requestOptions).then(handleResponse);
}

function handleResponse(response) {
  if (typeof response.headers !== 'undefined') {
    return response.text().then(text => {
      const data = text && JSON.parse(text);
      if (!response.ok) {
        if (response.status === 401) {
          // auto logout if 401 response returned from api
          logout();
          window.location.reload(true);
        }
        const error = (data && data.error) || response.statusText;
        return Promise.reject(error);
      }
      return data;
    });
  } else {
    // TODO: this is a hack because of differences between real and fake backend. Remove when removing the fake backend.
    return response.json().then(data => {
      // const data = text && JSON.parse(text);
      if (!response.ok) {
        const error = (data && data.message) || response.statusText;
        return Promise.reject(error);
      }

      return data;
    });
  }
}

/*
   TODO: solve this hack on the backend. We should have the same user roles, not extra ones. We should have only one
   register endpoint that determines the user type based on the data given
*/
function getHackedUserRole(role) {
  let hackedRole = '';
  switch (role) {
    case 'ENTERPRISE_USER':
      hackedRole = 'USER';
      break;
    case 'ENTERPRISE_ADMIN':
      hackedRole = 'ADMIN';
      break;
    default:
      break;
  }
  return hackedRole;
}
