import { authHeader } from '../utils/auth-header';
import { userService } from './index';
import { env } from '../utils/config';

export const applicationService = {
  addApplication,
  getUserApplications,
  getApplicationTypes,
  getApplicationConfig,
  updateApplication,
  deleteApplication,
  addApplicationConfig
};

function addApplication(application) {
  const requestOptions = {
    method: 'POST',
    headers: { ...authHeader(), 'Content-Type': 'application/json' },
    body: JSON.stringify(application)
  };
  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_APP_REGISTRY_BASE_PATH}${env.REACT_APP_SERVER_APP_REGISTRY_INSTANCES}`, requestOptions).then(handleResponse);
}

function addApplicationConfig(applicationId, applicationPath, applicationConfig) {
  const requestOptions = {
    method: 'POST',
    headers: { ...authHeader(), 'Content-Type': 'application/json' },
    body: JSON.stringify({data: applicationConfig})
  };
  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${applicationPath}${env.REACT_APP_SERVER_APP_CONF}${applicationId}`, requestOptions).then(handleResponse);
}

function getUserApplications() {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  };
  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_APP_REGISTRY_BASE_PATH}${env.REACT_APP_SERVER_APP_REGISTRY_INSTANCES}user`, requestOptions).then(handleResponse);
}

function getApplicationTypes() {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  };
  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_APP_REGISTRY_BASE_PATH}${env.REACT_APP_SERVER_APP_REGISTRY_TYPES}`, requestOptions).then(handleResponse);
}

function getApplicationConfig(applicationPath, applicationId) {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  };
  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${applicationPath}/configuration?applicationInstanceId=${applicationId}`,
    requestOptions).then(handleResponse);
}

function updateApplication(application) {
  const requestOptions = {
    method: 'PUT',
    headers: { ...authHeader(), 'Content-Type': 'application/json' },
    body: JSON.stringify(application)
  };
  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_APP_REGISTRY_BASE_PATH}${env.REACT_APP_SERVER_APP_REGISTRY_INSTANCES}${application.id}`,
    requestOptions).then(handleResponse);
}

function deleteApplication(applicationId) {
  const requestOptions = {
    method: 'DELETE',
    headers: { ...authHeader(), 'Content-Type': 'application/json' }
  };
  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_APP_REGISTRY_BASE_PATH}${env.REACT_APP_SERVER_APP_REGISTRY_INSTANCES}${applicationId}`, requestOptions).then(handleResponse);
}

function handleResponse(response) {
  if (typeof response.headers !== 'undefined') {
    return response.text().then(text => {
      const data = text && JSON.parse(text);
      if (!response.ok) {
        if (response.status === 401) {
          // auto logout if 401 response returned from api
          userService.logout();
          window.location.reload(true);
        }
        const error = (data && data.error) || response.statusText;
        return Promise.reject(error);
      }
      return data;
    });
  } else {
    // TODO: this is a hack because of differences between real and fake backend. Remove when removing the fake backend.
    return response.json().then(data => {
      // const data = text && JSON.parse(text);
      if (!response.ok) {
        const error = (data && data.message) || response.statusText;
        return Promise.reject(error);
      }

      return data;
    });
  }
}