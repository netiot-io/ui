import {authHeader} from "../utils/auth-header";
import { userService } from './index';
import { env } from '../utils/config';

export const deviceService = {
  getDecoders,
  getProtocols,
  addDecoder,
  updateDecoder,
  deleteDecoder,
  getAll,
  add,
  update,
  delete: _delete,
  getMessageCount,
  getLastMessage,
  getLast10Alerts,
  getDeviceTypes,
  getDeviceTypeData,
  getProfiles,
  addProfile,
  addDeviceType,
  sendPayload
};

function getDecoders(protocolId = '') {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  };
  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_DEVICE_BASE_PATH}${env.REACT_APP_SERVER_DEVICE_DECODERS}?protocolId=${protocolId}`, requestOptions).then(handleResponse);
}

function getProtocols() {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  };
  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_DEVICE_BASE_PATH}${env.REACT_APP_SERVER_DEVICE_PROTOCOLS}`, requestOptions).then(handleResponse);
}

function addDecoder(decoder) {
  const requestOptions = {
    method: 'POST',
    headers: { ...authHeader(), 'Content-Type': 'application/json' },
    body: JSON.stringify(decoder)
  };

  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_DEVICE_BASE_PATH}${env.REACT_APP_SERVER_DEVICE_DECODERS}`, requestOptions).then(handleResponse);
}

function updateDecoder(decoder) {
  const requestOptions = {
    method: 'PUT',
    headers: { ...authHeader(), 'Content-Type': 'application/json' },
    body: JSON.stringify(decoder)
  };
  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_DEVICE_BASE_PATH}${env.REACT_APP_SERVER_DEVICE_DECODERS}/${decoder.id}`, requestOptions).then(handleResponse);
}

function deleteDecoder(id) {
  const requestOptions = {
    method: 'DELETE',
    headers: authHeader()
  };

  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_DEVICE_BASE_PATH}${env.REACT_APP_SERVER_DEVICE_DECODERS}/${id}`, requestOptions).then(handleResponse);
}

function add(device) {
  const requestOptions = {
    method: 'POST',
    headers: { ...authHeader(), 'Content-Type': 'application/json' },
    body: JSON.stringify(device)
  };

  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_DEVICE_BASE_PATH}${env.REACT_APP_SERVER_DEVICE}`, requestOptions).then(handleResponse);
}

function getMessageCount(deviceId) {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  };
  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_DEVICE_BASE_PATH}${env.REACT_APP_SERVER_DEVICE}/${deviceId}${env.REACT_APP_SERVER_DEVICE_MESSAGE_COUNT}`, requestOptions).then(handleResponse);
}

function getLastMessage(deviceId) {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  };
  const millisFrom =  new Date().getTime() - 1000 * 60 * 60 * 24 * 2;
  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_DEVICE_BASE_PATH}${env.REACT_APP_SERVER_DEVICE}${env.REACT_APP_SERVER_DEVICE_GET_DATA}?device-id=${deviceId}&millis-from=${millisFrom}`, requestOptions).then(handleResponse);
}

function getLast10Alerts(deviceId) {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  };

  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_DEVICE_BASE_PATH}${env.REACT_APP_SERVER_DEVICE_ALERTS}${deviceId}${env.REACT_APP_SERVER_DEVICE_LAST_10_ALERTS}`, requestOptions).then(handleResponse);
}

function getDeviceTypes(protocolId) {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  };
  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_DEVICE_BASE_PATH}${env.REACT_APP_SERVER_DEVICE_TYPE}?protocolId=${protocolId}`, requestOptions).then(handleResponse);
}

function getDeviceTypeData(deviceTypeId) {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  };
  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_DEVICE_BASE_PATH}${env.REACT_APP_SERVER_DEVICE_TYPE}${deviceTypeId}`, requestOptions).then(handleResponse);
}

function getProfiles(deviceTypeId) {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  };
  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_DEVICE_BASE_PATH}${env.REACT_APP_SERVER_DEVICE_TYPE}${deviceTypeId}${env.REACT_APP_SERVER_DEVICE_TYPE_PROFILES}`, requestOptions).then(handleResponse);
}

function addProfile(profile, deviceTypeId) {
  const requestOptions = {
    method: 'POST',
    headers: { ...authHeader(), 'Content-Type': 'application/json' },
    body: JSON.stringify(profile)
  };
  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_DEVICE_BASE_PATH}${env.REACT_APP_SERVER_DEVICE_TYPE}${deviceTypeId}${env.REACT_APP_SERVER_DEVICE_TYPE_PROFILES}`, requestOptions).then(handleResponse);
}

function addDeviceType(deviceType) {
  const requestOptions = {
    method: 'POST',
    headers: { ...authHeader(), 'Content-Type': 'application/json' },
    body: JSON.stringify(deviceType)
  };

  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_DEVICE_BASE_PATH}${env.REACT_APP_SERVER_DEVICE_TYPE}`, requestOptions).then(handleResponse);
}

function sendPayload(deviceId, payload) {
  const requestOptions = {
    method: 'POST',
    headers: { ...authHeader(), 'Content-Type': 'application/json' },
    body: JSON.stringify(payload)
  };

  return fetch(`${env.REACT_APP_API_URL}${env.REACT_APP_SERVER_DEVICE_BASE_PATH}${deviceId}/${env.REACT_APP_SERVER_DEVICE_PAYLOAD}`, requestOptions).then(handleResponse);
  // return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_DEVICE_BASE_PATH}${deviceId}/${env.REACT_APP_SERVER_DEVICE_PAYLOAD}`, requestOptions).then(handleResponse);
}

function getAll() {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  };

  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_DEVICE_BASE_PATH}${env.REACT_APP_SERVER_DEVICE}`, requestOptions).then(handleResponse);
}

function update(device) {
  const requestOptions = {
    method: 'PUT',
    headers: { ...authHeader(), 'Content-Type': 'application/json' },
    body: JSON.stringify(device)
  };

  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_DEVICE_BASE_PATH}${env.REACT_APP_SERVER_DEVICE}${device.id}`, requestOptions).then(handleResponse);
}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(deviceId) {
  const requestOptions = {
    method: 'DELETE',
    headers: authHeader()
  };

  return fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_DEVICE_BASE_PATH}${env.REACT_APP_SERVER_DEVICE}${deviceId}`, requestOptions).then(handleResponse);
}

function handleResponse(response) {
  if (typeof response.headers !== 'undefined') {
    return response.text().then(text => {
      const data = text && JSON.parse(text);
      if (!response.ok) {
        if (response.status === 401) {
          // auto logout if 401 response returned from api
          userService.logout();
          window.location.reload(true);
        }
        const error = (data && data.error) || response.statusText;
        return Promise.reject(error);
      }
      return data;
    });
  } else {
    // TODO: this is a hack because of differences between real and fake backend. Remove when removing the fake backend.
    return response.json().then(data => {
      // const data = text && JSON.parse(text);
      if (!response.ok) {
        const error = (data && data.message) || response.statusText;
        return Promise.reject(error);
      }

      return data;
    });
  }
}
