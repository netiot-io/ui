export const applicationConstants = {
  ADD_APPLICATION_SUCCESS: 'APPLICATIONS_ADD_APPLICATION_SUCCESS',
  ADD_APPLICATION_FAILURE: 'APPLICATIONS_ADD_APPLICATION_FAILURE',
  ADD_APPLICATION_REQUEST: 'APPLICATIONS_ADD_APPLICATION_REQUEST',

  GET_USER_APPLICATIONS_REQUEST: 'APPLICATIONS_GET_USER_APPLICATIONS_REQUEST',
  GET_USER_APPLICATIONS_SUCCESS: 'APPLICATIONS_GET_USER_APPLICATIONS_SUCCESS',
  GET_USER_APPLICATIONS_FAILURE: 'APPLICATIONS_GET_USER_APPLICATIONS_FAILURE',

  GET_APPLICATION_TYPES_REQUEST: 'APPLICATIONS_GET_APPLICATION_TYPES_REQUEST',
  GET_APPLICATION_TYPES_SUCCESS: 'APPLICATIONS_GET_APPLICATION_TYPES_SUCCESS',
  GET_APPLICATION_TYPES_FAILURE: 'APPLICATIONS_GET_APPLICATION_TYPES_FAILURE',

  GET_APPLICATION_CONFIG_REQUEST: 'APPLICATIONS_GET_APPLICATION_CONFIG_REQUEST',
  GET_APPLICATION_CONFIG_SUCCESS: 'APPLICATIONS_GET_APPLICATION_CONFIG_SUCCESS',
  GET_APPLICATION_CONFIG_FAILURE: 'APPLICATIONS_GET_APPLICATION_CONFIG_FAILURE',

  UPDATE_APPLICATIONS_SUCCESS: 'APPLICATIONS_UPDATE_APPLICATIONS_SUCCESS',
  UPDATE_APPLICATIONS_FAILURE: 'APPLICATIONS_UPDATE_APPLICATIONS_FAILURE',
  UPDATE_APPLICATIONS_REQUEST: 'APPLICATIONS_UPDATE_APPLICATIONS_REQUEST',

  DELETE_APPLICATION_SUCCESS: 'APPLICATIONS_DELETE_APPLICATION_SUCCESS',
  DELETE_APPLICATION_FAILURE: 'APPLICATIONS_DELETE_APPLICATION_FAILURE',
  DELETE_APPLICATION_REQUEST: 'APPLICATIONS_DELETE_APPLICATION_REQUEST',

  DELETE_STORE_APPLICATION_CONFIG_SUCCESS: 'APPLICATIONS_DELETE_STORE_APPLICATION_CONFIG_SUCCESS'
};