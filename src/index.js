import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { Provider } from 'react-intl-redux';
import store from './utils/store';
import { App } from './containers/App';

// setup fake backend
import { configureFakeBackend } from './utils/fake-backend';
configureFakeBackend();

ReactDOM.render(
  // add the redux store, but from intl-redux so that it includes the intl
  <Provider store={store}>
    <App/>
  </Provider>,
  document.getElementById('root'));
