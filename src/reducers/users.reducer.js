import {userConstants} from '../constants';

export function users(state = {}, action) {
  switch (action.type) {
    case userConstants.GETALL_REQUEST:
      return {
        ...state,
        loading: true
      };
    case userConstants.GETALL_SUCCESS:
      return {
        ...state,
        items: action.users
      };
    case userConstants.GETALL_FAILURE:
      return {
        error: action.error
      };
    case userConstants.GET_ORGANIZATIONS_REQUEST:
      return {
        loading: true
      };
    case userConstants.GET_ORGANIZATIONS_SUCCESS:
      return {
        ...state,
        organizations: action.organizations
      };
    case userConstants.GET_ORGANIZATIONS_FAILURE:
      return {
        error: action.error
      };
    case userConstants.GET_ORGANIZATION_USERS_REQUEST:
      return {
        ...state,
        loading: true
      };
    case userConstants.GET_ORGANIZATION_USERS_SUCCESS:
      let users = (typeof state.items === 'undefined') ? [] : state.items;
      for (const user of action.users) {
        if (users.findIndex(u => u.id === user.id) === -1) {
          users.push(user);
        }
      }
      return {
        ...state,
        items: users
      };
    case userConstants.GET_ORGANIZATION_USERS_FAILURE:
      return {
        ...state,
        error: action.error
      };
    case userConstants.GET_APPLICATIONS_REQUEST:
      return {
        ...state,
        loading: true
      };
    case userConstants.GET_APPLICATIONS_SUCCESS:
      return {
        ...state,
        applications: action.applications
      };
    case userConstants.GET_APPLICATIONS_FAILURE:
      return {
        error: action.error
      };
    case userConstants.GET_APPLICATION_TYPES_REQUEST:
      return {
        ...state,
        loading: true
      };
    case userConstants.GET_APPLICATION_TYPES_SUCCESS:
      return {
        ...state,
        applicationTypes: action.applicationTypes
      };
    case userConstants.GET_APPLICATION_TYPES_FAILURE:
      return {
        error: action.error
      };
    case userConstants.GET_APPLICATION_CONFIG_REQUEST:
      return {
        ...state,
        loading: true
      };
    case userConstants.GET_APPLICATION_CONFIG_SUCCESS:
      return {
        ...state,
        applicationConfig: action.applicationConfig
      };
    case userConstants.GET_APPLICATION_CONFIG_FAILURE:
      return {
        error: action.error
      };
    case userConstants.UPDATE_APPLICATIONS_REQUEST:
      // add 'updating:true' property to sensor being deleted
      return {
        ...state,
        applications: state.applications.map(application =>
          application.id === action.id
            ? { ...application, updating: true }
            : application
        )
      };
    case userConstants.UPDATE_APPLICATIONS_SUCCESS:
      return {
        ...state,
        applications: state.applications.map(application =>
          application.id === action.application.id
            ? action.application
            : application
        )
      };
    case userConstants.UPDATE_APPLICATIONS_FAILURE:
      // remove 'updating:true' property and add 'updateError:[error]' property to sensor
      return {
        ...state,
        applications: state.applications.map(application => {
          if (application.id === action.id) {
            // make copy of sensor without 'updating:true' property
            // const { updating, ...sensorCopy } = sensor;
            const { ...applicationCopy } = application;
            // return copy of sensor with 'updateError:[error]' property
            return { ...applicationCopy, updateError: action.error };
          }

          return application;
        })
      };
    case userConstants.DELETE_APPLICATION_REQUEST:
      state.applications = state.applications.map(application => action.applicationId === application.id ? { ...application, deleting: true } : application);
      return {
        ...state
      };
    case userConstants.DELETE_APPLICATION_SUCCESS:
      // remove deleted user from state
      state.applications = state.applications.filter(application => application.id !== action.id);
      return {
        ...state
      };
    case userConstants.DELETE_APPLICATION_FAILURE: {
      // remove 'deleting:true' property and add 'deleteError:[error]' property to user
      const deletedApplication = state.applications.find(application => application.id === action.id);
      const {deleting, ...applicationCopy} = deletedApplication;
      state.applications = state.applications.map(application => action.applicationId === application.id ? {
        ...applicationCopy,
        deleteError: action.error
      } : application);
      return {
        ...state
      };
    }
    case userConstants.ADD_ORGANIZATIONS_SUCCESS:
      state.organizations = [...state.organizations, action.organization];
      return {
        ...state
      };
    case userConstants.UPDATE_ORGANIZATIONS_SUCCESS:
      if (typeof state.organizations !== 'undefined') {
        state.organizations = state.organizations.map(organization => action.organization.id === organization.id ? action.organization : organization);
      }
      return {
        ...state
      };
    case userConstants.DELETE_ORGANIZATIONS_REQUEST:
      state.organizations = state.organizations.map(organization => action.organizationId === organization.id ? { ...organization, deleting: true } : organization);
      return {
        ...state
      };
    case userConstants.DELETE_ORGANIZATIONS_SUCCESS:
      // remove deleted user from state
      state.organizations = state.organizations.filter(organization => organization.id !== action.id);
      return {
        ...state
      };
    case userConstants.DELETE_ORGANIZATIONS_FAILURE:
      // remove 'deleting:true' property and add 'deleteError:[error]' property to user
      const deletedOrganization = state.organizations.find(organization => organization.id === action.id);
      const { deleting, ...organizationCopy } = deletedOrganization;
      state.organizations = state.organizations.map(organization => action.organizationId === organization.id ? { ...organizationCopy, deleteError: action.error } : organization);
      return {
        ...state
      };
    case userConstants.ADD_SUCCESS:
      state.items = [...state.items, action.user];
      return {
        ...state
      };
    case userConstants.UPDATE_SUCCESS:
      state.items = state.items.map(user => action.user.id === user.id ? action.user : user);
      return {
        ...state
      };
    case userConstants.DELETE_REQUEST:
      // add 'deleting:true' property to user being deleted
      return {
        ...state,
        items: state.items.map(user =>
          user.id === action.id
            ? { ...user, deleting: true }
            : user
        )
      };
    case userConstants.DELETE_SUCCESS:
      // remove deleted user from state
      return {
        ...state,
        items: state.items.filter(user => user.id !== action.id)
      };
    case userConstants.DELETE_FAILURE:
      // remove 'deleting:true' property and add 'deleteError:[error]' property to user
      return {
        ...state,
        items: state.items.map(user => {
          if (user.id === action.id) {
            // make copy of user without 'deleting:true' property
            const { deleting, ...userCopy } = user;
            // return copy of user with 'deleteError:[error]' property
            return { ...userCopy, deleteError: action.error };
          }

          return user;
        })
      };
    case userConstants.GET_PARTNER_INFO_REQUEST:
      return {
        loading: true
      };
    case userConstants.GET_PARTNER_INFO_SUCCESS:
      return {
        ...state,
        partner: action.partner
      };
    case userConstants.GET_PARTNER_INFO_FAILURE:
      return {
        error: action.error
      };
    case userConstants.CHANGE_PASSWORD_FAILURE:
      return {
        error: action.error
      };
    case userConstants.RESET_PASSWORD_FAILURE:
      return {
        error: action.error
      };
    default:
      return state
  }
}