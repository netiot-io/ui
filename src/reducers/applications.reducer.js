import {applicationConstants} from '../constants';

export function applications(state = {}, action) {
  switch (action.type) {
    case applicationConstants.ADD_APPLICATION_SUCCESS:
      return {
        ...state,
        applications: [...state.applications, action.application]
      };
    case applicationConstants.GET_USER_APPLICATIONS_REQUEST:
      return {
        ...state,
        loading: true
      };
    case applicationConstants.GET_USER_APPLICATIONS_SUCCESS:
      return {
        ...state,
        applications: action.applications
      };
    case applicationConstants.GET_USER_APPLICATIONS_FAILURE:
      return {
        error: action.error
      };
    case applicationConstants.GET_APPLICATION_TYPES_REQUEST:
      return {
        ...state,
        loading: true
      };
    case applicationConstants.GET_APPLICATION_TYPES_SUCCESS:
      return {
        ...state,
        applicationTypes: action.applicationTypes
      };
    case applicationConstants.GET_APPLICATION_TYPES_FAILURE:
      return {
        error: action.error
      };
    case applicationConstants.GET_APPLICATION_CONFIG_REQUEST:
      return {
        ...state,
        loading: true
      };
    case applicationConstants.GET_APPLICATION_CONFIG_SUCCESS:
      return {
        ...state,
        applicationConfig: action.applicationConfig
      };
    case applicationConstants.GET_APPLICATION_CONFIG_FAILURE:
      return {
        error: action.error
      };
    case applicationConstants.UPDATE_APPLICATIONS_REQUEST:
      // add 'updating:true' property to sensor being deleted
      return {
        ...state,
        applications: state.applications.map(application =>
          application.id === action.id
            ? { ...application, updating: true }
            : application
        )
      };
    case applicationConstants.UPDATE_APPLICATIONS_SUCCESS:
      return {
        ...state,
        applications: state.applications.map(application =>
          application.id === action.application.id
            ? action.application
            : application
        )
      };
    case applicationConstants.UPDATE_APPLICATIONS_FAILURE:
      // remove 'updating:true' property and add 'updateError:[error]' property to sensor
      return {
        ...state,
        applications: state.applications.map(application => {
          if (application.id === action.id) {
            // make copy of sensor without 'updating:true' property
            // const { updating, ...sensorCopy } = sensor;
            const { ...applicationCopy } = application;
            // return copy of sensor with 'updateError:[error]' property
            return { ...applicationCopy, updateError: action.error };
          }

          return application;
        })
      };
    case applicationConstants.DELETE_APPLICATION_REQUEST:
      state.applications = state.applications.map(application => action.applicationId === application.id ? { ...application, deleting: true } : application);
      return {
        ...state
      };
    case applicationConstants.DELETE_APPLICATION_SUCCESS:
      // remove deleted user from state
      state.applications = state.applications.filter(application => application.id !== action.id);
      return {
        ...state
      };
    case applicationConstants.DELETE_APPLICATION_FAILURE: {
      // remove 'deleting:true' property and add 'deleteError:[error]' property to user
      const deletedApplication = state.applications.find(application => application.id === action.id);
      const {deleting, ...applicationCopy} = deletedApplication;
      state.applications = state.applications.map(application => action.applicationId === application.id ? {
        ...applicationCopy,
        deleteError: action.error
      } : application);
      return {
        ...state
      };
    }
    case applicationConstants.DELETE_STORE_APPLICATION_CONFIG_SUCCESS: {
      const {applicationConfig, ...newState} = state;
      return newState;
    }
    default:
      return state
  }
}