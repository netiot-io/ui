import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { intlReducer } from 'react-intl-redux'
import { authentication } from './authentication.reducer';
import { registration } from './registration.reducer';
import { activation } from './activation.reducer';
import { users } from './users.reducer';
import { devices } from './devices.reducer';
import { alerts } from './alert.reducer';
import { applications } from './applications.reducer';
import { filterReducer } from 'material-ui-filter';
import { jsonformsReducer } from '@jsonforms/core';

// combine reducers for the redux store
export default combineReducers({
  routing: routerReducer,
  intl: intlReducer,
  authentication,
  registration,
  activation,
  users,
  devices,
  alerts,
  applications,
  filters: filterReducer,
  jsonforms: jsonformsReducer()
});