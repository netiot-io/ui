import { alertConstants } from '../constants';

export function alerts(state = [], action) {
  switch (action.type) {
    case alertConstants.SUCCESS:
      return [...state,
        {type: 'success', message: action.message, values: action.values}
      ];
    case alertConstants.ERROR:
      return [...state,
        {type: 'error', message: action.message, values: action.values}
      ];
    case alertConstants.CLEAR:
      return [];
    default:
      return state
  }
}