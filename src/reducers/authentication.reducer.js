import { userConstants } from '../constants';
import defineAbilitiesFor from '../utils/ability';

let user = JSON.parse(localStorage.getItem('user'));
const initialState = user ? { loggedIn: true, user, abilities: defineAbilitiesFor(user.role) } : {};

export function authentication(state = initialState, action) {
  switch (action.type) {
    case userConstants.LOGIN_REQUEST:
      return {
        loggingIn: true,
        user: action.user
      };
    case userConstants.LOGIN_SUCCESS:
      return {
        loggedIn: true,
        user: action.user,
        abilities: defineAbilitiesFor(action.user.role)
      };
    case userConstants.LOGIN_FAILURE:
      return {};
    case userConstants.LOGOUT:
      return {};
    case userConstants.GET_USER_INFO_REQUEST:
      return {
        ...state,
        loading: true
      };
    case userConstants.GET_USER_INFO_SUCCESS:
      return {
        ...state,
        user: {...user, ...action.user}
      };
    case userConstants.GET_USER_INFO_FAILURE:
      return {
        error: action.error
      };
    default:
      return state
  }
}