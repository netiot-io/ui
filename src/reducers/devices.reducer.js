import {deviceConstants} from '../constants';

export function devices(state = {}, action) {
  switch (action.type) {
    case deviceConstants.GET_DECODERS_REQUEST:
      return {
        loading: true,
        ...state
      };
    case deviceConstants.GET_DECODERS_SUCCESS:
      return {
        ...state,
        decoders: action.decoders
      };
    case deviceConstants.GET_DECODERS_FAILURE:
      return {
        error: action.error
      };
    case deviceConstants.GET_PROTOCOLS_REQUEST:
      return {
        loading: true,
        ...state
      };
    case deviceConstants.GET_PROTOCOLS_SUCCESS:
      return {
        ...state,
        protocols: action.protocols
      };
    case deviceConstants.GET_PROTOCOLS_FAILURE:
      return {
        error: action.error
      };
    case deviceConstants.ADD_DECODER_SUCCESS:
      return {
        ...state,
        decoders: [...state.decoders, action.decoder]
      };
    case deviceConstants.UPDATE_DECODER_SUCCESS:
      state.decoders = state.decoders.map(decoder => action.decoder.id === decoder.id ? action.decoder : decoder);
      return {
        ...state
      };
    case deviceConstants.DELETE_DECODER_REQUEST:
      state.decoders = state.decoders.map(decoder => action.decoderId === decoder.id ? { ...decoder, deleting: true } : decoder);
      return {
        ...state
      };
    case deviceConstants.DELETE_DECODER_SUCCESS:
      state.decoders = state.decoders.filter(decoder => decoder.id !== action.id);
      return {
        ...state
      };
    case deviceConstants.DELETE_DECODER_FAILURE:
      const deletedDecoder = state.decoders.find(decoder => decoder.id === action.id);
      const { deleting, ...decoderCopy } = deletedDecoder;
      state.decoders = state.decoders.map(decoder => action.decoderId === decoder.id ? { ...decoderCopy, deleteError: action.error } : decoder);
      return {
        ...state
      };
    case deviceConstants.GETALL_REQUEST:
      return {
        loading: true
      };
    case deviceConstants.GETALL_SUCCESS:
      return {
        ...state,
        items: action.devices
      };
    case deviceConstants.GETALL_FAILURE:
      return {
        error: action.error
      };
    case deviceConstants.ADD_SUCCESS:
      return {
        ...state,
        items: [...state.items, action.device]
      };
    case deviceConstants.DELETE_REQUEST:
      // add 'deleting:true' property to device being deleted
      return {
        ...state,
        items: state.items.map(device =>
          device.id === action.id
            ? { ...device, deleting: true }
            : device
        )
      };
    case deviceConstants.DELETE_SUCCESS:
      // remove deleted device from state
      return {
        ...state,
        items: state.items.filter(device => device.id !== action.id)
      };
    case deviceConstants.DELETE_FAILURE:
      // remove 'deleting:true' property and add 'deleteError:[error]' property to device
      return {
        ...state,
        items: state.items.map(device => {
          if (device.id === action.id) {
            // make copy of device without 'deleting:true' property
            // const { deleting, ...deviceCopy } = device;
            const { ...deviceCopy } = device;
            // return copy of device with 'deleteError:[error]' property
            return { ...deviceCopy, deleteError: action.error };
          }

          return device;
        })
      };
    case deviceConstants.UPDATE_REQUEST:
      // add 'updating:true' property to device being deleted
      return {
        ...state,
        items: state.items.map(device =>
          device.id === action.id
            ? { ...device, updating: true }
            : device
        )
      };
    case deviceConstants.UPDATE_SUCCESS:
      return {
        ...state,
        items: state.items.map(device =>
          device.id === action.device.id
            ? action.device
            : device
        )
      };
    case deviceConstants.UPDATE_FAILURE:
      // remove 'updating:true' property and add 'updateError:[error]' property to device
      return {
        ...state,
        items: state.items.map(device => {
          if (device.id === action.id) {
            // make copy of device without 'updating:true' property
            // const { updating, ...deviceCopy } = device;
            const { ...deviceCopy } = device;
            // return copy of device with 'updateError:[error]' property
            return { ...deviceCopy, updateError: action.error };
          }

          return device;
        })
      };
    case deviceConstants.GET_MESSAGE_COUNT_SUCCESS:
      return {
        ...state,
        items: state.items.map(device =>
          device.id === action.deviceId
            ? {...device, messageCount: action.messageCount}
            : device
        )
      };
    case deviceConstants.GET_LAST_MESSAGE_SUCCESS:
      if (action.lastMessages.length > 0) {
        action.lastMessages.sort((a, b) => a.timestamp > b.timestamp ? 1 : (b.timestamp > a.timestamp ? -1 : 0));
      }
      return {
        ...state,
        items: state.items.map(device =>
          device.id === action.deviceId
            ? { ...device, lastMessage: action.lastMessages.length > 0 ? action.lastMessages[action.lastMessages.length - 1] : undefined}
            : device
        )
      };
    case deviceConstants.GET_LAST_ALERT_SUCCESS:
      return {
        ...state,
        items: state.items.map(device =>
          device.id === action.deviceId
            ? {...device, alerts: action.last10Alerts}
            : device
        )
      };
    case deviceConstants.GET_DEVICE_TYPES_SUCCESS:
      if (typeof state.deviceTypes === 'undefined') {
        state.deviceTypes = [];
      }
      return {
        ...state,
        deviceTypes: [...state.deviceTypes, ...action.deviceTypes.filter(deviceType => !state.deviceTypes.find(stateDeviceType => stateDeviceType.id === deviceType.id))
          .map(deviceType => deviceType)]
      };
    case deviceConstants.GET_DEVICE_TYPE_DATA_SUCCESS:
      if (typeof state.deviceTypes === 'undefined') {
        state.deviceTypes = [];
      }
      if (state.deviceTypes.filter(deviceType => deviceType.id === parseInt(action.deviceTypeId, 10)).length > 0) {
        return {
          ...state,
          deviceTypes: state.deviceTypes.map(deviceType =>
            deviceType.id === parseInt(action.deviceTypeId, 10)
              ? {...deviceType, ...action.deviceTypeData}
              : deviceType
          )
        };
      } else {
        return {
          ...state,
          deviceTypes: [...state.deviceTypes, {id: action.deviceTypeId, ...action.deviceTypeData}]
        }
      }

    case deviceConstants.GET_PROFILES_SUCCESS:
      return {
        ...state,
        deviceTypes: state.deviceTypes.map(deviceType => {
           return deviceType.id === parseInt(action.deviceTypeId, 10) ?
              {...deviceType, profiles: action.profiles}
              : deviceType;
          }
        )
      };
    default:
      return state
  }
}