import React, {Fragment} from 'react';
import decodersPageStyle from "./DecodersPageStyle.jsx";
import { withStyles } from '@material-ui/core/styles';
import GridContainer from "../../components/Grid/GridContainer.jsx";
import GridItem from "../../components/Grid/GridItem.jsx";
import classNames from 'classnames';
import { Header } from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import logo from '../../imgs/logo.svg';
import InfoArea from '../../components/InfoArea/InfoArea.jsx';
import { connect } from 'react-redux';
import { DecodersTable } from '../../components/Table/DecoderTable/DecodersTable';
import {alertActions, deviceActions} from "../../actions";
import TableAlerts from '../../components/Alerts/TableAlerts';

class DecodersPage extends React.Component {
  constructor(props) {
    super(props);
    props.dispatch(deviceActions.getDecoders());
    if (typeof props.devices.protocols === 'undefined') {
      props.dispatch(deviceActions.getProtocols());
    }
  }

  handleDelete = (decodersIds) => {
    for (const decoderId of decodersIds) {
      this.props.dispatch(deviceActions.deleteDecoder(decoderId));
    }
  };

  handleAdd = (decoder) => {
    this.props.dispatch(deviceActions.addDecoder(decoder));
  };

  handleEdit = (decoder) => {
    this.props.dispatch(deviceActions.updateDecoder(decoder));
  };

  render() {
    const { classes, devices, alerts } = this.props;
    return (
      <Fragment>
        <Header rightLinks={<Fragment />} brand={<Fragment><img src={logo} className={classes.logo} alt={'NETIOT Logo'}/><span className={classes.brand}>NETIOT</span></Fragment>} color={'dark'} />
        <InfoArea
          title={'Manage decoders'}
          description='This is the page where you can manage (add, edit and delete) your decoders.'
        />

        <div className={classNames("jumbotron", classes.jumbotron)}>
          <div className={classNames("container", classes.container)}>
            <div className="col-xs-12">
              {<TableAlerts alerts={alerts} clearAlerts={() => this.props.dispatch(alertActions.clear())}/>}
              <GridContainer className={classes.root}>
                <GridItem xs={12}>
                  {devices.decoders && devices.protocols && <DecodersTable data={devices.decoders.filter(decoder => decoder.defined_by_platform === true)}
                                                                          type="public" protocols={devices.protocols}
                                                                          handleAdd={this.handleAdd} handleDelete={this.handleDelete} handleEdit={this.handleEdit}/>}
                </GridItem>

                <GridItem xs={12}>
                  {devices.decoders && devices.protocols && <DecodersTable data={devices.decoders.filter(decoder => decoder.defined_by_platform !== true)}
                                                                          type="private" protocols={devices.protocols}
                                                                          handleAdd={this.handleAdd} handleDelete={this.handleDelete} handleEdit={this.handleEdit}/>}
                </GridItem>
              </GridContainer>
            </div>
          </div>
        </div>

        <Footer />
      </Fragment>
    );
  }
}

function mapStateToProps(state) {
  const { devices, alerts } = state;
  return {
    devices,
    alerts
  };
}

const connectedDecodersPage = connect(mapStateToProps)(DecodersPage);
const styledDecodersPage = withStyles(decodersPageStyle)(connectedDecodersPage);
export { styledDecodersPage as DecodersPage };