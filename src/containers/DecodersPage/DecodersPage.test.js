import React from 'react';
import ReactDOM from 'react-dom';
import DecodersPage from './';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<DecodersPage />, div);
  ReactDOM.unmountComponentAtNode(div);
});
