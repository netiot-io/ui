import React, {Fragment} from 'react';
import applicationsPageStyle from "./ApplicationsPageStyle.jsx";
import { withStyles } from '@material-ui/core/styles';
import GridContainer from "../../components/Grid/GridContainer.jsx";
import GridItem from "../../components/Grid/GridItem.jsx";
import classNames from 'classnames';
import { Header } from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import logo from '../../imgs/logo.svg';
import InfoArea from '../../components/InfoArea/InfoArea.jsx';
import { connect } from 'react-redux';
import { ApplicationsTable } from '../../components/Table/PartnerApplications/ApplicationsTable';
import { EnterpriseUserApplicationsTable } from '../../components/Table/EnterpriseUserApplications/EnterpriseUserApplicationsTable';
import {alertActions, applicationActions, userActions} from "../../actions";
import TableAlerts from '../../components/Alerts/TableAlerts';
import {getData} from "@jsonforms/core/lib/index";

class ApplicationsPage extends React.Component {
  constructor(props) {
    super(props);
    if (props.abilities.can('view', 'organizations')) {
      if (typeof props.users.organizations === 'undefined') {
        props.dispatch(userActions.getOrganizations(true));
      }
    } else {
      props.dispatch(userActions.getOrganizationUsers(props.user.organizationId));
    }
    props.dispatch(applicationActions.getUserApplications());
  }

  handleDelete = (applicationIds) => {
    for (const applicationId of applicationIds) {
      this.props.dispatch(applicationActions.deleteApplication(applicationId));
    }
  };

  handleAdd = (application) => {
    application.data = this.props.applicationConfigData;
    this.props.dispatch(applicationActions.addApplication(application));
  };

  handleEdit = (application) => {
    application.data = this.props.applicationConfigData;
    this.props.dispatch(applicationActions.updateApplication(application));
  };

  render() {
    const { classes, applications, alerts, abilities, users } = this.props;
    return (
      <Fragment>
        <Header rightLinks={<Fragment />} brand={<Fragment><img src={logo} className={classes.logo} alt={'NETIOT Logo'}/><span className={classes.brand}>NETIOT</span></Fragment>} color={'dark'} />
        <InfoArea
          title={'Manage applications'}
          description='This is the page where you can manage (view and assign) your applications.'
        />
        <div className={classNames("jumbotron", classes.jumbotron)}>
          <div className={classNames("container", classes.container)}>
            <div className="col-xs-12">
              {<TableAlerts alerts={alerts} clearAlerts={() => this.props.dispatch(alertActions.clear())}/>}
              {typeof applications.applications !== 'undefined' &&
              <GridContainer className={classes.root}>
                <GridItem xs={12}>
                  {abilities.can('view', 'enterprise_user_header') &&
                  <EnterpriseUserApplicationsTable data={applications.applications}/>}
                  {abilities.can('view', 'partner_header') && users.organizations &&
                  <ApplicationsTable handleEdit={this.handleEdit} handleAdd={this.handleAdd}
                                     handleDelete={this.handleDelete}/>}
                  {abilities.can('view', 'enterprise_admin_header') &&
                  <ApplicationsTable data={applications} handleEdit={this.handleEdit}/>}
                </GridItem>
              </GridContainer>
              }
            </div>
          </div>
        </div>
        <Footer />
      </Fragment>
    );
  }
}

function mapStateToProps(state) {
  const { applications, alerts, authentication, users } = state;
  const { user, abilities } = authentication;
  return {
    applications,
    alerts,
    abilities,
    applicationConfigData: getData(state),
    users,
    user
  };
}

const connectedApplicationsPage = connect(mapStateToProps)(ApplicationsPage);
const styledApplicationsPage = withStyles(applicationsPageStyle)(connectedApplicationsPage);
export { styledApplicationsPage as ApplicationsPage };