const ApplicationsPageStyle = {
  root: {
    width: '100%',
    overflowX: 'inherit',
  },
  table: {
    minWidth: 700,
  },
  row: {
    '&:nth-of-type(odd)': {
      backgroundColor: 'white',
    },
  },
  paper: {
    padding: 20,
    textAlign: 'center',
    color: 'black'
  },
  panel: {
    width: '50%'
  },
  logo: {
    width: '50px'
  },
  brand: {
    fontFamily: 'Chelsea Market',
    fontSize: '50px',
    paddingLeft: '10px',
    color: '#4ebd9e'
  },
  jumbotron: {
    marginBottom: '0'
  },
  "@media (min-width: 1200px)": {
    container: {
      width: '1900px'
    }
  }
};

export default ApplicationsPageStyle;