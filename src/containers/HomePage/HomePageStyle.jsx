const homePageStyle = {
  root: {
    flexGrow: 1,
    paddingTop: 2 * 2,
    paddingBottom: 2 * 2,
  },
  paper: {
    padding: 20,
    textAlign: 'center',
    color: 'black'
  },
  panel: {
    width: '50%'
  },
  logo: {
    width: '50px'
  },
  brand: {
    fontFamily: 'Chelsea Market',
    fontSize: '50px',
    paddingLeft: '10px',
    color: '#4ebd9e'
  },
  jumbotron: {
    marginBottom: '0'
  }
};

export default homePageStyle;