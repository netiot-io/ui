import React, {Fragment} from 'react';
import homePageStyle from "./HomePageStyle.jsx";
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import GridContainer from "../../components/Grid/GridContainer.jsx";
import GridItem from "../../components/Grid/GridItem.jsx";
import devicesLogo from '../../imgs/devices.svg';
import appsLogo from '../../imgs/apps.svg';
import organizationsLogo from '../../imgs/organizations.svg';
import usersLogo from '../../imgs/users.svg';
import decodersLogo from '../../imgs/decoder.svg';
import classNames from "classnames";
import { Header } from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import logo from '../../imgs/logo.svg';
import InfoArea from "../../components/InfoArea/InfoArea.jsx";
import {FormattedMessage} from 'react-intl';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import { env } from '../../utils/config';
import {BacnetAdminPage} from "../BacnetAdminPage";

class HomePage extends React.Component {
  render() {
    const { classes, user, abilities } = this.props;
    return (
      <Fragment>
        <Header rightLinks={<Fragment />}
                brand={<Fragment><img src={logo} className={classes.logo} alt={'NETIOT Logo'}/><span
                  className={classes.brand}>NETIOT</span></Fragment>} color={'dark'}/>
        <InfoArea
          title={`Hi, ${user.firstName}!`}
          description={abilities.can('view', 'apps') ? "This is your main page. Here you can add sensors, activate apps and view dashboards." :
            abilities.can('view', 'organizations') ? "This is your main page. Here you can view and edit organizations and their users." : ""
          }
        />
        <div className={classNames("jumbotron", classes.jumbotron)}>
          <div className="container">
            <div className="col-sm-12">
              {alert.message &&
              <div className={`alert ${alert.type}`}>
                <FormattedMessage id={alert.message.split(':')[0]}
                                  defaultMessage={alert.message.split(':')[0]}
                                  description="Backend error message message"
                                  values={{ value: alert.message.split(':').length > 1 ? alert.message.split(':')[1] : '' }} />
              </div>
              }
              <GridContainer className={classes.root} spacing={40}>
                  {abilities.can('send', 'bacnet') &&
                  <GridItem xs={12}>
                      <Paper className={classes.paper}>
                          <BacnetAdminPage />
                      </Paper>
                  </GridItem>
                  }
                {abilities.can('view', 'devices') &&
                <GridItem xs={6}>
                  <Paper className={classes.paper}>
                    <Link to={`${env.REACT_APP_DOMAIN}devices`} style={{textDecoration: 'none'}}>
                      <h1>DEVICES</h1>
                      <img src={devicesLogo} className={classes.panel} alt={'Devices Logo'}/>
                    </Link>
                  </Paper>
                </GridItem>
                }
                {abilities.can('view', 'apps') &&
                <GridItem xs={6}>
                  <Paper className={classes.paper}>
                    <Link to={`${env.REACT_APP_DOMAIN}applications`} style={{textDecoration: 'none'}}>
                      <h1>APPLICATIONS</h1>
                      <img src={appsLogo} className={classes.panel} alt={'Apps Logo'}/>
                    </Link>
                  </Paper>
                </GridItem>
                }
                {abilities.can('view', 'organizations') &&
                <GridItem xs={6}>
                  <Paper className={classes.paper}>
                    <Link to={`${env.REACT_APP_DOMAIN}organizations`} style={{textDecoration: 'none'}}>
                      <h1>ORGANIZATION</h1>
                      <img src={organizationsLogo} className={classes.panel} alt={'Organizations Logo'}/>
                    </Link>
                  </Paper>
                </GridItem>
                }
                {abilities.can('view', 'users') &&
                <GridItem xs={6}>
                  <Paper className={classes.paper}>
                    <Link to={`${env.REACT_APP_DOMAIN}users`} style={{textDecoration: 'none'}}>
                      <h1>USERS</h1>
                      <img src={usersLogo} className={classes.panel} alt={'Users Logo'}/>
                    </Link>
                  </Paper>
                </GridItem>
                }
                {abilities.can('view', 'decoders') &&
                <GridItem xs={6}>
                  <Paper className={classes.paper}>
                    <Link to={`${env.REACT_APP_DOMAIN}decoders`} style={{textDecoration: 'none'}}>
                      <h1>DECODERS</h1>
                      <img src={decodersLogo} className={classes.panel} alt={'Decoders Logo'}/>
                    </Link>
                  </Paper>
                </GridItem>
                }
              </GridContainer>
            </div>
          </div>
        </div>
        <Footer />
      </Fragment>
    );
  }
}

function mapStateToProps(state) {
  const {  authentication } = state;
  const { user, abilities } = authentication;
  return {
    user,
    abilities
  };
}

const connectedHomePage = connect(mapStateToProps)(HomePage);
const styledHomePage = withStyles(homePageStyle)(connectedHomePage);
export { styledHomePage as HomePage };
