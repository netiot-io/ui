import React from 'react';
import ReactDOM from 'react-dom';
import DevicesPage from './';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<DevicesPage />, div);
  ReactDOM.unmountComponentAtNode(div);
});
