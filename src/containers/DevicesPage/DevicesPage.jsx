import React, {Fragment} from 'react';
import devicesPageStyle from "./DevicesPageStyle.jsx";
import { withStyles } from '@material-ui/core/styles';
import GridContainer from "../../components/Grid/GridContainer.jsx";
import GridItem from "../../components/Grid/GridItem.jsx";
import classNames from 'classnames';
import { Header } from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import logo from '../../imgs/logo.svg';
import InfoArea from '../../components/InfoArea/InfoArea.jsx';
import { connect } from 'react-redux';
import { DevicesTable } from '../../components/Table/DevicesTable/DevicesTable';
import {alertActions, deviceActions} from "../../actions";
import TableAlerts from '../../components/Alerts/TableAlerts';

class DevicesPage extends React.Component {
  constructor(props) {
    super(props);
    props.dispatch(deviceActions.getAll());

    this.startTimer = this.startTimer.bind(this);
  }

  startTimer = () => {
    this.intervalID = setInterval(() => {
      if (typeof this.props.devices !== 'undefined' && typeof this.props.devices.items !== 'undefined' &&
        this.props.devices.items.length > 0) {
        this.props.devices.items.forEach(device => {
          this.props.dispatch(deviceActions.getLast10Alerts(device.id));
          this.props.dispatch(deviceActions.getLastMessage(device.id));
          this.props.dispatch(deviceActions.getMessageCount(device.id));
        });
      }
    }, 5000);
  };

  handleDelete = (deviceIds) => {
    for (const deviceId of deviceIds) {
      this.props.dispatch(deviceActions.delete(deviceId));
    }
  };

  handleAdd = (device) => {
    this.props.dispatch(deviceActions.add(device));
  };

  handleEdit = (sensor) => {
    this.props.dispatch(deviceActions.update(sensor));
  };

  // noinspection JSUnusedGlobalSymbols
  componentWillMount = () => {
    this.startTimer();
  };

  // noinspection JSUnusedGlobalSymbols
  componentWillUnmount = () => {
    clearInterval(this.intervalID);
  };

  render() {
    const { classes, devices, alerts } = this.props;
    return (
      <Fragment>
        <Header rightLinks={<Fragment />} brand={<Fragment><img src={logo} className={classes.logo} alt={'NETIOT Logo'}/><span className={classes.brand}>NETIOT</span></Fragment>} color={'dark'} />
        <InfoArea
          title={'Manage devices'}
          description='This is the page where you can manage (add, edit and delete) your devices.'
        />
        <div className={classNames("jumbotron", classes.jumbotron)}>
          <div className={classNames("container", classes.container)}>
            <div className="col-xs-12">
              {<TableAlerts alerts={alerts} clearAlerts={() => this.props.dispatch(alertActions.clear())}/>}
        <GridContainer className={classes.root}>
          <GridItem xs={12}>
            {devices.items && <DevicesTable data={devices} handleAdd={this.handleAdd} handleDelete={this.handleDelete} handleEdit={this.handleEdit}/>}
          </GridItem>
        </GridContainer>
            </div>
          </div>
        </div>
        <Footer />
      </Fragment>
    );
  }
}

function mapStateToProps(state) {
  const {  devices, alerts } = state;
  return {
    devices,
    alerts
  };
}

const connectedDevicesPage = connect(mapStateToProps)(DevicesPage);
const styledDevicesPage = withStyles(devicesPageStyle)(connectedDevicesPage);
export { styledDevicesPage as DevicesPage };