import React from 'react';
import bacnetAdminPageStyle from "./BacnetAdminPageStyle";
import { withStyles } from '@material-ui/core/styles';
import GridContainer from "../../components/Grid/GridContainer.jsx";
import GridItem from "../../components/Grid/GridItem.jsx";
import { connect } from 'react-redux';
import {getData} from "@jsonforms/core/lib/index";
import {Button, FormControl, InputLabel, MenuItem, Select, TextField} from "@material-ui/core";
import {env} from "../../utils/config";
import {authHeader} from "../../utils/auth-header";

class BacnetAdminPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            controller: '',
            sensor: '',
            value: '',
            controllers: [],
            sensors: []
        };
        this.controllers = [];
        this.sensors = [];
        const requestOptions = {
            method: 'GET',
            headers: authHeader()
        };
        var that = this;
        fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_DEVICE_BASE_PATH}${env.REACT_APP_SERVER_DEVICE_PROTOCOLS}`, requestOptions)
            .then(response => response.json())
            .then((js) => {
                const data = JSON.parse(js.protocols.find(protocol => protocol.id === 3).metadata).devices;
                const controllers = [];
                const sensors = [];
                for (const [key, value] of Object.entries(data)) {
                    controllers.push(key);
                    sensors[key] = value;
                }
                that.setState({controllers, sensors});
            })
            .catch(e => {
                console.log(e);
                this.setState({progress: false});
            });
    }

    handleChange = property => event => {
        this.setState({[property]: event.target.value});
    };

    handleSend = event => {
        const requestOptions = {
            method: 'GET',
            headers: authHeader()
        };
        var that = this;
        fetch(`${env.REACT_APP_SERVER_BASE_PATH}${env.REACT_APP_SERVER_DEVICE_BASE_PATH}${env.REACT_APP_SERVER_DEVICE}`, requestOptions)
            .then(response => response.json())
            .then((js) => {
                const deviceId = js.devices.find(device => device.name === that.state.controller).id;
                const requestOptions = {
                    method: 'POST',
                    headers: { ...authHeader(), 'Content-Type': 'application/json' },
                    body: JSON.stringify({
                        device_id: deviceId,
                        sensor_name: this.state.sensor,
                        value: parseFloat(this.state.value)
                    })
                };
                fetch(`${env.REACT_APP_SERVER_BASE_PATH}app-hotel/bacnet-command/send`, requestOptions)
                    .then(response => console.log(response))
                    .catch(e => {
                        console.log(e);
                        this.setState({progress: false});
                    });
            })
            .catch(e => {
                console.log(e);
                this.setState({progress: false});
            });
    };

    render() {
        const { classes } = this.props;
        return (
            <GridContainer className={classes.root} alignItems='flex-start' alignContent='flex-start' justify='flex-start'>
                <GridItem xs={4} style={{textAlign: 'left'}}>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="controller">Controller</InputLabel>
                        <Select
                            value={this.state.controller}
                            onChange={this.handleChange('controller')}
                            inputProps={{
                                name: 'controller',
                                id: 'controller',
                            }}
                            autoWidth={true}
                        >
                            {this.state.controllers.map(controller => (
                                <MenuItem value={controller} key={controller}>{controller}</MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                </GridItem>
                <GridItem xs={4} style={{textAlign: 'left'}}>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="sensor">Sensor</InputLabel>
                        <Select
                            value={this.state.sensor}
                            onChange={this.handleChange('sensor')}
                            inputProps={{
                                name: 'sensor',
                                id: 'sensor',
                            }}
                            autoWidth={true}
                        >
                            {this.state.controller.length > 0 && this.state.sensors[this.state.controller].map(sensor => (
                                <MenuItem value={sensor} key={sensor}>{sensor}</MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                </GridItem>
                <GridItem xs={2} style={{display: 'contents', textAlign: 'left'}}>
                    <FormControl className={classes.formControl}>
                        <TextField
                            id="value"
                            label="Value"
                            value={this.state.value}
                            onChange={this.handleChange('value')}
                            margin="normal"
                            className={classes.textField}
                        />
                    </FormControl>
                </GridItem>
                <GridItem xs={1} style={{textAlign: 'left'}}>
                    <FormControl className={classes.formControl}>
                        <Button variant="contained" color="primary" className={classes.button}
                                onClick={this.handleSend}>
                            Send
                        </Button>
                    </FormControl>
                </GridItem>
            </GridContainer>
        );
    }
}

function mapStateToProps(state) {
    const { applications, alerts, authentication, users } = state;
    const { user, abilities } = authentication;
    return {
        applications,
        alerts,
        abilities,
        applicationConfigData: getData(state),
        users,
        user
    };
}

const connectedBacnetAdminPage = connect(mapStateToProps)(BacnetAdminPage);
const styledBacnetAdminPage = withStyles(bacnetAdminPageStyle)(connectedBacnetAdminPage);
export { styledBacnetAdminPage as BacnetAdminPage };
