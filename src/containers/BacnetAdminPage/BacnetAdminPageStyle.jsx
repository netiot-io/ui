const BacnetAdminPageStyle = {
    root: {
        width: '100%',
        overflowX: 'inherit',
    },
    textField: {
        marginTop: '0',
        marginBottom: '0'
    },
    formControl: {
        display: 'flex'
    }
};

export default BacnetAdminPageStyle;
