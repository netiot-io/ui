import React, {Fragment} from 'react';
import usersPageStyle from "./UsersPageStyle.jsx";
import { withStyles } from '@material-ui/core/styles';
import GridContainer from "../../components/Grid/GridContainer.jsx";
import GridItem from "../../components/Grid/GridItem.jsx";
import classNames from 'classnames';
import { Header } from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import logo from '../../imgs/logo.svg';
import InfoArea from '../../components/InfoArea/InfoArea.jsx';
import { connect } from 'react-redux';
import { UsersTable } from '../../components/Table/PartnerUsers/UsersTable';
import {alertActions, userActions} from "../../actions";
import TableAlerts from '../../components/Alerts/TableAlerts';

class UsersPage extends React.Component {
  constructor(props) {
    super(props);
    if (props.abilities.can('view', 'organizations')) {
      if (typeof props.users.organizations === 'undefined') {
        props.dispatch(userActions.getOrganizations(true));
      }
    } else {
      props.dispatch(userActions.getOrganizationUsers(props.user.organizationId));
    }
  }

  handleDelete = (userIds) => {
    for (const userId of userIds) {
      this.props.dispatch(userActions.delete(userId));
    }
  };

  handleAdd = (user) => {
    this.props.dispatch(userActions.add(user));
  };

  handleEdit = (user) => {
    this.props.dispatch(userActions.update(user));
  };

  render() {
    const { classes, users, alerts } = this.props;
    return (
      <Fragment>
        <Header rightLinks={<Fragment />} brand={<Fragment><img src={logo} className={classes.logo} alt={'NETIOT Logo'}/><span className={classes.brand}>NETIOT</span></Fragment>} color={'dark'} />
        <InfoArea
          title={'Manage users'}
          description='This is the page where you can manage (add, edit and delete) your users.'
        />
        <div className={classNames("jumbotron", classes.jumbotron)}>
          <div className={classNames("container", classes.container)}>
            <div className="col-xs-12">
              {<TableAlerts alerts={alerts} clearAlerts={() => this.props.dispatch(alertActions.clear())}/>}
        <GridContainer className={classes.root}>
          <GridItem xs={12}>
            {users.items && <UsersTable handleAdd={this.handleAdd} handleDelete={this.handleDelete} handleEdit={this.handleEdit}/>}
          </GridItem>
        </GridContainer>
            </div>
          </div>
        </div>
        <Footer />
      </Fragment>
    );
  }
}

function mapStateToProps(state) {
  const { users, alerts, authentication } = state;
  const { user, abilities } = authentication;
  return {
    user,
    abilities,
    users,
    alerts
  };
}

const connectedUsersPage = connect(mapStateToProps)(UsersPage);
const styledUsersPage = withStyles(usersPageStyle)(connectedUsersPage);
export { styledUsersPage as UsersPage };