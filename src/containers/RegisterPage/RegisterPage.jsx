import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {FormattedMessage} from 'react-intl';
import {alertActions, userActions} from '../../actions';
import {InputAdornment, TextField, FormControl, InputLabel, Input, IconButton, FormHelperText} from '@material-ui/core';
import { Visibility, VisibilityOff } from "@material-ui/icons";
import {withStyles} from '@material-ui/core/styles';
import RegisterPageStyle from "./RegisterPageStyle.jsx";
import classNames from 'classnames';
import { emailValidator, passwordValidator } from "../../utils/regex";
import { sha512 } from 'js-sha512';
import TableAlerts from '../../components/Alerts/TableAlerts';
import { env } from '../../utils/config';

class RegisterPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      user: {
        first_name: '',
        last_name: '',
        email: '',
        password: '',
        confirm_password: ''
      },
      showPassword: false,
      showConfirmPassword: false,
      formValid: false,
      formErrors: {
        firstname: 'Please fill in the First Name',
        lastname: 'Please fill in the Last Name',
        email: 'Please fill in a correct Email',
        password: 'Please fill in the password (it must contain at least 1 capital letter, 1 small letter, 1 digit and 1 special character and it needs to be at least 8 characters long)',
        confirmPassword: 'Passwords do not match'
      },
      firstnameValid: true,
      lastnameValid: true,
      emailValid: true,
      passwordValid: true,
      confirmPasswordValid: true,
      openAlert: false
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  validateForm = () => {
    let formValid = true;
    const { user } = this.state;
    if (user.first_name.length > 0) {
      this.setState({firstnameValid: true});
    } else {
      this.setState({firstnameValid: false});
      formValid = false;
    }

    if (user.last_name.length > 0) {
      this.setState({lastnameValid: true});
    } else {
      this.setState({lastnameValid: false});
      formValid = false;
    }

    if (emailValidator.test(user.email)) {
      this.setState({emailValid: true});
    } else {
      this.setState({emailValid: false});
      formValid = false;
    }

    if (passwordValidator.test(user.password)) {
      this.setState({passwordValid: true});
    } else {
      this.setState({passwordValid: false});
      formValid = false;
    }

    if (user.confirm_password === user.password) {
      this.setState({confirmPasswordValid: true});
    } else {
      this.setState({confirmPasswordValid: false});
      formValid = false;
    }

    this.setState({formValid});
    return formValid;
  };

  handleChange = property => event => {
    const { user } = this.state;
    this.setState({
      user: {
        ...user,
        [property]: event.target.value
      }
    });
  };

  handleSubmit(event) {
    event.preventDefault();
    const { dispatch } = this.props;
    if (this.validateForm()) {
      const user = {...this.state.user};
      user.password = sha512(user.password);
      delete user.confirm_password;
      dispatch(userActions.register(user));
    }
  }

  handleMouseDownPassword = event => {
    event.preventDefault();
  };

  handleClickShowPassword = (name) => {
    this.setState({ [name]: !this.state[name] });
  };

  render() {
    const { classes, registering, alerts  } = this.props;
    return (
      <div className="col-md-6 col-md-offset-3">
        <h2>Register</h2>
        {<TableAlerts alerts={alerts} clearAlerts={() => this.props.dispatch(alertActions.clear())}/>}
        <form name="form" onSubmit={this.handleSubmit}>
          <div className="col-md-12">
            <TextField
              autoFocus={true}
              error={!this.state.firstnameValid}
              id="firstname"
              name="firstname"
              label={<FormattedMessage id="app.register.firstname"
                                       defaultMessage="First Name"
                                       description="First name in registration form"/>}
              defaultValue={this.state.firstname}
              className={classes.textField}
              margin="normal"
              onChange={this.handleChange('first_name')}
              helperText={this.state.firstnameValid ? '' : this.state.formErrors.firstname}
              required={true}
              fullWidth={true}
            />
            <TextField
              error={!this.state.lastnameValid}
              id="lastname"
              name="lastname"
              label={<FormattedMessage id="app.register.lastname"
                                       defaultMessage="Last Name"
                                       description="Last name in registration form"/>}
              defaultValue={this.state.lastname}
              className={classes.textField}
              margin="normal"
              onChange={this.handleChange('last_name')}
              helperText={this.state.lastnameValid ? '' : this.state.formErrors.lastname}
              required={true}
              fullWidth={true}
            />
            <TextField
              error={!this.state.emailValid}
              id="email"
              name="email"
              label="Email"
              defaultValue={this.state.email}
              className={classes.textField}
              margin="normal"
              onChange={this.handleChange('email')}
              helperText={this.state.emailValid ? '' : this.state.formErrors.email}
              required={true}
              fullWidth={true}
            />
            <FormControl fullWidth={true} margin={'normal'} required={true} error={!this.state.passwordValid}>
              <InputLabel htmlFor="password">
                <FormattedMessage id="app.common.password"
                                  defaultMessage="Password"
                                  description="Password"/>
              </InputLabel>
              <Input
                id="password"
                name="password"
                type={this.state.showPassword ? 'text' : 'password'}
                onChange={this.handleChange('password')}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="Toggle password visibility"
                      onClick={() => this.handleClickShowPassword('showPassword')}
                      onMouseDown={this.handleMouseDownPassword}
                    >
                      {this.state.showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
              />
              <FormHelperText id="weight-helper-text">{this.state.passwordValid ? '' : this.state.formErrors.password}</FormHelperText>
            </FormControl>
            <FormControl fullWidth={true} margin={'none'} required={true} error={!this.state.confirmPasswordValid}>
              <InputLabel htmlFor="confirmPassword">
                <FormattedMessage id="app.common.confirmPassword"
                                  defaultMessage="Confirm password"
                                  description="Confirm password"/>
              </InputLabel>
              <Input
                id="confirmPassword"
                type={this.state.showConfirmPassword ? 'text' : 'password'}
                onChange={this.handleChange('confirm_password')}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="Toggle password visibility"
                      onClick={() => this.handleClickShowPassword('showConfirmPassword')}
                      onMouseDown={this.handleMouseDownPassword}
                    >
                      {this.state.showConfirmPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
              />
              <FormHelperText id="weight-helper-text">{this.state.confirmPasswordValid ? '' : this.state.formErrors.confirmPassword}</FormHelperText>
            </FormControl>
            <div className={classNames("form-group", "col-md-12", classes.registerButtons)}>
              <button className="btn btn-primary">Register</button>
              {registering &&
              <img alt="progress circle" src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
              }
              <Link to={`${env.REACT_APP_DOMAIN}login`} className="btn btn-link">
                <FormattedMessage id="app.common.cancel"
                                  defaultMessage="Cancel"
                                  description="Cancel"/>
              </Link>
            </div>
          </div>
        </form>

      </div>
    );
  }
}

function mapStateToProps(state) {
  const { registering } = state.registration;
  const { alerts } = state;
  return {
    registering,
    alerts
  };
}

const connectedRegisterPage = connect(mapStateToProps)(RegisterPage);
const styledRegisterPage = withStyles(RegisterPageStyle)(connectedRegisterPage);
export { styledRegisterPage as RegisterPage };