import React from 'react';
import { Route } from 'react-router-dom';
import { connect  } from 'react-redux';
import { PrivateRoute } from '../../components';
import { HomePage } from '../HomePage';
import { LoginPage } from '../LoginPage';
import { RegisterPage } from '../RegisterPage';
import { ResetPasswordPage } from '../ResetPasswordPage';
import {history} from "../../utils/store";
import { ConnectedRouter } from 'react-router-redux';
import {alertActions} from "../../actions";
import withStyles from "@material-ui/core/styles/withStyles";
import appStyle from "./AppStyle";
import {DevicesPage} from "../DevicesPage";
import {AccountPage} from "../AccountPage";
import {OrganizationsPage} from "../OrganizationsPage";
import {ApplicationsPage} from "../ApplicationsPage";
import {UsersPage} from "../UsersPage";
import {DecodersPage} from "../DecodersPage";
import { env } from '../../utils/config';

class App extends React.Component {
  constructor(props) {
    super(props);

    const { dispatch } = this.props;
    history.listen(() => {
      // clear alert on location change
      dispatch(alertActions.clear());
    });
  }

  render() {
    return (
        <ConnectedRouter history={history}>
            {/*add the router with history*/}
              <div>
                <PrivateRoute exact path={`${env.REACT_APP_DOMAIN}`} component={HomePage} />
                <PrivateRoute exact path={`${env.REACT_APP_DOMAIN}devices`} component={DevicesPage} />
                <PrivateRoute exact path={`${env.REACT_APP_DOMAIN}account`} component={AccountPage} />
                <PrivateRoute exact path={`${env.REACT_APP_DOMAIN}organizations`} component={OrganizationsPage} />
                <PrivateRoute exact path={`${env.REACT_APP_DOMAIN}users`} component={UsersPage} />
                <PrivateRoute exact path={`${env.REACT_APP_DOMAIN}applications`} component={ApplicationsPage} />
                <PrivateRoute exact path={`${env.REACT_APP_DOMAIN}decoders`} component={DecodersPage} />
                <Route path={`${env.REACT_APP_DOMAIN}login`} component={LoginPage} />
                <Route path={`${env.REACT_APP_DOMAIN}register`} component={RegisterPage} />
                <Route path={`${env.REACT_APP_DOMAIN}set-password`} component={ResetPasswordPage} />
              </div>
        </ConnectedRouter>
    );
  }
}

function mapStateToProps(state) {
  const { alert } = state;
  return {
    alert
  };
}

const connectedApp = connect(mapStateToProps)(App);
const styledApp = withStyles(appStyle)(connectedApp);
export { styledApp as App };