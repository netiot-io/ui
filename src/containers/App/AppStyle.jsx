const appStyle = {
  logo: {
    width: '50px'
  },
  brand: {
    fontFamily: 'Chelsea Market',
    fontSize: '50px',
    paddingLeft: '10px',
    color: '#4ebd9e'
  },
  jumbotron: {
    marginBottom: '0'
  }
};

export default appStyle;