import React, {Fragment} from 'react';
import organizationsPageStyle from "./OrganizationsPageStyle.jsx";
import { withStyles } from '@material-ui/core/styles';
import GridContainer from "../../components/Grid/GridContainer.jsx";
import GridItem from "../../components/Grid/GridItem.jsx";
import classNames from 'classnames';
import { Header } from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import logo from '../../imgs/logo.svg';
import InfoArea from '../../components/InfoArea/InfoArea.jsx';
import { connect } from 'react-redux';
import PartnerOrganizationsTable from '../../components/Table/PartnerOrganizations/OrganizationsTable';
import {alertActions, applicationActions, userActions} from "../../actions";
import TableAlerts from '../../components/Alerts/TableAlerts';

class OrganizationsPage extends React.Component {
  constructor(props) {
    super(props);
    if (typeof props.users.organizations === 'undefined') {
      props.dispatch(userActions.getOrganizations(true));
    }

    if (typeof props.applications.applications === 'undefined') {
      props.dispatch(applicationActions.getUserApplications());
    }
  }

  handleDelete = (organizationIds) => {
    for (const organizationId of organizationIds) {
      this.props.dispatch(userActions.deleteOrganization(organizationId));
    }
  };

  handleAdd = (organization) => {
    this.props.dispatch(userActions.addOrganization(organization));
  };

  handleEdit = (organization) => {
    this.props.dispatch(userActions.updateOrganization(organization));
  };

  render() {
    const { classes, users, alerts, applications } = this.props;
    return (
      <Fragment>
        <Header rightLinks={<Fragment />} brand={<Fragment><img src={logo} className={classes.logo} alt={'NETIOT Logo'}/><span className={classes.brand}>NETIOT</span></Fragment>} color={'dark'} />
        <InfoArea
          title={'Manage organizations'}
          description='This is the page where you can manage (add, edit and delete) your organizations.'
        />
        <div className={classNames("jumbotron", classes.jumbotron)}>
          <div className={classNames("container", classes.container)}>
            <div className="col-xs-12">
              {<TableAlerts alerts={alerts} clearAlerts={() => this.props.dispatch(alertActions.clear())}/>}
        <GridContainer className={classes.root}>
          <GridItem xs={12}>
            {users.organizations && applications.applications && <PartnerOrganizationsTable data={users} applications={applications.applications}
                                                               handleAdd={this.handleAdd} handleDelete={this.handleDelete}
                                                               handleEdit={this.handleEdit}/>}
          </GridItem>
        </GridContainer>
            </div>
          </div>
        </div>
        <Footer />
      </Fragment>
    );
  }
}

function mapStateToProps(state) {
  const { users, alerts, applications } = state;
  return {
    users,
    alerts,
    applications
  };
}

const connectedOrganizationsPage = connect(mapStateToProps)(OrganizationsPage);
const styledOrganizationsPage = withStyles(organizationsPageStyle)(connectedOrganizationsPage);
export { styledOrganizationsPage as OrganizationsPage };