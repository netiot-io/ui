import React from 'react';
import ReactDOM from 'react-dom';
import OrganizationsPage from './';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<OrganizationsPage />, div);
  ReactDOM.unmountComponentAtNode(div);
});
