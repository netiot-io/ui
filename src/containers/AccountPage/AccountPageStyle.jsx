const AccountPageStyle = {
  root: {
    width: '100%',
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
  row: {
    '&:nth-of-type(odd)': {
      backgroundColor: 'white',
    },
  },
  paper: {
    padding: 20,
    textAlign: 'center',
    color: 'black'
  },
  panel: {
    width: '50%'
  },
  logo: {
    width: '50px'
  },
  brand: {
    fontFamily: 'Chelsea Market',
    fontSize: '50px',
    paddingLeft: '10px',
    color: '#4ebd9e'
  },
  gridContainer: {
    overflowX: 'visible'
  },
  margin: {
    margin: 10,
  },
  textField: {
    flexBasis: '500px',
    width: 'calc( 50% - 20px)',
  },
  jumbotron: {
    marginBottom: '0'
  },
  formContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    width: '100%'
  },
  accountDetailsForm: {
    width: '100%'
  },
  changePasswordForm: {
    width: '100%'
  },
  updateSubscriptionForm: {
    width: '100%'
  },
  saveButton: {
    marginTop: "20px"
  },
  "jumbotron p" :{
    marginBottom: '0px'
  },
  "@media (min-width: 1200px)": {
    container: {
      width: '1900px'
    }
  }
};

export default AccountPageStyle;