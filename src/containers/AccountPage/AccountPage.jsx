import React, {Fragment} from 'react';
import AccountPageStyle from "./AccountPageStyle.jsx";
import {withStyles} from '@material-ui/core/styles';
import GridContainer from "../../components/Grid/GridContainer.jsx";
import GridItem from "../../components/Grid/GridItem.jsx";
import classNames from 'classnames';
import {Header} from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import logo from '../../imgs/logo.svg';
import InfoArea from '../../components/InfoArea/InfoArea.jsx';
import {connect} from 'react-redux';
import {alertActions, userActions} from "../../actions";
import Typography from '@material-ui/core/Typography';
import {
  Avatar,
  Button,
  Card,
  CardContent,
  CardHeader,
  FormControl,
  IconButton,
  Input,
  InputAdornment,
  InputLabel,
  MenuItem,
  Paper,
  TextField,
  FormHelperText
} from '@material-ui/core';
import {Visibility, VisibilityOff} from "@material-ui/icons";
import TableAlerts from '../../components/Alerts/TableAlerts';
import { passwordValidator } from "../../utils/regex";
import { sha512 } from 'js-sha512';

class AccountPage extends React.Component {
  constructor(props) {
    super(props);
    if (typeof props.users.partner === 'undefined') {
      props.dispatch(userActions.getPartnerInfo());
    }
    this.state = {
      subscription: props.user.role,
      showCurrentPassword: false,
      showNewPassword: false,
      showConfirmPassword: false,
      firstname: props.user.firstName,
      lastname: props.user.lastName,
      phone: props.user.phone,
      email: props.user.email,
      organizationName: props.user.organizationName,
      organizationPhone: props.user.organizationPhone,
      organizationAddress: props.user.organizationAddress,
      currentPassword: '',
      newPassword: '',
      confirmPassword: '',
      formErrors: {
        currentPassword: 'Incorrect password',
        newPassword: 'Incorrect password (it must contain at least 1 capital letter, 1 small letter, 1 digit and 1 special character and it needs to be at least 8 characters long)',
        confirmPassword: 'Incorrect password (it must contain at least 1 capital letter, 1 small letter, 1 digit and 1 special character and it needs to be at least 8 characters long)',
        passwordsMatch: 'Passwords do not match',
        organizationName: 'Name cannot be empty'
      },
      currentPasswordValid: true,
      newPasswordValid: true,
      confirmPasswordValid: true,
      passwordsMatchValid: true,
      organizationNameValid: true
    }
  }

  handleSaveDetails = (user) => {
    this.props.dispatch(userActions.update(user));
  };

  handleSaveOrganizationDetails = () => {
    if (this.validateOrganizationForm()) {
      this.props.dispatch(userActions.updateOrganization({
        id: this.props.user.organizationId,
        name: this.state.organizationName,
        phone: this.state.organizationPhone,
        address: this.state.organizationAddress
      }));
    }
  };

  handleChangePassword = () => {
    if (this.validatePasswordForm()) {
      this.props.dispatch(userActions.changePassword({
        oldPassword: sha512(this.state.currentPassword),
        newPassword: sha512(this.state.newPassword)
      }));
    }
  };

  handleMouseDownPassword = event => {
    event.preventDefault();
  };

  handleClickShowPassword = (name) => {
    this.setState({ [name]: !this.state[name] });
  };

  validateOrganizationForm = () => {
    let formValid = true;
    if (this.state.organizationName.length > 0) {
      this.setState({organizationNameValid: true});
    } else {
      this.setState({organizationNameValid: false});
      formValid = false;
    }

    return formValid;
  };

  validatePasswordForm = () => {
    let formValid = true;
    if (this.state.currentPassword.length > 0) {
      this.setState({currentPasswordValid: true});
    } else {
      this.setState({currentPasswordValid: false});
      formValid = false;
    }
    if (passwordValidator.test(this.state.newPassword)) {
      this.setState({newPasswordValid: true});
    } else {
      this.setState({newPasswordValid: false});
      formValid = false;
    }
    if (passwordValidator.test(this.state.confirmPassword)) {
      this.setState({confirmPasswordValid: true});
    } else {
      this.setState({confirmPasswordValid: false});
      formValid = false;
    }
    if (this.state.newPassword === this.state.confirmPassword) {
      this.setState({passwordsMatchValid: true});
    } else {
      this.setState({passwordsMatchValid: false});
      formValid = false;
    }
    return formValid
  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
    switch (name) {
      case 'organizationName':
        if (event.target.value.length === 0) {
          this.setState({organizationNameValid: false});
        } else {
          this.setState({organizationNameValid: true});
        }
        break;
      default:
        console.log('Unknown property edit');
    }
  };

  render() {
    const { classes, alerts, abilities, users } = this.props;
    return (
      <Fragment>
        <Header rightLinks={<Fragment />}
                brand={<Fragment><img src={logo} className={classes.logo} alt={'NETIOT Logo'}/><span
                  className={classes.brand}>NETIOT</span></Fragment>} color={'dark'}/>
        <InfoArea
          title={'Manage Account'}
          description='This is the page where you can manage your account.'
        />
        <div className={classNames(classes.jumbotron, "jumbotron")}>
          <div className={classNames("container", classes.container)}>
            <div className="col-xs-12">
              {<TableAlerts alerts={alerts} clearAlerts={() => this.props.dispatch(alertActions.clear())}/>}
              {abilities.can('view', 'partner_info') && users.partner &&
                <GridContainer className={classNames("gridContainer")}>
                  <Card className={classNames('col-xs-4', 'col-xs-offset-4', classes.card)}>
                    <CardHeader
                      avatar={
                        <Avatar aria-label="Recipe" className={classes.avatar}
                                src={'https://cdn2.iconfinder.com/data/icons/lil-faces/241/lil-face-1-512.png'}/>
                      }
                      title={users.partner.first_name + ' ' + users.partner.last_name}
                      subheader="Account manager (N/A)"
                    />
                    <CardContent>
                      <Typography component="p">
                        Phone: {users.partner.phone_number} <br/>
                        Email: {users.partner.email}
                      </Typography>
                    </CardContent>
                  </Card>
                </GridContainer>
              }

        <GridContainer className={classNames("gridContainer")} style={{ marginTop: '10px'}}>
          <GridItem xs={6}>
            <Paper className={classes.paper}>
              <Typography variant="headline" component="h3">
                Details
              </Typography>
              <div className={classes.formContainer}>
                <form id="accountDetailsForm" className={classes.accountDetailsForm} onSubmit={this.handleSubmit}>
                  <TextField
                    id="firstname"
                    label="First Name"
                    className={classNames(classes.margin, classes.textField)}
                    value={this.state.firstname}
                    onChange={this.handleChange('firstname')}
                    margin="normal"
                  />
                  <TextField
                    id="lastname"
                    label="Last Name"
                    className={classNames(classes.margin, classes.textField)}
                    value={this.state.lastname}
                    onChange={this.handleChange('lastname')}
                    margin="normal"
                  />
                  <TextField
                    id="email"
                    label="Email"
                    className={classNames(classes.margin, classes.textField)}
                    value={this.state.email}
                    onChange={this.handleChange('email')}
                    margin="normal"
                  />
                  <TextField
                    id="phone"
                    label="Phone"
                    className={classNames(classes.margin, classes.textField)}
                    value={this.state.phone}
                    onChange={this.handleChange('phone')}
                    margin="normal"
                  />
                  <Button variant="contained" color="primary" className={classNames(classes.button, classes.typeButton, classes.saveButton)} onClick={this.handleSaveDetails}>
                    Save
                  </Button>
                </form>
              </div>
            </Paper>
          </GridItem>
          <GridItem xs={6}>
            <GridContainer className={classNames("gridContainer")}>
              {abilities.can('view', 'enterprise_admin_header') &&
              <GridItem xs={12}>
                <Paper className={classes.paper}>
                  <Typography variant="headline" component="h3">
                    Organization details
                  </Typography>
                  <div className={classes.formContainer}>
                    <form id="organizationDetailsForm" className={classes.changePasswordForm} onSubmit={this.handleSubmit}>
                      <TextField
                        required
                        error={!this.state.organizationNameValid}
                        id="organization-name"
                        label="Name"
                        className={classNames(classes.margin, classes.textField)}
                        value={this.state.organizationName}
                        onChange={this.handleChange('organizationName')}
                        margin="normal"
                        helperText={this.state.organizationNameValid ? '' : this.state.formErrors.organizationName}
                      />
                      <TextField
                        id="organization-phone"
                        label="Phone"
                        className={classNames(classes.margin, classes.textField)}
                        value={this.state.organizationPhone}
                        onChange={this.handleChange('organizationPhone')}
                        margin="normal"
                      />
                      <TextField
                        id="organization-address"
                        label="Address"
                        className={classNames(classes.margin, classes.textField)}
                        value={this.state.organizationAddress}
                        onChange={this.handleChange('organizationAddress')}
                        margin="normal"
                      />
                      <br />
                      <Button variant="contained" color="primary"
                              className={classNames(classes.button, classes.typeButton, classes.saveButton)}
                              onClick={this.handleSaveOrganizationDetails}>
                        Save
                      </Button>
                    </form>
                  </div>
                </Paper>
              </GridItem>
              }
              <GridItem xs={12} style={{ marginTop: abilities.can('view', 'enterprise_admin_header') ? '10px' : '0px'}}>
                <Paper className={classes.paper}>
                  <Typography variant="headline" component="h3">
                    Change password
                  </Typography>
                  <div className={classes.formContainer}>
                    <form id="changePasswordForm" className={classes.changePasswordForm} onSubmit={this.handleSubmit}>
                      <FormControl className={classNames(classes.margin, classes.textField)}>
                        <InputLabel htmlFor="currentPassword" required={true} error={!this.state.currentPasswordValid}>Current password</InputLabel>
                        <Input
                          error={!this.state.currentPasswordValid}
                          id="currentPassword"
                          type={this.state.showCurrentPassword ? 'text' : 'password'}
                          onChange={this.handleChange('currentPassword')}
                          endAdornment={
                            <InputAdornment position="end">
                              <IconButton
                                aria-label="Toggle password visibility"
                                onClick={() => this.handleClickShowPassword('showCurrentPassword')}
                                onMouseDown={this.handleMouseDownPassword}
                              >
                                {this.state.showCurrentPassword ? <VisibilityOff /> : <Visibility />}
                              </IconButton>
                            </InputAdornment>
                          }
                          required={true}
                        />
                        <FormHelperText error={!this.state.currentPasswordValid}
                          id="weight-helper-text">{this.state.currentPasswordValid ? '' : this.state.formErrors.currentPassword}</FormHelperText>
                      </FormControl>
                      <FormControl className={classNames(classes.margin, classes.textField)}>
                        <InputLabel htmlFor="newPassword" required={true} error={!this.state.newPasswordValid || !this.state.passwordsMatchValid}>New password</InputLabel>
                        <Input
                          error={!this.state.newPasswordValid || !this.state.passwordsMatchValid}
                          id="newPassword"
                          type={this.state.showNewPassword ? 'text' : 'password'}
                          onChange={this.handleChange('newPassword')}
                          endAdornment={
                            <InputAdornment position="end">
                              <IconButton
                                aria-label="Toggle password visibility"
                                onClick={() => this.handleClickShowPassword('showNewPassword')}
                                onMouseDown={this.handleMouseDownPassword}
                              >
                                {this.state.showNewPassword ? <VisibilityOff /> : <Visibility />}
                              </IconButton>
                            </InputAdornment>
                          }
                          required={true}
                        />
                        <FormHelperText error={!this.state.newPasswordValid || !this.state.passwordsMatchValid}
                                        id="weight-helper-text">{this.state.newPasswordValid ? (this.state.passwordsMatchValid ? '' : this.state.formErrors.passwordsMatch)
                                            : this.state.formErrors.newPassword}</FormHelperText>
                      </FormControl>
                      <FormControl className={classNames(classes.margin, classes.textField)}>
                        <InputLabel htmlFor="confirmPassword" required={true} error={!this.state.confirmPasswordValid || !this.state.passwordsMatchValid}>Confirm password</InputLabel>
                        <Input
                          error={!this.state.confirmPasswordValid || !this.state.passwordsMatchValid}
                          id="confirmPassword"
                          type={this.state.showConfirmPassword ? 'text' : 'password'}
                          onChange={this.handleChange('confirmPassword')}
                          endAdornment={
                            <InputAdornment position="end">
                              <IconButton
                                aria-label="Toggle password visibility"
                                onClick={() => this.handleClickShowPassword('showConfirmPassword')}
                                onMouseDown={this.handleMouseDownPassword}
                              >
                                {this.state.showConfirmPassword ? <VisibilityOff /> : <Visibility />}
                              </IconButton>
                            </InputAdornment>
                          }
                          required={true}
                        />
                        <FormHelperText error={!this.state.confirmPasswordValid || !this.state.passwordsMatchValid}
                                        id="weight-helper-text">{this.state.confirmPasswordValid ? (this.state.passwordsMatchValid ? '' : this.state.formErrors.passwordsMatch)
                                          : this.state.formErrors.confirmPassword}</FormHelperText>
                      </FormControl>
                      <br />
                      <Button variant="contained" color="primary" className={classNames(classes.button, classes.typeButton, classes.saveButton)} onClick={this.handleChangePassword}>
                        Save
                      </Button>
                    </form>
                  </div>
                </Paper>
              </GridItem>
              {abilities.can('view', 'subscription') &&
                <GridItem xs={12}>
                  <Paper className={classes.paper} style={{marginTop: '10px'}}>
                    <Typography variant="headline" component="h3">
                      Update subscription
                    </Typography>
                    <div className={classes.formContainer}>
                      <form id="updateSubscriptionForm" className={classes.updateSubscriptionForm}
                            onSubmit={this.handleSubmit}>
                        <TextField
                          select
                          label=""
                          className={classNames(classes.margin, classes.textField)}
                          value={this.state.subscription}
                          onChange={this.handleChange('subscription')}
                          InputProps={{
                            startAdornment: <InputAdornment position="start"
                                                            disableTypography={true}>Type</InputAdornment>,
                          }}
                        >
                          <MenuItem key='subscription0' value='free'>
                            Free
                          </MenuItem>
                          <MenuItem key='subscription1' value='premium'>
                            Premium
                          </MenuItem>
                        </TextField>
                        <br/>
                        <a href='https://www.google.com'>Compare subscription types</a>
                        <br/>
                        <Button variant="contained" color="primary"
                                className={classNames(classes.button, classes.typeButton, classes.saveButton)}
                                onClick={this.handleSaveDetails}>
                          Save
                        </Button>
                      </form>
                    </div>
                  </Paper>
                </GridItem>
              }
            </GridContainer>
          </GridItem>
        </GridContainer>
            </div>
          </div>
        </div>
        <Footer />
      </Fragment>
    );
  }
}

function mapStateToProps(state) {
  const {  authentication, alerts, users } = state;
  const { user, abilities } = authentication;
  return {
    user,
    users,
    abilities,
    alerts
  };
}

const connectedAccountPage = connect(mapStateToProps)(AccountPage);
const styledAccountPage = withStyles(AccountPageStyle)(connectedAccountPage);
export { styledAccountPage as AccountPage };