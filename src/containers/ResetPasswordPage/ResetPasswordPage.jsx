import React from 'react';
import { connect } from 'react-redux';
import {FormattedMessage} from 'react-intl';
import { userActions } from '../../actions';
import {InputAdornment, FormControl, InputLabel, Input, IconButton, FormHelperText} from '@material-ui/core';
import { Visibility, VisibilityOff } from "@material-ui/icons";
import {withStyles} from '@material-ui/core/styles';
import ResetPasswordPageStyle from "./ResetPasswordPageStyle.jsx";
import classNames from 'classnames';
import SnackbarContent from '../../components/Snackbar/SnackbarContent';
import InfoOutline from "@material-ui/icons/InfoOutlined";
import { sha512 } from 'js-sha512';

const qs = require('querystringify');

const passwordValidator = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@!%*?&])[A-Za-z\d$@!%*?&]{8,}/;

class ResetPasswordPage extends React.Component {
  constructor(props) {
    super(props);

    const {search} = props.location;
    let activate = 0;
    let token = '';
    if (search.length > 0) {
      const query = qs.parse(this.props.location.search);
      activate = query.hasOwnProperty('activate') ? query.activate : '';
      token = query.hasOwnProperty('token') ? query.token : '';
    }

    this.state = {
      token,
      activate,
      password: '',
      confirmPassword: '',
      showPassword: false,
      showConfirmPassword: false,
      formValid: false,
      formErrors: {
        password: 'Please fill in the password (it must contain at least 1 capital letter, 1 small letter, 1 digit and 1 special character and it needs to be at least 8 characters long)',
        confirmPassword: 'Passwords do not match'
      },
      passwordValid: true,
      confirmPasswordValid: true,
      openAlert: false
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  validateForm = () => {
    let formValid = true;
    if (passwordValidator.test(this.state.password)) {
      this.setState({passwordValid: true});
    } else {
      this.setState({passwordValid: false});
      formValid = false;
    }

    if (this.state.confirmPassword === this.state.password) {
      this.setState({confirmPasswordValid: true});
    } else {
      this.setState({confirmPasswordValid: false});
      formValid = false;
    }

    this.setState({formValid});
    return formValid;
  };

  handleChange = property => event => {
    this.setState({
      ...this.state,
      [property]: event.target.value
    });
  };

  handleSubmit(event) {
    event.preventDefault();
    const { dispatch } = this.props;
    if (this.validateForm()) {
      delete this.state.confirmPassword;
      if (this.state.activate === '1') {
        dispatch(userActions.activate(this.state.token, sha512(this.state.password)));
      } else {
        dispatch(userActions.setPassword(this.state.token, sha512(this.state.password)));
      }
      
    }
  }

  handleMouseDownPassword = event => {
    event.preventDefault();
  };

  handleClickShowPassword = (name) => {
    this.setState({ [name]: !this.state[name] });
  };

  componentWillReceiveProps = nextProps => {
    const {alert} = nextProps;
    if (typeof alert !== 'undefined') {
      if (alert.type === 'alert-danger') {
        this.setState({openAlert: true});
      } else if (this.state.openAlert) {
        this.setState({openAlert: false});
      }
    }
  };

  render() {
    const { classes, alert  } = this.props;
    return (
      <div className="col-md-6 col-md-offset-3">
        <h2>Set password</h2>
        {this.state.openAlert && <SnackbarContent
          message={<span><FormattedMessage id={alert.message.split(':')[0]}
                                           defaultMessage={alert.message.split(':')[0]}
                                           description="Password alert messages"
                                           values={{ value: alert.message.split(':').length > 1 ? alert.message.split(':')[1] : '' }} />
                  </span>}
          close
          color="danger"
          icon={InfoOutline}
        />}
        <form name="form" onSubmit={this.handleSubmit}>
          <div className="col-md-12">
            <FormControl fullWidth={true} margin={'normal'} required={true} error={!this.state.passwordValid}>
              <InputLabel htmlFor="password">
                <FormattedMessage id="app.common.password"
                                  defaultMessage="Password"
                                  description="Password"/>
              </InputLabel>
              <Input
                id="password"
                name="password"
                type={this.state.showPassword ? 'text' : 'password'}
                onChange={this.handleChange('password')}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="Toggle password visibility"
                      onClick={() => this.handleClickShowPassword('showPassword')}
                      onMouseDown={this.handleMouseDownPassword}
                    >
                      {this.state.showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
              />
              <FormHelperText id="weight-helper-text">{this.state.passwordValid ? '' : this.state.formErrors.password}</FormHelperText>
            </FormControl>
            <FormControl fullWidth={true} margin={'none'} required={true} error={!this.state.confirmPasswordValid}>
              <InputLabel htmlFor="confirmPassword">
                <FormattedMessage id="app.common.confirmPassword"
                                  defaultMessage="Confirm password"
                                  description="Confirm password"/>
              </InputLabel>
              <Input
                id="confirmPassword"
                type={this.state.showConfirmPassword ? 'text' : 'password'}
                onChange={this.handleChange('confirmPassword')}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="Toggle password visibility"
                      onClick={() => this.handleClickShowPassword('showConfirmPassword')}
                      onMouseDown={this.handleMouseDownPassword}
                    >
                      {this.state.showConfirmPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
              />
              <FormHelperText id="weight-helper-text">{this.state.confirmPasswordValid ? '' : this.state.formErrors.confirmPassword}</FormHelperText>
            </FormControl>
            <div className={classNames("form-group", "col-md-12", classes.registerButtons)}>
              <button className="btn btn-primary">Send</button>
            </div>
          </div>
        </form>

      </div>
    );
  }
}

function mapStateToProps(state) {
  const { alert } = state;
  return {
    alert
  };
}

const connectedResetPasswordPage = connect(mapStateToProps)(ResetPasswordPage);
const styledResetPasswordPage = withStyles(ResetPasswordPageStyle)(connectedResetPasswordPage);
export { styledResetPasswordPage as ResetPasswordPage };