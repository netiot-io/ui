FROM node:8.10 as builder

RUN mkdir /ui
WORKDIR /ui
COPY package.json package.json
RUN npm install --quiet

COPY . .

RUN npm run build

# Copy built app into nginx container
FROM nginx:1.15.2-alpine
RUN apk add --no-cache jq
COPY --from=builder /ui/build /var/www
COPY nginx.conf /etc/nginx/nginx.conf
EXPOSE 80
COPY docker-entrypoint.sh /
RUN chmod 755 docker-entrypoint.sh
ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]
